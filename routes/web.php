<?php

use App\Http\Controllers\TodoController;
use App\Modules\User\Models\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Modules\Course\Controllers\CourseController;
use App\Modules\Course\Models\Course;
Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/auth/{provider}', 'Auth\LoginController@socialLoginRedirect');
Route::get('/callback/{provider}', 'Auth\LoginController@socialLoginCallback');
Route::get('/search/courses/{position}', function ($position) {
    $results = Course::where('ispublished',true)->where('posisi_user',$position)->paginate(9);
    $users = User::all();
    $locations = DB::table('provinces')->get();
    $participants = DB::table('class_donations')
                      ->join('transactions','transactions.transaction_code','=','class_donations.transaction_code')
                      ->join('users','user_id','=','id')
                      // ->where('course_id',$course->id)
                      ->select('course_id','user_id','name','link_foto_profile','amount','type')->get();
    $keyword ='';                      
    return view('search', ['results'=>$results,'locations'=>$locations, 'users'=>$users,'participants'=>$participants,'keyword'=>$keyword]);
});
Route::get('/search/speakers', function () {
    $results = User::where('isspeaker',true)->paginate(9);
    $keyword = '';
    return view('speakers', ['results'=>$results,'keyword'=>$keyword]);
});
Route::get('/about', ['as' => 'about', 'uses' => 'HomeController@about']);
Route::get('/flow', ['as' => 'flow', 'uses' => 'HomeController@flow']);


Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/blog', ['as' => 'admin.blog', 'uses' => 'BlogController@index']);
    Route::get('/blog/create', ['as' => 'admin.blog.create', 'uses' => 'BlogController@create']);
    Route::post('/blog/create', ['as' => 'admin.blog.store', 'uses' => 'BlogController@store']);
    Route::get('/blog/{id}', ['as' => 'admin.blog.show', 'uses' => 'BlogController@show'])->where('id', '[0-9]+');
    Route::get('/blog/{id}/edit', ['as' => 'admin.blog.edit', 'uses' => 'BlogController@edit'])->where('id', '[0-9]+');
    Route::post('/blog/{id}/edit', ['as' => 'admin.blog.update', 'uses' => 'BlogController@update'])->where('id', '[0-9]+');
    Route::post('/blog/{id}/delete', ['as' => 'admin.blog.delete', 'uses' => 'BlogController@destroy'])->where('id', '[0-9]+');
});
