<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/3/2017
 * Time: 20:58
 */
return [
    'modules' => [
        'User',
        'Course',
        'Transaction',
        'Admin',
    ]
];
?>
