<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => \App\Modules\User\Models\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '1326774814054362',
        'client_secret' => '6b53afa8d8c9b2a4d1b1061645dabc88',
        'redirect' => 'http://belpatungan.com/callback/facebook',
     ],

    'google' => [
        'client_id' => '1021903151000-cmna8uqnc4llrlngriascn361q0f2b9n.apps.googleusercontent.com',
        'client_secret' => 'sg-jBBlfWEA6xuelLP6EcIlg',
        'redirect' => 'http://belpatungan.com/callback/google',
    ],

];
