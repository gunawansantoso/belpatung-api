<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 4/8/2017
 * Time: 11:00
 */

namespace App\Http\Repositories;


interface PostRepository
{
    public function all();

    public function getById($id);

    public function create(array $attr);

    public function update($id, array $attr);

    public function delete($id);

}