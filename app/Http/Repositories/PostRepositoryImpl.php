<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 4/8/2017
 * Time: 11:03
 */

namespace App\Http\Repositories;


use App\Post;

class PostRepositoryImpl implements PostRepository
{
    private $postModel;

    public function __construct(Post $post)
    {
        $this->postModel = $post;
    }

    public function all()
    {
        return $this->postModel->paginate(5);
    }

    public function getById($id)
    {
        $post = $this->postModel->find($id);
        return $post;
    }

    public function create(array $attr)
    {
        $post = $this->postModel->create($attr);
        return $post;
    }


    public function update($id, array $attr)
    {
        $post = $this->postModel->find($id);
        if ($post)
            $post->update($attr);
        return $post;
    }

    public function delete($id)
    {
        $result = false;
        $post = $this->postModel->find($id);
        if ($post) {
            $post->delete();
            $result = true;
        }
        return $result;
    }

}