<?php
namespace App\Http\Repositories;
use GuzzleHttp\Client;

class PhotoRepositoryImpl implements PhotoRepository {

    public function getDownloadUrl($file)
    {
        $name = $file->getClientOriginalName();
        $client = new Client();
        $response = $client->request('POST', 'https://api.imgur.com/3/upload', [
            'headers' => [
                'Authorization' => 'Client-ID 64b7e5009ee260e',
            ],
            'multipart' => [
                [
                    'name'     => 'image',
                    'contents' => fopen($file->getPathname(), 'r'),
                    'filename' => $name
                ],
            ],
        ]);
        $img_url = json_decode($response->getBody()->getContents())->data->link;
        return $img_url;
    }
}

?>