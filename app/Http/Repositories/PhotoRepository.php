<?php
namespace App\Http\Repositories;

interface PhotoRepository {

    public function getDownloadUrl($file);

}
?>