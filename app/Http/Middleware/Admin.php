<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::info(json_encode(Auth::user()));

        if (!Auth::user()) {
            return redirect('/login');
        }
        else if (!Auth::user()->isadmin) {
            return redirect('/')->withErrors('failed', 'Terjadi kesalahan, tidak dapat mengakses halaman.');
        }
        return $next($request);
    }


}
