<?php

namespace App\Http\Controllers;

use App\Http\Repositories\BlogRepositoryImpl;
use App\Http\Repositories\PostRepositoryImpl;
use App\Http\Repositories\PhotoRepositoryImpl;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private $postRepository;
    private $photoRepository;

    public function __construct(PostRepositoryImpl $postRepositoryImpl, PhotoRepositoryImpl $photoRepositoryImpl)
    {
        $this->postRepository = $postRepositoryImpl;
        $this->photoRepository = $photoRepositoryImpl;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->postRepository->all();
        return view('admin.post.index', ['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
            'photo' => 'required'
        ]);

        $file = $request->file('photo');
        $img_url = $this->photoRepository->getDownloadUrl($file);
        $data = $request->all();
        $data['photo'] = $img_url;
        $result = $this->postRepository->create($data);
        return $this->checkResult($result);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->postRepository->getById($id);
        return view('admin.post.detail', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->postRepository->getById($id);
        return view('admin.post.edit', ['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'content' => 'required',
        ]);

        $file = $request->input('photo');
        $data = $request->all();
        if ($file) {
            $img_url = $this->photoRepository->getDownloadUrl($file);
            $data->photo = $img_url;
        }
        $result = $this->postRepository->update($id, $data);
        return $this->checkResult($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->postRepository->delete($id);
        return $this->checkResult($result);
    }

    private function checkResult($result) {
        if (!$result) {
            return redirect()->back()->withErrors('failed', 'Terjadi kesalahan.');
        }
        return redirect()->back();
    }
}
