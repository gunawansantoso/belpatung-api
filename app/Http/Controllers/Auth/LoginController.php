<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Modules\User\Repositories\UserRepository;
use App\Modules\User\Repositories\UserRepositoryImpl;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    private $userRepository;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepositoryImpl $userRepository)
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->userRepository = $userRepository;
    }

    public function socialLoginRedirect($provider) {
        return Socialite::driver($provider)->redirect();
    }

    public function socialLoginCallback($provider) {
        // NOTE : callback login
        $facebookUser = Socialite::driver($provider)->user();
        $user = $this->userRepository->createOrGetUser($facebookUser, $provider);
        Auth::login($user);
        if (Auth::user() != null) {
            return redirect()->back();
        } else {
            return redirect()->back()->withErrors('failed', "not authenticated");
        }
    }
}
