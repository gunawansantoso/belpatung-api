<?php

namespace App\Http\Controllers;

use App\Modules\Course\Repositories\CourseRepository;
use Illuminate\Http\Request;
use App\Modules\User\Models\Testimoni;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $courseRepository;
    public function __construct(CourseRepository $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preferences = DB::table('landing_page')->where('id',1)->first();
        // $preferences = collect();
        $users = User::all();
        if ($preferences) {
            $courses = [
                $this->courseRepository->getCoursesById($preferences->class_id_1),
                $this->courseRepository->getCoursesById($preferences->class_id_2),
                $this->courseRepository->getCoursesById($preferences->class_id_3),
            ];

            $speakerCourses = [
                $this->courseRepository->getCoursesById($preferences->class_id_4),
                $this->courseRepository->getCoursesById($preferences->class_id_5),
                $this->courseRepository->getCoursesById($preferences->class_id_6),
            ];
            // $testimonis = 0;
           $testimonis =  DB::table('testimonis')
               ->join('users','users.id','=','testimonis.user_id')
               ->whereIn('testimonis.id',[$preferences->testimoni_1,$preferences->testimoni_2,$preferences->testimoni_3])->get();

           $participants = DB::table('class_donations')
                      ->join('transactions','transactions.transaction_code','=','class_donations.transaction_code')
                      ->join('users','user_id','=','id')
                      // ->where('course_id',$course->id)
                      ->select('course_id','user_id','name','link_foto_profile','amount','type')->get();

            // $testimonis =  DB::table('testimonis')->get();
        }


        return view('index', ['preferences' => $preferences, 'courses' => $courses, 'testimonis'=> $testimonis, 'speakerCourses' => $speakerCourses,'users'=>$users,'participants'=>$participants]);
    }

    public function about() {
        return view('about');
    }

    public function flow() {
        return view('flow');
    }

    public function calendar(){
      return view('calendar');
    }
}
