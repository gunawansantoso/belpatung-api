@extends('layouts.dashboard')

@section('title', 'Page Title')

@section('main')
    <div class="container container-white">
        <p class="title-dashboard" > Detail Pembayaran </p>
        <div class="green-line"></div>
        <br>
        <div class="well" style="background-color: #ffffff;">
            <div class="row" style="width:80%;margin:auto">
                <div class="col-sm-12">
                    <img src="{{asset('belpatung-api/public/images/image.png')}}" alt="" style="width:100%;max-height: 300px">

                    <h3>Detail Pembayaran</h3>
                    <hr>
                    <div>
                        <div style="width:150px;display: inline-block;">
                            Nama Kelas
                        </div>
                        <div style="width:250px;display: inline-block;">
                            {{ $donation->course->nama_kelas }}
                        </div>
                    </div>
                    <br>
                    <div>
                        <div style="width:150px;display: inline-block;">
                            Kode Kelas
                        </div>
                        <div style="width:250px;display: inline-block;">
                            {{ $donation->course->id}}
                        </div>
                    </div>
                    <br>
                    <div>
                        <div style="width:150px;display: inline-block;">
                            Jenis Pembayaran
                        </div>
                        <div style="width:250px;display: inline-block;">
                            {{ $donation->transaction->type }}
                        </div>
                    </div>
                    <br>
                    <div>
                        <div style="width:150px;display: inline-block;">
                            Nominal Pembayaran
                        </div>
                        <div style="width:250px;display: inline-block;">
                            {{ $donation->transaction->amount }}
                        </div>
                    </div>
                    <br>
                    <div>
                        <div style="width:150px;display: inline-block;">
                            Waktu Pembayaran
                        </div>
                        <div style="width:250px;display: inline-block;">
                            {{ $donation->created_at }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



<!-- /Chatra {/literal} -->
