@extends('layouts.dashboard')

@section('title', 'Page Title')

@section('main')
    <div class="container container-white">
        <form class="form-horizontal" id="form" role="form" method="post" action="{{Request::url()}}/confirm">
            {{ csrf_field() }}
            <p class="title-dashboard" > Pembayaran </p>
            <div class="green-line"></div>
            <br>
            <b><h4>Nama Kelas : </h4></b>
            <p>{{ $course->nama_kelas }}</p>
            <hr>
            <b><h4>Masukan Nomor HP Anda :
                    @if($errors->has('phone_number'))
                        <span class="text-danger">{{ $errors->first('phone_number') }}</span>
                    @endif
                </h4></b>
            <input type="number" id="number" name="phone_number" class="form-control" value="{{ Auth::user()->nomor_wa }}"/>
            <hr>
            <b><h4>Pilih Metode Pembayaran : </h4></b>
            <select name="payment_method" class="form-control">
                <option value="ATM">ATM</option>
                <option value="ATM">Debit</option>
                <option value="ATM">Pulsa</option>
                <option value="ATM">Celengan</option>
            </select>
            <hr>
            <b><h4>Nominal pembayaran:
            @if($errors->has('amount'))
                <span class="text-danger">{{ $errors->first('amount') }}</span>
            @endif
            </h4></b>
            <input type="number" id="nominal" name="amount" class="form-control">
            <hr>
            <b><h4>Pilih jenis pembayaran: </h4></b>
            <div class="panel">
                <i>Anda dapat ikut patungan jika anda ingin mengikuti kelas yang bersangkutan, dan anda juga dapat memilih donasi jika hanya ingin membantu terciptanya kelas tanpa ikut kelas tersebut</i>
            </div>
            <input type="hidden" id="donation_type" name="donation_type" />
            <br/>
            <div class="row">
                <div class="col-sm-6">
                    <button id="btn_patungan" type="button" class="btn btn-primary btn-lg" onclick="onSubmit('patungan')" style="width: 100%"> Bayar sebagai Patungan</button>
                </div><div class="col-sm-6">
                    <button id="btn_donation" type="button" class="btn btn-warning btn-lg" onclick="onSubmit('donatur')" style="width: 100%"> Bayar sebagai Donasi</button>
                </div>
            </div>
        </form>
    </div>

    <script>
        function onSubmit(type) {
            $("#donation_type").val(type);
            $("#form").submit();
        }
    </script>
@endsection



<!-- /Chatra {/literal} -->
