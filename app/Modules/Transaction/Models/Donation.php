<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/9/2017
 * Time: 11:35
 */
namespace App\Modules\Transaction\Models;

class Donation extends \Illuminate\Database\Eloquent\Model {

    protected $table = "class_donations";
    protected $primaryKey = "donation_id";
    protected $fillable = ['user_id', 'course_id', 'transaction_code'];

    public function transaction() {
        return $this->hasOne('App\Modules\Transaction\Models\Transaction', 'transaction_code', 'transaction_code');
    }

    public function user() {
        return $this->belongsTo('App\Modules\User\Models\User', 'user_id');
    }

    public function course() {
        return $this->belongsTo('App\Modules\Course\Models\Course', 'course_id');
    }

}

?>
