<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/8/2017
 * Time: 17:19
 */
namespace App\Modules\Transaction\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {
    protected $primaryKey = 'transaction_code';
    protected $fillable = ['bank', 'payment_method', 'transaction_code', 'fraud_status', 'amount', 'phone_number', 'type'];

    public function donation() {
        return $this->belongsTo('App\Modules\Transaction\Models\Donation', 'transaction_code');
    }
}

?>