<?php

/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/4/2017
 * Time: 18:29
 */

namespace App\Modules\Transaction\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Transaction\Repositories\TransactionRepository;

class TransactionController extends Controller {

    private $transactionRepository;

    /**
     * UserController constructor.
     * @param $userRepository
     */
    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function paymentNotification(Request $request) {
        $this->transactionRepository->create($request);
        return json_encode($request);
    }

    public function changePaymentStatus($id, $status) {

    }

}
?>
