<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/9/2017
 * Time: 10:57
 */
namespace App\Modules\Transaction\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Course\Models\Course;
use App\Modules\Course\Repositories\CourseRepository;
use App\Modules\Transaction\Models\Transaction;
use App\Modules\Transaction\Repositories\DonationRepository;
use App\Modules\User\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class DonationController extends Controller {
    private $donationRepository;
    private $courseRepository;
    private $userRepository;
    private $log;

    public function __construct(DonationRepository $donationRepository, CourseRepository $courseRepository, UserRepository $userRepository)
    {
        $this->donationRepository = $donationRepository;
        $this->courseRepository = $courseRepository;
        $this->userRepository = $userRepository;
        $this->log = new Log();
        $this->middleware('auth');
        $this->middleware('active.user');
    }

    public function create($courseId) {
        $course = Course::findOrFail($courseId);
        
        return view("Transaction::donate", ['course' => $course]);
    }

    public function confirm($courseId, Request $request) {
        $this->validate($request, [
            'payment_method' => 'required',
            'amount' => 'required|integer',
            'donation_type' => 'required',
            'phone_number' => 'required'
        ]);
        $course = Course::find($courseId);
        $data = $request->all();

        // $isValid = $this->donationRepository->verifyTransactionCode($transactionCode);

            // create donation user and redirect to summary page
            $donation = $this->donationRepository->create($courseId, $data);
            $activity = $this->userRepository->createActivity(Auth::id(), 'melakukan donasi ' . $data['amount'] .' pada kelas ' . $course->nama_kelas);
            return view("Transaction::summary", ['donation' => $donation]);
    }

    public function summary($userDonation) {
        return view('Transaction::summary', ['donation' => $userDonation]);
    }

    public function join(Request $request){
      $data = $request->all();
      
      return $this->donationRepository->createDefaultDonation($data['user_id'],$data['course_id']);
    }

}

?>
