<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/8/2017
 * Time: 17:33
 */
namespace App\Modules\Transaction\Repositories;

use App\Modules\Transaction\Models\Transaction;
use App\Modules\Transaction\Models\Donation;
use App\Modules\User\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class DonationRepository implements DonationRepositoryInterface {

    public function all()
    {

    }

    public function getAllUserDonation($userid)
    {

    }

    public function getDonationById($donationId)
    {

    }

    public function create($courseId, array $attributes)
    {
        $transaction = new Transaction([
            'payment_method' => $attributes['payment_method'],
            'amount' => $attributes['amount'],
            'phone_number' => $attributes['phone_number'],
            'type' => $attributes['donation_type']
        ]);
        $transaction->save();

        Log::debug("attribute : " . json_encode($attributes));
        $donation = new Donation([
            'user_id' => Auth::id(),
            'course_id' => $courseId,
            'donation_type' => $attributes['donation_type'],
            'transaction_code' => $transaction->transaction_code
        ]);

        $donation->save();
        return $donation;
    }

    public function verifyTransactionCode($code) {
        $transaction = Transaction::find($code);
        if ($transaction) {
            return false;
        }
        $lastDonation = Donation::where('user_id',Auth::id())
            ->orderBy('created_at', 'desc')
            ->first();

//        if ($lastDonation != null && ($lastDonation->created_at->getTimestamp() - (Carbon::now()->getTimestamp())) < 1800) { // if user has made donation request in last 30 minutes
//            return false;
//        }
//        else {
//            return true;
//        }
        return true;
    }

    public function createDefaultDonation($userid, $courseId) {
        $transaction = new Transaction([
            'payment_method' => null,
            'amount' => 0,
            'phone_number' => 0,
            'type' => 'peserta',
            'fraud_status' => 'accepted'
        ]);
        $transaction->save();
        $donation = new Donation([
            'user_id' => $userid,
            'course_id' => $courseId,
            'donation_type' => 'gratis',
            'transaction_code' => $transaction->transaction_code
        ]);
        $donation->hasOne($transaction);
        $donation->save();
        return $donation;
    }

    public function addConfirmationImg($transactionCode, $confirmImg) {
        $donation = Transaction::where('transaction_code', $transactionCode)->first();
        if ($donation) {
            $donation->confirmation_img = $confirmImg;
            $donation->save();
        }
        return $donation;
    }



}
?>
