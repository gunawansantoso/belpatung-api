<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/8/2017
 * Time: 17:33
 */
namespace App\Modules\Transaction\Repositories;
use App\Modules\Transaction\Models\Transaction;
use App\Modules\Transaction\Repositories\TransactionRepositoriesInterface;
use Illuminate\Http\Request;

class TransactionRepository implements TransactionRepositoriesInterface {
    private $model;

    public function __construct(Transaction $model)
    {
        $this->model = $model;
    }


    public function all()
    {

    }

    public function getTransactionById($id)
    {
        // TODO: Implement getTransactionById() method.
    }

    public function getTransactionByUserId($userid)
    {
        // TODO: Implement getTransactionByUserId() method.
    }

    public function create(Request $request)
    {
        $stats = $request->input('fraud_status');
        switch ($stats) {
            case 'accept' :

        }
        $transaction = new Transaction();
        $transaction->payment_id = $request->transaction_id;
        $transaction->payment_type = $request->payment_type;
        $transaction->transaction_time = $request->transaction_time;
        $transaction->amount = $request->gross_amount;
        $transaction->status_message = $request->status_message;
        $transaction->saveOrFail();
    }

    public function changeStatus($id, $status)
    {
        $this->model->find($id)
            ->update(['fraud_status' => $status]);
    }

}
?>