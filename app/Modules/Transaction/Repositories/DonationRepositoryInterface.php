<?php
/**
 * this controller handle all action of class donation class
 */
namespace App\Modules\Transaction\Repositories;

interface DonationRepositoryInterface {

    /**
     * @return mixed
     * return all user donation
     */
    public function all();

    /**
     * @param $userid
     * @return mixed
     * get list of donation of specific user
     */
    public function getAllUserDonation($userid);

    /**
     * @param $donationId
     * @return mixed
     * get specific donation by id
     */
    public function getDonationById($donationId);

    /**
     * @param $attributes
     * @return mixed
     * create user donation
     */
    public function create($courseId, array $attributes);

    /**
     * @param $code
     * @return mixed
     * check if transaction code is not unique
     */
    public function verifyTransactionCode($code);
}
?>