<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/8/2017
 * Time: 17:31
 */
namespace App\Modules\Transaction\Repositories;

use Illuminate\Http\Request;

interface TransactionRepositoriesInterface {

    public function all();

    public function getTransactionById($id);

    public function getTransactionByUserId($userid);

    public function create(Request $request);

    public function changeStatus($id, $status);
}
?>