<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/3/2017
 * Time: 14:37
 */

Route::group(['prefix' => 'transactions', 'namespace' => 'App\Modules\Transaction\Controllers'], function () {

    Route::post('/payment/notification', 'TransactionController@paymentNotification');
    Route::get('/donation/{course_id}', 'DonationController@create')->where('course_id', '[0-9]+');
    Route::post('/donation/{course_id}/confirm', 'DonationController@confirm')->where('course_id', '[0-9]+');
    Route::get('/donation/{course_id}/summary', 'DonationController@summary')->where('course_id','[0-9]+');
    Route::post('/join','DonationController@join');
});

?>
