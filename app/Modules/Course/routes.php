<?php

Route::group(['prefix' => 'courses', 'namespace' => 'App\Modules\Course\Controllers'], function () {
  
    Route::get('/create', ['as' => 'courses.create', 'uses' => 'CourseController@create'])->middleware('active.user');;
    Route::get('/userimage/{filename}', ['uses' => 'CourseController@getCourseImage','as' => 'course.image']);
    Route::get('/getCoursesList', ['as' => 'courses.getCoursesList', 'uses' => 'CourseController@getCoursesList']);
    Route::get('/{link_kelas}/getCoursesDetail', ['as' => 'courses.getCoursesDetail', 'uses' => 'CourseController@getCoursesDetail']);
    Route::get('/{link_kelas}', 'CourseController@detail');
    Route::get('/{link_kelas}/update','CourseController@update')->middleware('active.user');
    Route::get('/search/{keyword}/{lokasi}/{tanggal_mulai}/{tanggal_selesai}', ['as' => 'courses.search', 'uses' => 'CourseController@search']);
    Route::post('/delete/{id}','CourseController@delete');
    Route::post('/publish/{user_id}/{course_id}','CourseController@publish')->where(['user_id' => '[0-9]+'])->where(['course_id' => '[0-9]+']);
    Route::post('/store', 'CourseController@store');

});

?>
