<?php
/**
 * Created by PhpStorm.
 * course: lightmire
 * Date: 2/4/2017
 * Time: 18:54
 */

namespace App\Modules\Course\Repositories;

use App\Modules\Course\Models\Participant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class ParticipantRepository
{
    private $model;

    /**
     * courseRepository constructor.
     * @param $model
     */
    public function __construct(Participant $model)
    {
        $this->model = $model;
    }


    public function getParticipants($class_id)
    {
        return $this->model->where('class_id',$class_id)->get();
    }

    




}
