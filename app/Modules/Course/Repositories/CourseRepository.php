<?php
/**
 * Created by PhpStorm.
 * course: lightmire
 * Date: 2/4/2017
 * Time: 18:54
 */

namespace App\Modules\Course\Repositories;

use App\Modules\Course\Models\Course;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Speaker;
use App\Modules\Transaction\Models\Donation;
use App\Modules\Transaction\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class CourseRepository
{
    private $model;

    /**
     * courseRepository constructor.
     * @param $model
     */
    public function __construct(Course $model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function getByLinkKelas($link_kelas)
    {
        $data = $this->model->where('link_kelas',$link_kelas)->get();
        if(count($data) > 0){
          return true;
        }
        return false;
    }

    public function getById($link_kelas){
        return DB::table('courses')
                ->join('users', 'courses.user_id','=','users.id')
                ->where('link_kelas',$link_kelas)
                ->select('courses.*', 'users.isverified')
                ->first();
    }

    public function getDetailUpdate($link_kelas){
        return $this->model->where('link_kelas',$link_kelas)->first();
    }
    public function create(Request $request, $filename)
    {

      Course::create([
        'user_id'=>Auth::id(),
        'nama_kelas'=>$request->nama_kelas,
        'kategori'=>$request->kategori,
        'target_dana'=>$request->target_dana,
        'total_dana'=>0,
        'posisi_user'=>$request->posisi_user,
        'estimasi_peserta'=>$request->estimasi_peserta,
        'link_kelas'=>$request->link_kelas,
        'link_foto'=>$filename,
        'tanggal_kelas'=>$request->tanggal_kelas,
        'deadline_pembayaran'=>$request->deadline_pembayaran,
        'lokasi'=>$request->lokasi,
        'deskripsi'=>$request->deskripsi,
        'status'=>'Belum Fix'
      ]);
      $class_id = $this->model->where('link_kelas',$request->link_kelas)->first();
      Speaker::create([
          'user_id'=>$request->pembicara,
          'class_id'=>$class_id->id
      ]);

    }
    public function publishConstraint($user_id, $course_id){

        $user = User::find($user_id);

          $donation =  DB::table('courses')
                      ->join('class_donations','courses.id','=','class_donations.course_id')
                      ->join('transactions', 'class_donations.transaction_code','=','transactions.transaction_code')
                      ->where('class_donations.user_id',$user_id)
                      ->where('courses.user_id',$user_id)
                      ->where('course_id',$course_id)
                      ->where('fraud_status','accepted')->first();

          if($user->isverified && count($donation) == 1){

              DB::table('courses')
              ->where('id',$course_id)
              ->update(['ispublished'=> TRUE]);
          }
    }

    public function search($keyword, $lokasi, $tanggal_mulai, $tanggal_selesai){
        $lokasi = $lokasi == 'all' ? '' : $lokasi;
        $keyword = $keyword == 'all' ? '' : $keyword;

        if($tanggal_mulai != 'all' && $tanggal_selesai != 'all'){
          return Course::where('nama_kelas', 'like', '%'.$keyword.'%')
                        ->where('lokasi','like','%'.$lokasi.'%')
                        ->where('tanggal_kelas','>=',$tanggal_mulai)
                        ->where('tanggal_kelas', '<=',$tanggal_selesai)
                        ->where('ispublished',true)->paginate(9);
        }
        return Course::where('nama_kelas', 'like', '%'.$keyword.'%')
                      ->where('lokasi','like','%'.$lokasi.'%')
                      ->where('ispublished',true)->paginate(9);
    }


    public function update($id, array $attributes)
    {
      return $this->model->find($id)
          ->update($attributes);
    }

    public function delete($id)
    {
      $course = $this->model->find($id);
      $course->delete();
    }

    public function getCoursesById($id) {
        return Course::find($id);
    }

}
