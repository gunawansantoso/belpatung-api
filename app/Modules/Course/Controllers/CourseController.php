<?php
/**
 * Created by PhpStorm.
 * course: lightmire
 * Date: 2/4/2017
 * Time: 18:29
 */
namespace App\Modules\Course\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Course\Repositories\CourseRepository;
use App\Modules\User\Repositories\UserRepository;
use App\Modules\Course\Repositories\ParticipantRepository;
use Auth;
use DB;
use App\Modules\Course\Models\Course;
use App\Modules\Transaction\Models\Transaction;
use App\Modules\Transaction\Models\Donation;
use App\Modules\User\Models\Speaker;
use App\Modules\User\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Image;

class CourseController extends Controller {

    private $courseRepository;
    private $userRepository;
    /**
     * courseController constructor.
     * @param $courseRepository
     */
    public function __construct(CourseRepository $courseRepository, UserRepository $userRepository)
    {
        $this->courseRepository = $courseRepository;
        $this->userRepository = $userRepository;
        $this->middleware('active.user');
    }


    public function index() {
        return View('Course::index');
    }

    public function create(){
        $speakers = User::where('isspeaker', true)->get();
        $kategori = DB::table('kategori')->get();
        $locations = DB::table('provinces')->get();
        if(!Auth::user()->isverified){
            return redirect('/verification');
        }
        return View('Course::create', ['speakers'=>$speakers,'kategori'=>$kategori,'locations'=>$locations]);
    }

    public function detail($link_kelas){
      // $courses = $this->courseRepository->getById($id);
      // if(count($courses) == 0){
      //   return view('404');
      // }
      // if(!$courses->ispublished){
      //   if(Auth::id() == $courses->user_id){
      //     return redirect('/transactions/donation/'.$courses->id);
      //   }
      //   return view('404');
      // }
      // $participants = DB::table('class_donations')
      //                 ->join('transactions','transactions.transaction_code','=','class_donations.transaction_code')
      //                 ->join('users','user_id','=','id')
      //                 ->where('course_id',$courses->id)
      //                 ->select('user_id','name','link_foto_profile','amount','type')->get();

      // return View('Course::image', ['courses'=>$courses,'participants'=>$participants]);
      // return View('Course::image');
      $course = Course::where('link_kelas',$link_kelas)->first();
      if(count($course) == 0){
        return view('404');
      }
      if(!$course->ispublished){
        if(Auth::id() == $course->user_id){
          return redirect('/transactions/donation/'.$course->id);
        }
        return view('404');
      }

      $participants = DB::table('class_donations')
                      ->join('transactions','transactions.transaction_code','=','class_donations.transaction_code')
                      ->join('users','user_id','=','id')
                      ->where('course_id',$course->id)
                      ->select('user_id','name','link_foto_profile','amount','type')->get();

      return View('Course::detail',['course'=>$course,'participants'=>$participants]);
    }

    public function update($link_kelas){
      $courses = $this->courseRepository->getDetailUpdate($link_kelas);
      if(!Auth::user()->isadmin && Auth::id() != $courses->user_id){
        return view('404');
      }
      $speakers = User::where('isspeaker', true)->get();
      $current_speaker= Speaker::where('class_id',$courses->id)->first();
      $locations = DB::table('provinces')->get();
        $kategori = DB::table('kategori')->get();
      return View('Course::update',['courses'=>$courses,'locations'=>$locations,'kategori'=>$kategori, 'speakers'=>$speakers, 'current_speaker'=>$current_speaker]);
    }


    public function delete($id){
        $this->courseRepository->delete($id);
        return  "Course " . $id . " deleted";
    }

    public function getCoursesList(){
      $courses = Course::where('ispublished',true)->get();
      return $courses;
    }

    public function store(Request $request){
      if($request->action === 'update'){
        $file =$request->file('image');
        if($file){
          $file =$request->file('image');
          $filename = $request->link_kelas .'.'. $file->getClientOriginalExtension();
          $request->offsetSet('link_foto',$filename);
          Image::make($file)->resize(300,300)->save(public_path('/uploads/'.$filename));
        }
        $this->courseRepository->update($request->id,$request->all());
        Speaker::where('class_id',$request->id)
        ->update(['user_id'=> $request->pembicara]);
        $this->userRepository->createActivity(Auth::id(),'Mengupdate detail kelas '.$request->nama_kelas);
        return redirect('courses/'.$request->link_kelas);
      }
      $file = $request->file('image');
      $filename = $request->link_kelas .'.'. $file->getClientOriginalExtension();
      $link_kelas = str_replace(' ', '', $request->link_kelas);
      $request->offsetSet('link_kelas',$link_kelas);
      if($this->courseRepository->getByLinkKelas($request->link_kelas)){
        return "Link kelas sudah ada";
      }
      if($file){
         Image::make($file)->resize(300,300)->save(public_path('/uploads/'.$filename));
      }
      $this->courseRepository->create($request, $filename);
      // return redirect('courses/'.$request->link_kelas);
      return redirect('courses/'.$request->id);

    }

    public function getCoursesDetail($link_kelas){
        return $this->courseRepository->getById($link_kelas);
    }

    public function publish($user_id, $course_id){
      $this->courseRepository->publishConstraint($user_id, $course_id);
    }

    public function search($keyword, $lokasi, $tanggal_mulai, $tanggal_selesai){
        $results = $this->courseRepository->search($keyword,$lokasi,$tanggal_mulai,$tanggal_selesai);
        $locations = DB::table('provinces')->get();
        $participants = DB::table('class_donations')
                      ->join('transactions','transactions.transaction_code','=','class_donations.transaction_code')
                      ->join('users','user_id','=','id')
                      // ->where('course_id',$course->id)
                      ->select('course_id','user_id','name','link_foto_profile','amount','type')->get();
        $users=User::all();
        return view('search',['results'=>$results, 'locations'=>$locations,'participants'=>$participants,'users'=>$users,'keyword'=>$keyword]);
    }

}
?>
