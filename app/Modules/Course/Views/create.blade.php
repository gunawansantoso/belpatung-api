@extends('layouts.dashboard')
@section('title','Create Course')
@section('main')

	<div class="container container-white">
		<p class="title-dashboard" > Buat Kelas Saya </p>
		<div class="green-line"></div>
		<br>
			<form role="form" method="POST" action="{{url('/courses/store')}}" enctype="multipart/form-data">
				{{ csrf_field() }}
			<div class="form-group">
				<label for="nama_kelas">Nama kelas</label>
				<input id="nama_kelas" type="text" class="form-control" name="nama_kelas" required >
			</div>

		<div class="form-group">
			<label for="nama_kelas">Kategori</label>
				<select  id="kategori" class="form-control" name="kategori" required>
					<?php
						for($i = 0; $i < count($kategori); $i++){
							echo '<option value="'.$kategori[$i]->nama.'">'.$kategori[$i]->nama.'</option>';
						}
					?>
				</select>

		</div>

			<div class="form-group">
				<label for="dana">Target Dana</label>
			 <input id="target_dana" type="text" class="form-control" name="target_dana" required>
			</div>
			<div class="form-group">
				<label for="posisi">Posisi Saya Sebagai</label>
				<select name="posisi_user" class="form-control"  required>
					<option  value="peserta"> Sebagai Peserta</option>
					<option  value="po"> Sebagai Project Officer</option>
					<?php if(Auth::user()->isspeaker){
						echo '<option  value="pembicara"> Sebagai Pembicara</option>';
					}?>
				</select>
			</div>
			 <div class="form-group">
				<label for="peserta">Estimasi Peserta kelas</label>
				<input type="number" class="form-control" name="estimasi_peserta" required>
			</div>
			<div class="form-group">
				<label for="link">Link kelas</label>
				<input type="text" class="form-control" id="link" name="link_kelas" placeholder="Link yang akan ditampilkan di Url" required>
			</div>
			<div class="form-group">
				<label for="pembicara">Nama Pembicara</label>
				<select id="pembicara"  class="form-control" name="pembicara" required>

							@foreach($speakers as $speaker)
								<option value={{ $speaker->id }}>{{$speaker->name}}</option>

							@endforeach

				</select>
			</div>



			<div class="checkbox">
				<label><input type="checkbox">Tambahkan saya sebagai pembicara</label>
			</div>
			<div class="form-group">
			 <label for="lokasi">Lokasi</label>

			 <select  name="lokasi"class="form-control" id="lokasi"  required>
				 <?php
					 for($i = 0; $i < count($locations); $i++){
						 echo '<option value="'.$locations[$i]->nama.'">'.$locations[$i]->nama.'</option>';
					 }
				 ?>
			 </select>
		 </div>
			<div class="form-group">
			 <label for="tanggal_kelas">Tanggal Mulai</label>
			 <input type="date" name="tanggal_kelas"class="form-control" id="tanggal_kelas"  required>
		 </div>
			<div class="form-group">
				<label for="deadline">Deadline Pembayaran</label>
				<input type="date" name="deadline_pembayaran" class="form-control" id="deadline"  required>
			</div>

			<div class="form-group">
				<label for="foto">Tambahkan foto</label>
				<input type="file" class="form-control"name="image" id="foto"  required>
			</div>

			<div class="form-group">
				<label for="detail">Deskripsi kelas</label>
				<textarea class="form-control textarea" id="detail" name="deskripsi" required autofocus> </textarea>
			</div>

			<button type="submit" class="btn btn-primary btn-lg">Buat kelas saya</button>
		</form>
	</div>


@endsection
<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
	<style>
	textarea
	{
		width:100%;
	}
	</style>
	<script>
		tinymce.init({
		selector: '.textarea',
		theme: 'modern',
		height: 300,
		plugins: [
			'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
			'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			'save table contextmenu directionality emoticons template paste textcolor'
		],
		content_css: 'css/content.css',
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  print preview media fullpage | forecolor backcolor emoticons'
	});
	</script>
