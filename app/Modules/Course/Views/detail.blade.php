@extends('layouts.app')
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection
@section('content')
<div id="section-single-kelas" class="section">
  <div class="content-section">
    <div class="row">
      <div class="col-sm-8"  >
        <div class="title" style="text-align: left;color: #333333;">
          <h1><b> {{$course->nama_kelas}} - (kelas {{$course->kategori}})</b></h1>
        </div>
        <a href=""><img src="{{asset('belpatung-api/public/uploads')}}/{{ $course->link_foto }}" style="width: 100%"></a>
        <br><br>
        <div class="row">
          <div class="col-sm-3">
            <button class="btn btn-primary btn-md" style="width: 100%"><i class="fa fa-facebook"></i> Share ke Facebook </button>
          </div>
          <div class="col-sm-3">
            <button class="btn btn-info btn-md" style="width: 100%"><i class="fa fa-twitter"></i> Share ke Twitter </button>
          </div>
          <div class="col-sm-3">
            <button class="btn btn-danger btn-md" style="width: 100%"><i class="fa fa-google-plus"></i> Share ke Google +</button>
          </div>
        </div>
        <br>
        <div >
        {{$course->deskripsi}}
        </div>
        <br><br>
        <?php $registered=$participants->where('user_id',Auth::user()->id)->first(); ?>
        @if(!$registered)
        <a type="button" href="{{url('transactions/donation')}}/{{$course->id}}" class="btn btn-lg btn-green" style="width: 100%"><h4><b>ikut Patungan Sekarang</b></h4></a>
        <br><br>
        <center><p>atau</p></center>
        @else
        <div class="btn-lg btn-outline-success" style="width: 100%;border:1px solid #329666;text-align: center;color: #329666"><h4><b>Anda Telah Terdaftar Sebagai {{$registered->type==='patungan'?'Peserta':'Donatur'}} <i class="fa fa-check"></i></b></h4></div>
        @endif
        <br>
        <a href="{{ route('courses.create') }}" type="button" class="btn btn-lg btn-primary" style="width: 100%"><h4><b>Buat Kelas Anda</b></h4></a>
      </div>
      <div class="col-sm-4">
        <h1 style="color:#333333;font-family: montserrat"><b> Rp {{number_format($course->total_dana,2, ',', '.')}}</b></h1>
        <p style="font-size: 1.2em">Donasi terkumpul</p>
        <div class="progress">
          <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{round($course->total_dana/$course->target_dana,2)*100}}"
          aria-valuemin="0" aria-valuemax="100" style="width:40%">
            {{round($course->total_dana/$course->target_dana,2)*100}}% Complete (success)
          </div>
        </div>
        <hr>
        <div class="panel panel-primary">
            <div class="panel-heading"><h4> Informasi Umum</h4></div>
            <div class="panel-body">
              Kelas dibuka pada :
              <h2><b> <i class="fa fa-calendar"></i> {{date('d M Y',strtotime($course->tanggal_kelas))}} </b></h2>
              <hr>
              Kategori kelas :
              <h2><b> <i class="fa fa-pencil"></i> {{$course->kategori}} </b></h2>
              <hr>
              Informasi Umum:
              <br><br>
              <p>Lorem Ipsum Dolor Sit Amet</p>
            </div>
          </div>
          <hr>         
        <div id="data-donatur">
          <?php $donasi = $participants->where('type','donatur'); ?>
          <h1 style="color:#333333;font-family: montserrat"><b>{{count($donasi)}} </b></h1>
          <p style="font-size: 1.2em">Jumlah Donatur Saat Ini</p>
          <div class="row">
            @foreach($donasi as $donatur)
            <div class="col-peserta">
              <img src="{{$donatur->link_foto_profile}}" alt="" class="img-circle" style="width: 100%">
              <br><br>
            </div>
            @endforeach
            
          </div>
          @if(count($donasi)>0)
          <center><button type="button" class="btn btn-warning" id="btn-hover-donatur">Lihat lebih banyak</button></center>
          @endif
        </div>
        <hr>
        <div id="data-peserta">
          <?php $patungan = $participants->where('type','patungan'); ?>
          <h1 style="color:#333333;font-family: montserrat"><b> {{count($patungan)}} </b></h1>
          <!-- <h1 style="color:#333333;font-family: montserrat"><b> {{count($participants)}} </b></h1> -->
          <p style="font-size: 1.2em ">Jumlah Peserta Saat Ini</p>
          <div class="row">
            @foreach($patungan as $peserta)
            <div class="col-peserta">
              <img src="{{$peserta->link_foto_profile}}" alt="" class="img-circle" style="width: 100%">
              <br><br>
            </div>
            @endforeach
          </div>
          @if(count($patungan)>0)
          <center><button type="button" class="btn btn-green" id="btn-hover-peserta">Lihat lebih banyak</button></center>
          @endif
        </div>
        <hr>
        <div id="data-respon">
          <h1 style="color:#333333;font-family: montserrat"><b> 50 </b></h1>
          <p style="font-size: 1.2em ">Orang Membicarakan Ini</p>
          <div id="content-respon">
            <!-- Left-aligned -->
            <div class="media">
              <div class="media-left">
                <img src="{{asset('belpatung-api/public/images/img_avatar1.png')}}" class="media-object img-circle" style="width:40px">
              </div>
              <div class="media-body">
                <h4 class="media-heading">John Doe</h4>
                <p>Lorem ipsum...</p>
              </div>
              <br>
            </div>
            <!-- Left-aligned -->
            <div class="media">
              <div class="media-left">
                <img src="{{asset('belpatung-api/public/images/img_avatar1.png')}}" class="media-object img-circle" style="width:40px">
              </div>
              <div class="media-body">
                <h4 class="media-heading">John Doe</h4>
                <p>Lorem ipsum...</p>
              </div>
              <br>
            </div>
            <!-- Left-aligned -->
            <div class="media">
              <div class="media-left">
                <img src="{{asset('belpatung-api/public/images/img_avatar1.png')}}" class="media-object img-circle" style="width:40px">
              </div>
              <div class="media-body">
                <h4 class="media-heading">John Doe</h4>
                <p>Lorem ipsum...</p>
              </div>
              <br>
            </div>
            <center><button type="button" class="btn btn-info" id="btn-hover-peserta">Lihat lebih banyak</button></center>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

});


</script>
