@extends('layouts.app')

@section('content')

<div id="courses_list">

</div>

@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

  $.getJSON( "/courses/getCoursesList", function( data ) {
  
      var content = "";
      for(var i = 0; i < data.length; i++){
      content += '<div class="col-md-6 col-md-offset-3"><div class="panel panel-default"><div class="panel-heading">';
      content += '<span>'+data[i]['nama_kelas']+'</span>';
      content += '<span class="pull-right">'+data[i]['status']+'</span>';
      content += '</div><div class="panel-body">';
      content += '<img src="/uploads/'+data[i]['link_foto']+'" alt="" class="img-responsive">';
      content += data[i]['deskripsi'] + '</div>';
      content += '<a href="/courses/'+data[i]['link_kelas']+'" class="btn btn-success">Detail</a></div></div>';
    }
      $('#courses_list').append(content);
  });
});
</script>
