<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <title>Beranda</title>

    <meta property="fb:app_id"        content="1824061621201938" />
    <meta property="og:title"         content="<?php echo $courses->nama_kelas;?>" />
    <meta property="og:url"           content="http://belpatung-api.herokuapp.com/courses/<?php echo $courses->link_kelas;?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:description"   content="Website terkece sepanjang masa" />
    <meta property="og:image"         content="http://belpatung-api.herokuapp.com/uploads/<?php echo $courses->link_foto;?>" />
    <meta property="og:image:width"          content="200" />
    <meta property="og:image:height"         content="200" />

    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href={{ asset("css/style.css")}}>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Damion">
    <script src="{{ asset('bootstrap/js/bootstrap.min.js')}}" type="text/javascript" charset="utf-8" async defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src=" {{ asset('js/myjs.js') }}" type="text/javascript" charset="utf-8" async defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
    <div id="header-1">
        <div id="nav">
            <div id="nav-logo"><a href="#"> LOGO HERE </a></div>
            <div class="nav-menu" id="navmenu1"> CARA KERJA </div>
            <div class="nav-menu" id="navmenu2"> TENTANG KAMI </div>
            <div class="nav-menu" id="navmenu3"> BLOG </div>
            <div class="nav-menu" id="navmenu4"> KALKEL </div>
            <div id="nav-flag"> <img src="{{ asset('images/flag_indo.png') }}"></div>
            @yield('navbar')
            @if(Auth::check())
                <a href="{{ url('/logout') }}" class="btn btn-green"
                   onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Logout
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            @endif

        </div>
    </div>

    <!-- Scripts -->
    <?php
      $contentDonatur = "";
      $counter2 = 0;
      $contentPeserta = "";
      $counter = 0;
      $total_dana = 0;
      for($i = 0; $i < count($participants); $i++){
        $total_dana += $participants[$i]->amount;
        if($participants[$i]->type == 'donatur'){
          $contentDonatur .= '<div class="col-peserta">';
          if($participants[$i]->link_foto_profile == null){
            $contentDonatur .= '<a title="'.$participants[$i]->name.'" href="/users/'.$participants[$i]->user_id.'"><img src="/images/img_avatar1.png" alt="" class="img-circle" style="width: 100%"></a>';
          }
          else{
            $contentDonatur .= '<a title="'.$participants[$i]->name.'" href="/users/'.$participants[$i]->user_id.'"><img src="'.$participants[$i]->link_foto_profile.'" alt="" class="img-circle" style="width: 100%"></a>';
          }
          $contentDonatur .= '<br><br></div>';
          $counter2++;
        }
        else if($participants[$i]->type == 'peserta'){
          $contentPeserta .= '<div class="col-peserta">';
          if($participants[$i]->link_foto_profile == null){
            $contentPeserta .= '<a title="'.$participants[$i]->name.'" href="/users/'.$participants[$i]->user_id.'"><img src="/images/img_avatar1.png" alt="" class="img-circle" style="width: 100%"></a>';
          }
          else{
            $contentPeserta .= '<a title="'.$participants[$i]->name.'" href="/users/'.$participants[$i]->user_id.'"><img src="'.$participants[$i]->link_foto_profile.'" alt="" class="img-circle" style="width: 100%"></a>';
          }
          $contentPeserta .= '<br><br></div>';
          $counter++;
        }
      }
    ?>

    <div id="section-single-kelas" class="section">
  		<div class="content-section">
  			<div class="row">
  				<div class="col-sm-8"  >
  					<div class="title" style="text-align: left;color: #333333;">
  						<h1><b><?php echo $courses->nama_kelas;?></b></h1>
  					</div>
  					<a href=""><img src="/uploads/<?php echo $courses->link_foto;?>" style="width: 100%"></a>
  					<br><br>
  					<div class="row">
  						<div class="col-sm-3">
  							  <div class="fb-like" data-href="http://belpatung-api.herokuapp.com/courses/<?php echo $courses->link_kelas;?>" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
  						</div>
  					</div>
  					<br>
  					<div>
  					<?php echo $courses->deskripsi;?>
  					</div>
  					<br><br>
            <?php

             if($courses->deadline_pembayaran < date("Y/m/d")){
                if($courses->target_dana == 0){
                    echo '<form action="/transactions/join" method="POST">';
                    echo '<input type="hidden" name="course_id" value="'.$courses->id.'">';
                    echo '<input type="hidden" name="user_id" value="'.Auth::id().'">';
                    echo '<button class="btn btn-lg btn-green" style="width: 100%"><h4><b>Daftar jadi peserta</b></h4></button>';
                    echo '</form>';
                }else{
                echo '<a href="/transactions/donation/'.$courses->id.'" class="btn btn-lg btn-green" style="width: 100%"><h4><b>ikut Patungan Sekarang</b></h4></a>';
                }
              }
              else {
                    echo '<a href="#" class="btn btn-lg btn-green" style="width: 100%"><h4><b>Masa Patungan Sudah Selesai </b></h4></a>';
              }

            ?>
  					<br><br>
  					<center><p>atau</p></center>
  					<br>
  					<a href="/courses/create" class="btn btn-lg btn-primary" style="width: 100%"><h4><b>Buat Kelas Anda</b></h4></a>
  				</div>
  				<div class="col-sm-4">

  					<h1 style="color:#333333;font-family: montserrat"><b> Rp. <?php echo $courses->target_dana?> </b></h1>
  					<p style="font-size: 1.2em">Donasi terkumpul</p>
  					<div class="progress">

              <?php
              $persentaseDana = 0;
              if($courses->target_dana != 0){
               $persentaseDana = ( $total_dana / $courses->target_dana) * 100;
             }

               ?>
  					  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo $persentaseDana;?>"
  					  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo floor($persentaseDana);?>%">
  					    <?php echo floor($persentaseDana) . "%";?>
  					  </div>
  					</div>
  					<hr>
  					<div class="panel panel-primary">
  				      <div class="panel-heading"><h4> Informasi Umum</h4></div>
  				      <div class="panel-body">
  				      	Kelas dibuka pada :
  				      	<h2><b> <i class="fa fa-calendar"></i> <?php echo $courses->tanggal_kelas;?> </b></h2>
  				      	<hr>
  				      	Kategori kelas :
  				      	<h2><b> <i class="fa fa-pencil"></i> <?php echo $courses->kategori?></b></h2>
  				      	<hr>
  				      	Informasi Umum:
  				      	<br><br>
  				      	<p>Lorem Ipsum Dolor Sit Amet</p>
  				      </div>
  				    </div>
  				    <hr>
  					<div id="data-donatur">
  						<h1 style="color:#333333;font-family: montserrat"><b> 	<?php echo $counter2;?> </b></h1>
  						<p style="font-size: 1.2em">Jumlah Donatur Saat Ini</p>
  						<div class="row">
  							<?php echo $contentDonatur;?>
  						</div>
  						<center><button type="button" class="btn btn-warning" id="btn-hover-donatur">Lihat lebih banyak</button></center>
  					</div>
  					<hr>
  					<div id="data-peserta">
  						<h1 style="color:#333333;font-family: montserrat"><b> <?php echo $counter;?> </b></h1>
  						<p style="font-size: 1.2em ">Jumlah Peserta Saat Ini</p>
  						<div class="row">
                <?php
                  echo $contentPeserta;
                ?>



  						</div>
  						<center><button type="button" class="btn btn-green" id="btn-hover-peserta">Lihat lebih banyak</button></center>
  					</div>
  					<hr>

  				</div>
  			</div>
  		</div>
  	</div>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8&appId=1824061621201938";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    <script type="text/javascript">
    $(document).ready(function(){

    });
    </script>
</body>
</html>
