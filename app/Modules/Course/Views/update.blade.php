@extends('layouts.dashboard')

@section('content')


  <div class="container container-white">
    <p class="title-dashboard" > Update Kelas </p>
    <div class="green-line"></div>
    <br>
    <form class="form-horizontal" role="form" method="POST" action="/courses/store" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input id="id" name="id" type="hidden" value="<?php echo $courses->id;?>">
          <input id="id" name="user_id" type="hidden" value="<?php echo $courses->user_id;?>">
        <input id="link_kelas" name="link_kelas" type="hidden" value="<?php echo $courses->link_kelas;?>">
        <input id="link_foto" name="link_foto" type="hidden" value="<?php echo $courses->link_foto;?>">
        <input id="action" name="action" type="hidden" value="update">
        <div class="form-group">
            <label for="nama_kelas" class="control-label">Nama Kelas</label>


                <input id="nama_kelas" type="text" class="form-control" name="nama_kelas" value="<?php echo $courses->nama_kelas;?>" required autofocus>

        </div>

        <div class="form-group">
            <label for="kategori" class="control-label">Kategori</label>

            <select  id="kategori" class="form-control" name="kategori" required>
              <?php
                for($i = 0; $i < count($kategori); $i++){
                  if($courses->kategori != $kategori[$i]->nama){
                    echo '<option value="'.$kategori[$i]->nama.'">'.$kategori[$i]->nama.'</option>';
                  }else{
                    echo '<option value="'.$kategori[$i]->nama.'" selected>'.$kategori[$i]->nama.'</option>';
                  }
                }
              ?>
            </select>

        </div>

        <div class="form-group">
            <label for="target_dana" class="control-label">Target Dana</label>
                <input id="target_dana" type="text" class="form-control" name="target_dana" value="<?php echo $courses->target_dana;?>" required autofocus>
        </div>

        <div class="form-group">
            <label for="posisi_user" class="control-label">Posisi User</label>
            <select name="posisi_user" required class="form-control">
              <?php if($courses->posisi_user === 'peserta'){
                echo '  <option  value="peserta" selected> Sebagai Peserta</option>';
              }else{
                echo '<option  value="peserta"> Sebagai Peserta</option>';
              }
              if($courses->posisi_user === 'po'){
                echo '<option  value="po" selected> Sebagai Project Officer</option>';
              }else{
                echo '<option  value="po"> Sebagai Project Officer</option>';
              }
              if($courses->posisi_user === 'pembicara'){
                  echo '<option  value="pembicara" selected> Sebagai Pembicara</option>';
              }else {
                  echo '<option  value="pembicara"> Sebagai Pembicara</option>';
              }
              ?>

            </select>


        </div>

        <div class="form-group">
            <label for="estimasi_peserta" class="control-label">Estimasi Peserta</label>


                <input id="estimasi_peserta" type="text" class="form-control" value="<?php echo $courses->estimasi_peserta;?>" name="estimasi_peserta" required autofocus>

        </div>


        <div class="form-group">
            <label for="tanggal_kelas" class="control-label">Tanggal Kelas</label>
            <input id="tanggal_kelas" type="date" class="form-control" name="tanggal_kelas" value="<?php echo $courses->tanggal_kelas;?>" required autofocus>
        </div>

        <div class="form-group">
            <label for="deadline_pembayaran" class="control-label">Deadline Pembayaran</label>
                <input id="deadline_pembayaran" type="date" class="form-control" name="deadline_pembayaran" value="<?php echo $courses->deadline_pembayaran;?>" required autofocus>
        </div>
        <div class="form-group">
            <label for="pembicara" class="control-label">Pembicara</label>
                <select id="pembicara"  class="form-control" name="pembicara" required autofocus>
                    @foreach($speakers as $speaker)
                        @if($current_speaker->user_id != $speaker->id)
                          <option value={{ $speaker->id }}>{{$speaker->name}}</option>
                        @else
                          <option value={{ $speaker->id }} selected>{{$speaker->name}}</option>
                        @endif
                    @endforeach
                </select>
        </div>

        <div class="form-group">
            <label for="lokasi" class="control-label">Lokasi</label>

            <select  name="lokasi"class="form-control" id="lokasi"  required>
              <?php
                for($i = 0; $i < count($locations); $i++){
                  if($locations[$i]->nama != $courses->lokasi){
                    echo '<option value="'.$locations[$i]->nama.'">'.$locations[$i]->nama.'</option>';
                  }else{
                      echo '<option value="'.$locations[$i]->nama.'" selected>'.$locations[$i]->nama.'</option>';
                  }
                }
              ?>
            </select>

        </div>

        <div class="form-group">
            <label for="image" class="control-label">Course Photo</label>

                <input id="image" type="file" class="form-control" name="image" autofocus>

        </div>

        <div class="form-group">
            <label for="deskripsi" class="control-label">Deskripsi</label>
                <textarea id="deskripsi" class="form-control textarea" name="deskripsi" required autofocus><?php echo $courses->deskripsi;?> </textarea>

        </div>
        <button type="submit" class="btn btn-primary btn-lg">Update</button>
    </form>

  </div>

@endsection
<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
	<style>
	textarea
	{
		width:100%;
	}
	</style>
	<script>
		tinymce.init({
		selector: '.textarea',
		theme: 'modern',
		height: 300,
		plugins: [
			'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
			'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
			'save table contextmenu directionality emoticons template paste textcolor'
		],
		content_css: 'css/content.css',
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  print preview media fullpage | forecolor backcolor emoticons'
	});
	</script>
