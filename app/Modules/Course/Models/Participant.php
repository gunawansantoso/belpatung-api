<?php
/**
 * Created by PhpStorm.
 * course: lightmire
 * Date: 2/4/2017
 * Time: 19:55
 */

namespace App\Modules\Course\Models;

use Illuminate\Database\Eloquent\Model;


class Participant extends Model {

    protected $fillable = ['user_id', 'class_id','donasi','status',
                         'created_at','updated_at'];

}

?>
