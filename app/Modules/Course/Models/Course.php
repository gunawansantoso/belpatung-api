<?php
/**
 * Created by PhpStorm.
 * course: lightmire
 * Date: 2/4/2017
 * Time: 19:55
 */

namespace App\Modules\Course\Models;

use Illuminate\Database\Eloquent\Model;


class Course extends Model {

    protected $fillable = ['user_id', 'nama_kelas', 'kategori','target_dana','total_dana','posisi_user','estimasi_peserta',
                           'link_kelas','link_foto','tanggal_kelas','deadline_pembayaran','lokasi','deskripsi','status','isFree',
                         'created_at','updated_at'];

    public function users() {
        return $this->belongsToMany('App\Modules\User\Models\User', 'participants', 'class_id', 'user_id');
    }

    public function donations() {
        return $this->hasMany('App\Modules\Transaction\Models\Donation', 'course_id');
    }
}

?>
