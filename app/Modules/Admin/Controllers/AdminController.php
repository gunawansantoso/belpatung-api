<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/11/2017
 * Time: 23:05
 */
namespace App\Modules\Admin\Controllers;

use \App\Http\Controllers\Controller;
use App\Modules\Admin\Repositories\AdminRepository;
use App\Modules\Course\Repositories\CourseRepository;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Testimoni;
use App\Modules\Course\Models\Course;
use App\Modules\User\Repositories\UserRepositoryImpl;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Image;
class AdminController extends Controller {

    private $adminRepository;
    private $courseRepository;
    private $userRepositoryImpl;

    public function __construct(AdminRepository $adminRepository, CourseRepository $courseRepository, UserRepositoryImpl $userRepositoryImpl)
    {
        $this->adminRepository = $adminRepository;
        $this->courseRepository = $courseRepository;
        $this->userRepositoryImpl = $userRepositoryImpl;
    }

    public function index()
    {
        $user = User::paginate(5);
        return view('Admin::index', ['users' => $user]);
    }

    public function transactions() {
        $transations = $this->adminRepository->getAllTransactions();
        return view('Admin::transactions', ['transactions' => $transations]);
    }

    public function courses(){
      $courses = DB::table('courses')
                ->leftjoin('class_donations','courses.id', '=', 'class_donations.course_id')
                ->leftjoin('transactions','transactions.transaction_code','=','class_donations.transaction_code')
                ->select('courses.id','link_foto','link_kelas','nama_kelas','courses.user_id','kategori','target_dana','ispublished', DB::raw("sum(CASE WHEN fraud_status IS NOT NULL AND fraud_status != 'waiting' THEN amount ELSE 0 END) as total"))
                ->whereNull('fraud_status')->orWhere('fraud_status','waiting')->orWhere('fraud_status','accepted')
                ->groupBy('courses.id','link_foto','link_kelas','nama_kelas','courses.user_id','kategori','target_dana','ispublished')
                ->paginate(5);
      return view('Admin::courses', ['courses'=> $courses]);
    }

    public function detailCourse($id){
      $courses = Course::find($id);
      return View('Admin::coursesDetail',['courses'=>$courses]);
    }
    public function deleteCourse($id){
        $course = Course::find($id);
        $course->delete();
        $response = ['status'=>'success', 'data'=>'Course has been deleted'];
        return json_encode($response);
    }

    public function editCourse($id, array $attributes){
        return View('Admin::coursesUpdate');
    }
    public function donations() {
        $donations = $this->adminRepository->getAllDonations();
        return view('Admin::donations', ['donations' => $donations]);
    }

    public function speakers() {
        $speakers = $this->adminRepository->getAllSpeakers();
        return view('Admin::speakers', ['speakers' => $speakers]);
    }

    public function classes() {
        $classes = $this->adminRepository->getAllClasses();
        return view('Admin::classes', ['classes' => $classes]);
    }

    public function paymentChangeStatus(Request $request) {
        $code = $request->input('transaction_code');
        $courseId = $request->input('course_id');
        $status = $request->input('status');
        $course = Course::find($courseId);
        $this->adminRepository->paymentChangeStatus($code, $status);
        if ($status == 'accepted') {

            $userid = $request->input('user_id');
            $this->userRepositoryImpl->createActivity($userid, 'donasi kelas ' . $course->nama_kelas . ' telah diterima');
            $this->courseRepository->publishConstraint($userid, $courseId);
        }
        return back();
    }

    public function verifyUser(Request $request) {
        $userid = $request->input('id');
        $status = $request->input('status');
        $this->adminRepository->changeVerificationUser($userid, $status);
        return redirect()->back()->with('status', 'success melakukan verifikasi user');
    }

    public function updateSpeakerStatus(Request $request) {
        $userid = $request->input('id');
        $status = $request->input('status');
        $this->adminRepository->changeSpeakerStatus($userid, $status);
        return redirect()->back()->with('status', 'success melakukan verifikasi user');
    }

    public function searchUser(Request $request) {
        $query = $request->query('query');
        $users = $this->adminRepository->searchUser($query);
        return view("Admin::index", ['users' => $users]);
    }

    public function searchCourse(Request $request) {

        $query = $request->query('query');
        $courses = $this->adminRepository->searchCourse($query);
        return view("Admin::courses", ['courses' => $courses]);
    }
    public function searchTransaction(Request $request) {
        $query = $request->query('query');
        $donations = $this->adminRepository->searchDonation($query);
        return view("Admin::donations", ['donations' => $donations]);
    }

    public function landingPage() {
          $courses = $this->courseRepository->all();
        $preferences = DB::table('landing_page')->first();
        $testimonis = Testimoni::all();

        return view('Admin::landingPage', ['courses' => $courses, 'preferences' => $preferences,'testimonis'=>$testimonis]);
    }

    public function saveLandingPageSetting(Request $request) {
        $description = $request->input('description');
        $partner_img = $request->file('partner_img');
        $classid1 = $request->input('class_id_1');
        $classid2 = $request->input('class_id_2');
        $classid3 = $request->input('class_id_3');
        $classid4 = $request->input('class_id_4');
        $classid5 = $request->input('class_id_5');
        $classid6 = $request->input('class_id_6');
        $testimoni_1 = $request->input('testimoni_1');
        $testimoni_2 = $request->input('testimoni_2');
        $testimoni_3 = $request->input('testimoni_3');

        if($partner_img){
            $filename = 'partner_img' .  '.' . $partner_img->getClientOriginalExtension();
            Image::make($partner_img)->resize(300,300)->save(public_path('/uploads/'.$filename));
        }
        $preferences = DB::table('landing_page')
            ->where('id', 1)->first();
        if ($preferences != null) {
            Log::info('landing : ' . json_encode($request->all()));
            DB::table('landing_page')
                ->where('id', 1)->update($request->all());
            return redirect()->back()->with('success', 'berhasil mengubah settingan');
        }
        DB::table('landing_page')->insert(
            ['id' => 1,'description' => $description, 'class_id_1' => $classid1, 'class_id_2' => $classid2, 'class_id_3' => $classid3, 'class_id_4' => $classid4, 'class_id_5' => $classid5, 'class_id_6' => $classid6
          ,'testimoni_1'=>$testimoni_1, 'testimoni_2'=>$testimoni_2,'testimoni_3'=>$testimoni_3]
        );
        return redirect()->back()->with('success', 'berhasil mengubah settingan');
    }

    public function users() {
        $user = User::paginate(5);
        return view('Admin::users', ['users' => $user]);
    }

    public function auth() {
        if (Auth::user()) {
            return redirect('/admin');
        }
        return view('Admin::login');
    }

    public function login(Request $request) {

        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::where('email', $email)->first();
        if ($user->isadmin) {
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                return redirect('/admin');
            }
        }
        return redirect()->back();
    }


}
?>
