<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/11/2017
 * Time: 22:58
 */
namespace App\Modules\Admin\Repositories;
use App\Modules\Admin\Repositories\AdminRepositoryInterface;
use App\Modules\Course\Models\Course;
use App\Modules\Transaction\Models\Donation;
use App\Modules\Transaction\Models\Transaction;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Log;
use DB;

class AdminRepository implements AdminRepositoryInterface {

    private $userModel;
    private $transactionModel;
    private $donationModel;
    private $courseModel;

    public function __construct(User $user, Transaction $transaction, Donation $donation, Course $course)
    {
        $this->userModel = $user;
        $this->transactionModel = $transaction;
        $this->donationModel = $donation;
        $this->courseModel = $course;
    }

    public function getAllDonations() {
        $donations = $this->donationModel->orderBy('created_at', 'desc')->paginate(5);
        return $donations;
    }

    public function paymentChangeStatus($code, $status) {
        Log::info('change status =>' . $code . ' ' . $status);
        if ($status == 'accepted') {

        }
        return $this->transactionModel->where('transaction_code',$code)->update([
            'fraud_status' => $status
        ]);

    }

    public function changeVerificationUser($userid, $status) {
        $user = $this->userModel->find($userid);
        $user->isverified = $status;
        $user->save();
    }

    public function changeSpeakerStatus($userid, $status) {
        $user = $this->userModel->find($userid);
        $user->isspeaker = $status;
        $user->save();
    }

    public function getAllSpeakers() {
        $speakers = $this->userModel->where('isspeaker', true)->paginate(10);
        return $speakers;
    }

    public function publishConstraint($user_id,$transaction_code){
        $course_id = $this->donationModel->where('transaction_code', $transaction_code)->first()->course_id;
        $user = User::find($user_id);
        $donation = DB::table('class_donations')
            ->join('transactions', 'class_donations.transaction_code','=','transactions.transaction_code')
            ->where('user_id',$user_id)
            ->where('course_id',$course_id)
            ->where('fraud_status','accepted')->first();

        if($user->isverified && count($donation) == 1){
            DB::table('courses')->where('user_id',$user_id)->update(['ispublished'=> TRUE]);
        }
    }

    public function searchUser($query) {
        return User::where('name', 'LIKE', "%$query%")
            ->orWhere('email', 'LIKE', "%$query%")
            ->orWhere('nomor_wa', 'LIKE', "%$query%")->paginate(5);
    }

    public function searchDonation($query) {
        return Transaction::where('transaction_code','LIKE', "%$query%")
            ->orWhere('payment_method', 'LIKE', "%$query")
            ->orWhere('fraud_status', 'LIKE', "%$query%")->paginate(5);
    }

    public function searchCourse($query){
      
      return DB::table('courses')
                ->leftjoin('class_donations','courses.id', '=', 'class_donations.course_id')
                ->leftjoin('transactions','transactions.transaction_code','=','class_donations.transaction_code')
                ->select('courses.id','link_foto','link_kelas','nama_kelas','courses.user_id','kategori','target_dana','ispublished', DB::raw('sum(CASE WHEN fraud_status IS NOT NULL THEN amount ELSE 0 END) as total'))
                ->whereNull('fraud_status')->orWhere('fraud_status','accepted')
                ->groupBy('courses.id','link_foto','link_kelas','nama_kelas','courses.user_id','kategori','target_dana','ispublished')
                ->havingRaw("nama_kelas ilike '%".$query."%'")
                ->paginate(5);

    }
}
?>
