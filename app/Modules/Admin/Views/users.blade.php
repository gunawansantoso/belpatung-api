@extends('layouts.admin')
@section("content")
    <section class="content">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissable fade in fixed">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $error }}
            </div>
        @endforeach
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="col-sm-6 col-md-6">
                                <form action="{{ route('admin.users.search') }}" method="GET">
                                    <div class="input-group" >
                                        <input type="text" class="form-control" placeholder="Search" name="query">
                                        <div class="input-group-btn">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="glyphicon glyphicon-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="pull-right">
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead class="table primary-thead">
                                        <tr>
                                            <td>Nama</td>
                                            <td>email</td>
                                            <td> Authorization Type </td>
                                            <td>Foto Profile</td>
                                            <td>Foto Identitas</td>
                                            <td>Verifikasi</td>
                                            <td>status</td>
                                            <td style="width: 15%">tindakan</td>
                                        </tr>
                                        </thead>
                                        <tbody style="background-color: #ffffff">
                                        @foreach($users as $user)
                                            <tr>
                                                <td> {{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>
                                                    @if($user->authorization == null)
                                                        Email
                                                    @elseif($user->authorization->provider == 'facebook')
                                                        <center><a class="btn btn-primary" style="width: 100%"> <i class="fa fa-facebook"> </i>&nbsp Facebook </a></center><br>
                                                    @else
                                                        <center><a class="btn btn-danger" style="width: 100%"><i class="fa fa-google-plus"> </i> &nbsp Google + </a></center>
                                                    @endif
                                                </td>
                                                <td><center>
                                                        @if($user->link_foto_profile == null)
                                                            <a class="btn btn-default"> Tidak Ada Gambar</a>
                                                        @else
                                                            <img src="{{ $user->link_foto_profile }}" alt="john doe" style="width:80px;height: auto">
                                                        @endif
                                                    </center></td>
                                                <td>
                                                    @if($user->link_foto_verfikasi == null)
                                                        <a class="btn btn-default"> Foto Identitas Tidak Ada </a>
                                                    @else
                                                        <img src="{{ $user->link_foto_verfikasi }}" alt="Foto Tidak Ada" style="width:80px;height: auto"/>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($user->isverified)
                                                        <span class="label label-success"> <i class="fa fa-check"></i> Verified </span><br><br>
                                                    @else
                                                        <span class="label label-danger"> <i class="fa fa-check"></i> Not Verified </span><br><br>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($user->isactivated)
                                                        <span class="label label-success"> <i class="fa fa-check"></i> Activated </span>
                                                    @else
                                                        <span class="label label-danger"> <i class="fa fa-check"></i> Not Activated </span>

                                                    @endif
                                                </td>
                                                <td>
                                                    @if($user->isverified)
                                                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deactive-user-modal" title="Activate User" onclick="action('unverify',{{ $user->id }})"> Unverify User </button>
                                                        <br><br>
                                                    @elseif($user->link_foto_verfikasi != null)
                                                        <button type="button" class="btn-sm btn btn-success" data-toggle="modal" data-target="#activate-user-modal" title="Activate User" onclick="action('verify',{{ $user->id }})"> Verify User </button>
                                                        <br/>
                                                    @endif
                                                    @if($user->isverified && !$user->isspeaker)
                                                        <button type="button" class="btn-sm btn btn-success" data-toggle="modal" data-target="#add-speaker-modal" title="Activate User" onclick="action('set_as_speaker',{{ $user->id }})"> Jadikan Pembicara </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <center>  {{ $users->links() }}</center>
                                <div id="activate-user-modal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Aktivasi User</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Apakah Anda yakin ingin mengaktifasi user ini ? </p>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row" >
                                                    <div class="col-sm-6" style="text-align: left">
                                                        <form action="{{ route("admin.users.verify") }}" method="POST">
                                                            <input type="hidden" name="id" id="verify_userid"/>
                                                            <input type="hidden" name="status" value="true"/>
                                                            <button type="submit" class="btn btn-success" ><i class="fa fa-trash-o"></i> Verificate User </button>
                                                        </form>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: right">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="deactive-user-modal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Batalkan Aktivasi User</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Apakah Anda yakin ingin membatalkan verifikasi user ini ? </p>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row" >
                                                    <div class="col-sm-6" style="text-align: left">
                                                        <form action="{{ route("admin.users.verify") }}" method="POST">
                                                            <input type="hidden" name="id" id="unverify_userid"/>
                                                            <input type="hidden" name="status" value="false"/>
                                                            <button type="submit" class="btn btn-danger" ><i class="fa fa-trash-o"></i> Unverificate User </button>
                                                        </form>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: right">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="add-speaker-modal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Hapus Pembicara</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Apakah Anda yakin ingin menjadikan pembicara ?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="row" >
                                                    <div class="col-sm-6" style="text-align: left">
                                                        <form action="{{ route("admin.users.speaker.changeStatus") }}" method="POST">
                                                            <input type="hidden" name="id" id="speaker_id"/>
                                                            <input type="hidden" name="status" value="true"/>
                                                            <button type="submit" class="btn btn-success" ><i class="fa fa-trash-o"></i> Jadikan Pembicara </button>
                                                        </form>
                                                    </div>
                                                    <div class="col-sm-6" style="text-align: right">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function action(action, userid) {
                    switch (action) {
                        case 'verify' :
                            $("#verify_userid").val(userid);
                            break;

                        case 'unverify' :
                            $("#unverify_userid").val(userid);
                            break;

                        case 'set_as_speaker' :
                            $("#speaker_id").val(userid);
                            break;
                    }
                }
            </script>
    </section>
@endsection
