@extends('layouts.admin')
@section("content")
    <div class="content-wrapper" style="min-height:600px">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Speaker
                <small>Control panel</small>
            </h1>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Kelolah user</li>
            </ol>
            <br><br>
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <form>
                        <div class="input-group" >
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <h2>10 Results</h2>
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="table primary-thead">
                            <tr>
                                <td> Nama </td>
                                <td> email </td>
                                <td> Foto Profile </td>
                                <td> Jumlah Kelas </td>
                                <td style="width: 15%">tindakan</td>
                            </tr>
                            </thead>
                            <tbody style="background-color: #ffffff">
                            @foreach($speakers as $speaker)
                                <tr>
                                    <td> {{ $speaker->name }}</td>
                                    <td> {{ $speaker->email }}</td>
                                    <td> <img src="{{ $speaker->link_foto_profile }}" alt="Tidak Ada Foto" style="width: 80px; height: auto;"/> </td>
                                    <td> {{ count($speaker->classes) }}</td>
                                    <td> <button type="button" class="btn-sm btn btn-danger" data-toggle="modal" data-target="#delete-speaker-modal" title="Activate User" onclick="action('delete_speaker',{{ $speaker->id }})"> Hapus Pembicara </button> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="delete-speaker-modal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Hapus Pembicara</h4>
                                </div>
                                <div class="modal-body">
                                    <p>Apakah Anda yakin ingin membatalkan verifikasi user ini ? </p>
                                </div>
                                <div class="modal-footer">
                                    <div class="row" >
                                        <div class="col-sm-6" style="text-align: left">
                                            <form action="{{ route("admin.users.speaker.changeStatus") }}" method="POST">
                                                <input type="hidden" name="id" id="speaker_id"/>
                                                <input type="hidden" name="status" value="false"/>
                                                <button type="submit" class="btn btn-danger" ><i class="fa fa-trash-o"></i> Unverificate User </button>
                                            </form>
                                        </div>
                                        <div class="col-sm-6" style="text-align: right">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <center>
            </center>
            <!-- /.row -->
        </section>
        <script>
            function action(action, userid) {
                switch (action) {
                    case 'delete_speaker' :
                        $("#speaker_id").val(userid);
                        break;
                }
            }
        </script>
    </div>
@endsection