@extends('layouts.admin')
@section("content")
    <div class="content-wrapper" style="min-height:600px">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Kelolah user
                <small>Control panel</small>
            </h1>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Kelolah user</li>
            </ol>
            <br><br>
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <form>
                        <div class="input-group" >
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <h2>{{ count($donations) }} Results</h2>
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-responsive">
                            <thead class="table primary-thead">
                            <tr>
                                <td> donation id </td>
                                <td> user id </td>
                                <td> course id </td>
                                <td> transaction code </td>
                                <td> payment method </td>
                                <td> amount </td>
                                <td> Photo </td>
                                <td> fraud status </td>
                                <td> phone number</td>
                                <td> type </td>
                                <td> Create At </td>
                                <td style="width: 15%"> Action </td>
                            </tr>
                            </thead>
                            <tbody style="background-color: #ffffff">
                            @foreach($donations as $donation)
                                <tr>
                                    <td> {{ $donation->donation_id }} </td>
                                    <td>{{ $donation->user_id }}</td>
                                    <td> {{ $donation->course_id }}</td>
                                    <td>{{ $donation->transaction_code }}</td>
                                    <td> {{ $donation->transaction->payment_method }} </td>
                                    <td> {{ $donation->transaction->amount }} </td>
                                    <td> <img height="100" width="150" src="{{ $donation->transaction->confirmation_img }}"/></td>
                                    <td>
                                    @if(($status = $donation->transaction->fraud_status) == 'accepted')
                                        <p class="btn btn-success"> accepted </p>
                                    @elseif($status == 'rejected')
                                        <p class="btn btn-danger"> rejected </p>
                                        @elseif($status == 'canncelled')
                                            <p class="btn btn-danger"> rejected </p>
                                    @else
                                        <p class="btn-warning btn"> {{ $status }} </p>
                                    @endif
                                    </td>
                                    <td> {{ $donation->transaction->phone_number }}</td>
                                    <td> {{ $donation->transaction->type }}</td>
                                    <td> {{ $donation->created_at }}</td>
                                    <td>
                                        <div class="row">
                                                @if($status = $donation->transaction->fraud_status == 'accepted')
                                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#cancel-payment-modal" onclick="paymentAction('cancel',{{ $donation->transaction_code }})"> Cancel Payment </button>
                                                @elseif($status == 'rejected')
                                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#delete-payment-modal" onclick="paymentAction('delete',{{ $donation->transaction_code }})"> Delete Payment </button>
                                                @elseif($status == 'cancelled')
                                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#reject-payment-modal" title="Accept Payment" onclick="paymentAction('reject',{{ $donation->transaction_code }})"> Reject Payment </button>
                                                @else
                                                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#accept-payment-modal" title="Accept Payment" onclick="paymentAction('accept',{{ $donation }})"> Accept Payment </button>
                                                    <button type="button" class="btn btn-default" data-toggle="modal" data-target="#reject-payment-modal" title="Reject Payment" onclick="paymentAction('reject',{{ $donation->transaction_code }})"> Reject Payment </button>
                                                @endif
                                                <div id="accept-payment-modal" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->

                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Hapus User</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Apakah Anda yakin ingin menerima pembayaran ini ? </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <div class="row" >
                                                                    <div class="col-sm-6" style="text-align: left">
                                                                        <form action="{{ route('admin.changeStatus') }}" method="POST">
                                                                            <input type="hidden" name="transaction_code" id="accept_transaction_code"/>
                                                                            <input type="hidden" name="status" value="accepted"/>
                                                                            <input type="hidden" name="course_id" value="" id="accept_course_id"/>
                                                                            <input type="hidden" name="user_id" value="" id="accept_user_id"/>
                                                                            <button type="submit" class="btn btn-success" > <i class="fa fa-trash-o"></i> Terima </button>
                                                                        </form>
                                                                    </div>
                                                                    <div class="col-sm-6" style="text-align: right">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            <div id="cancel-payment-modal" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->

                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Hapus User</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah Anda yakin ingin membatalkan pembayaran ini ? </p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="row" >
                                                                <div class="col-sm-6" style="text-align: left">
                                                                    <form action="donations/payment/status" method="POST">
                                                                        <input type="hidden" name="transaction_code" id="cancel_transaction_code"/>
                                                                        <input type="hidden" name="status" value="cancelled"/>
                                                                        <button type="submit" class="btn btn-danger" > <i class="fa fa-trash-o"></i> Batalkan </button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-sm-6" style="text-align: right">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div id="delete-payment-modal" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->

                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Hapus User</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah Anda yakin ingin membatalkan pembayaran ini ? </p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="row" >
                                                                <div class="col-sm-6" style="text-align: left">
                                                                    <form action="donations/payment/status" method="POST">
                                                                        <input type="hidden" name="transaction_code" id="cancel_transaction_code"/>
                                                                        <input type="hidden" name="status" value="cancelled"/>
                                                                        <button type="submit" class="btn btn-danger" > <i class="fa fa-trash-o"></i> Batalkan </button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-sm-6" style="text-align: right">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div id="reject-payment-modal" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <!-- Modal content-->

                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Hapus User</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Apakah Anda yakin ingin menolak pembayaran ini ? </p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <div class="row" >
                                                                <div class="col-sm-6" style="text-align: left">
                                                                    <form action="donations/payment/status" method="POST">
                                                                        <input type="hidden" name="transaction_code" id="reject_transaction_code"/>
                                                                        <input type="hidden" name="status" value="reject"/>
                                                                        <button type="submit" class="btn btn-warning" ><i class="fa fa-trash-o"></i> Reject </button>
                                                                    </form>
                                                                </div>
                                                                <div class="col-sm-6" style="text-align: right">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    {{ $donations->links() }}
                </div>
            </div>
            <br>
            <center>
            </center>
            <!-- /.row -->
        </section>
    </div>
    <script>
        function paymentAction(action, transactionId) {
            switch (action) {
                case 'accept' :
                case 'accept' :
                    $("#accept_transaction_code").val(transactionId.transaction_code);
                    $("#accept_user_id").val(transactionId.user_id);
                    $("#accept_course_id").val(transactionId.course_id);
                    break;

                case 'delete' :
                    $("#delete_transaction_code").val(transactionId);
                    break;

                case 'reject' :
                    $("#reject_transaction_code").val(transactionId);
                    break;

                case 'cancel' :
                    $("#cancel_transaction_code").val(transactionId);
                    break;
            }
        }
    </script>
@endsection
