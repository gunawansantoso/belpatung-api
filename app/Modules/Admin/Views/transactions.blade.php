@extends('layouts.admin')
@section("content")
    <div class="content-wrapper" style="min-height:600px">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Kelolah user
                <small>Control panel</small>
            </h1>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Kelolah user</li>
            </ol>
            <br><br>
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <form>
                        <div class="input-group" >
                            <input type="text" class="form-control" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <h2>10 Results</h2>
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead class="table primary-thead">
                            <tr>
                                <td> Transaction Code </td>
                                <td> Payment Method </td>
                                <td> Bank </td>
                                <td> Amount </td>
                                <td> Fraud Status </td>
                                <td style="width: 15%"> Status Message </td>
                            </tr>
                            </thead>
                            <tbody style="background-color: #ffffff">
                            @foreach($transactions as $transaction)
                                <tr>
                                    <td> {{ $transaction->transaction_code }}</td>
                                    <td>{{ $transaction->payment_method }}</td>
                                    <td><center><img src="adminlte.7/dist/img/avatar.png" alt="john doe" style="width:80px;height: auto"></center></td>
                                    <td> {{ $transaction->bank }}</td>
                                    <td>{{ $transaction->amount }}</td>
                                    <td> {{ $transaction->fraud_status }}</td>
                                    <td> {{ $transaction->status_message }}</td>
                                    <td>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <button type="" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Lihat detail"> <i class="fa fa-eye"></i> </button>
                                            </div>
                                            <div class="col-sm-4">
                                                <button type="" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Edit"> <i class="fa fa-pencil"></i> </button>
                                            </div>
                                            <div class="col-sm-4">
                                                <button type="" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-delete2" title="Hapus"> <i class="fa fa-trash-o"></i> </button>
                                                <!-- Modal -->
                                                <div id="modal-delete2" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Hapus User</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p>Apakah anda yakin ingin menghapus user ini ? </p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <div class="row" >
                                                                    <div class="col-sm-6" style="text-align: left">
                                                                        <button type="button" class="btn btn-danger" > <i class="fa fa-trash-o"></i> Hapus</button>
                                                                    </div>
                                                                    <div class="col-sm-6" style="text-align: right">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <center>
            </center>
            <!-- /.row -->
        </section>
    </div>
@endsection
