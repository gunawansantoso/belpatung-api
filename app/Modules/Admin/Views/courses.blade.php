@extends('layouts.admin')
@section("content")
    <div class="content-wrapper" style="min-height:600px">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Kelolah kelas
          <small>Control panel</small>
        </h1>

        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Kelolah kelas</li>
        </ol>
        <br><br>
        <!-- Small boxes (Stat box) -->
         <div class="row">
          <div class="col-sm-6 col-md-6">
            <form action="{{ route('admin.courses.search') }}" method="GET">
                <div class="input-group" >

                <input type="text" class="form-control" placeholder="Search" name="query">
                  <div class="input-group-btn">
                    <button class="btn btn-primary" type="submit">
                      <i class="glyphicon glyphicon-search"></i>
                    </button>
                  </div>
                </div>
            </form>
          </div>
         </div>

          <h2>10 Results</h2>
          <div class="row">
            <div class="col-sm-12">
              <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="table primary-thead" style="color: #ffffff;background-color: #337ab7">
                  <tr>
                    <td>Nama Kelas</td>
                    <td>Thumbnail</td>
                    <td>User id</td>
                    <td>Kategori</td>
                    <td>Jenis Kelas</td>
                    <td>Publikasi</td>
                    <td>Dana</td>
                    <td style="width: 15%">tindakan</td>
                  </tr>
                  </thead>
                <tbody style="background-color: #ffffff">
                  @foreach($courses as $course)
                      <tr>
                            <td>{{$course->nama_kelas}}</td>
                            <td><center><img  src="/uploads/{{$course->link_foto}}" alt="course" style="width:80px;height: auto"></center></td>
                            <td>{{$course->user_id}}</td>
                            <td>{{$course->kategori}}</td>

                          <td>
                              @if($course->target_dana == 0)
                                  <span class="label label-success"> <i class="fa fa-check"></i> Kelas Gratis </span>
                              @else
                                  <span class="label label-danger"> <i class="fa fa-check"></i> Kelas Berbayar </span>
                              @endif
                          </td>
                          <td>
                              @if($course->ispublished)
                                  <span class="label label-success"> <i class="fa fa-check"></i> Published</span>
                              @else
                                  <span class="label label-danger"> <i class="fa fa-check"></i> Not Published </span>
                              @endif
                          </td>
                          <td>{{$course->total}}/{{$course->target_dana}} </td>
                          <td>
                              <div class="row">
                                  <div class="col-sm-4">
                                      <a href="/courses/{{$course->link_kelas}}" class="btn btn-primary btn-xs" data-toggle="tooltip" title="Lihat detail"> <i class="fa fa-eye"></i> </a>
                                  </div>
                                  <div class="col-sm-4">
                                      <a href="/courses/{{$course->link_kelas}}/update" class="btn btn-warning btn-xs" data-toggle="tooltip" title="Edit"> <i class="fa fa-pencil"></i> </a>
                                  </div>
                                  <div class="col-sm-4">
                                      <button type="" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modal-delete2"  onclick="setId({{$course->id}})" title="Hapus"> <i class="fa fa-trash-o"></i> </button>
                                      <!-- Modal -->

                                  </div>
                              </div>
                          </td>
                      </tr>
                  @endforeach
                </tbody>
            </table>
          </div>
        </div>
      </div>

    <center>  {{ $courses->links() }}</center>
      <div id="modal-delete2" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <input id="id" type="hidden" value=""/>
              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Hapus Course</h4>
                  </div>
                  <div class="modal-body">
                      <p>Apakah anda yakin ingin menghapus course ini ? </p>
                  </div>
                  <div class="modal-footer">
                      <div class="row" >
                          <div class="col-sm-6" style="text-align: left">
                              <button type="button" class="btn btn-danger delete-btn" > <i class="fa fa-trash-o"></i> Hapus</button>
                          </div>
                          <div class="col-sm-6" style="text-align: right">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                      </div>

                  </div>
              </div>

            </div>
          </div>
        <!-- /.row -->

          <!-- <br>
          <center>
          <ul class="pagination">
        <li><a href="#">1</a></li>
        <li class="active"><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
      </ul>
      </center> -->
      </section>
    </div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
 $(document).ready(function(){

     $('.delete-btn').click(function(){
       $('#modal-delete2').modal('hide');
       $.ajax({
              url: '/admin/courses/'+$('#id').val()+'/delete',
              type: 'POST',
              datatype: 'json',
              success: function (data) {
                location.reload();
              },
              error: function (data) {
                  alert('error');
              }
          });

     });

   });
   function setId(id){
     $('#id').val(id);
   }

</script>
