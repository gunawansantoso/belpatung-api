@extends('layouts.admin')
@section("content")
    <div class="content-wrapper" style="min-height:600px">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Kelolah Halaman Utama
            </h1>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Kelolah user</li>
            </ol>
            <br><br>
            <div class="container container-white">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="panel">
                            <div class="panel-body">
                                <form method="POST" action="{{ route('admin.landingpage.save') }}" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="description"> Deskripsi </label>
                                        <textarea class="form-control" id="description" name="description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="partner_img"> Partner Image </label>
                                        <input class="form-control" type="file" id="partner_img" name="partner_img" />
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h3> Kelas Yang Di rekomendasikan </h3>
                                        </div>
                                        <div class="col-lg-4">
                                            <select name="class_id_1" class="form-control">
                                                @foreach($courses as $course)
                                                    @if($preferences->class_id_1 == $course->id){
                                                      <option value="{{ $course->id }}" selected> {{ $course->nama_kelas }}</option>
                                                    @else
                                                      <option value="{{ $course->id }}" > {{ $course->nama_kelas }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <select name="class_id_2" class="form-control">
                                                @foreach($courses as $course)
                                                @if($preferences->class_id_2 == $course->id){
                                                  <option value="{{ $course->id }}" selected> {{ $course->nama_kelas }}</option>
                                                @else
                                                  <option value="{{ $course->id }}" > {{ $course->nama_kelas }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <select name="class_id_3" class="form-control">
                                                @foreach($courses as $course)
                                                @if($preferences->class_id_3 == $course->id){
                                                  <option value="{{ $course->id }}" selected> {{ $course->nama_kelas }}</option>
                                                @else
                                                  <option value="{{ $course->id }}" > {{ $course->nama_kelas }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h3> Kelas Yang Pembicara Di rekomendasikan </h3>
                                        </div>
                                        <div class="col-lg-4">
                                            <select name="class_id_4" class="form-control">
                                                @foreach($courses as $course)
                                                @if($course->posisi_user == 'pembicara')
                                                  @if($preferences->class_id_4 == $course->id){
                                                    <option value="{{ $course->id }}" selected> {{ $course->nama_kelas }}</option>
                                                  @else
                                                    <option value="{{ $course->id }}" > {{ $course->nama_kelas }}</option>
                                                  @endif
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <select name="class_id_5" class="form-control">
                                                @foreach($courses as $course)
                                                @if($course->posisi_user == 'pembicara')
                                                  @if($preferences->class_id_5 == $course->id){
                                                    <option value="{{ $course->id }}" selected> {{ $course->nama_kelas }}</option>
                                                  @else
                                                    <option value="{{ $course->id }}" > {{ $course->nama_kelas }}</option>
                                                  @endif
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <select name="class_id_6" class="form-control">
                                                @foreach($courses as $course)
                                                @if($course->posisi_user == 'pembicara')
                                                  @if($preferences->class_id_6 == $course->id){
                                                    <option value="{{ $course->id }}" selected> {{ $course->nama_kelas }}</option>
                                                  @else
                                                    <option value="{{ $course->id }}" > {{ $course->nama_kelas }}</option>
                                                  @endif
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                      <div class="col-lg-12">
                                          <h3> Testimoni </h3>
                                      </div>
                                       <div class="col-lg-4">
                                            <select name="testimoni_1" class="form-control">
                                              @foreach($testimonis as $testimoni)
                                                  @if($preferences->testimoni_1 == $testimoni->id){
                                                    <option value="{{ $testimoni->id}}" selected> {{ $testimoni->comment }}</option>
                                                  @else
                                                    <option value="{{ $testimoni->id}}"> {{ $testimoni->comment }}</option>
                                                  @endif
                                              @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <select name="testimoni_2" class="form-control">
                                                @foreach($testimonis as $testimoni)
                                                @if($preferences->testimoni_2 == $testimoni->id){
                                                  <option value="{{ $testimoni->id}}" selected> {{ $testimoni->comment }}</option>
                                                @else
                                                  <option value="{{ $testimoni->id}}"> {{ $testimoni->comment }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <select name="testimoni_3" class="form-control">
                                              @foreach($testimonis as $testimoni)
                                              @if($preferences->testimoni_3 == $testimoni->id){
                                                <option value="{{ $testimoni->id}}" selected> {{ $testimoni->comment }}</option>
                                              @else
                                                <option value="{{ $testimoni->id}}"> {{ $testimoni->comment }}</option>
                                              @endif
                                              @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
