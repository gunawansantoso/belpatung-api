<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/3/2017
 * Time: 14:37
 */
use App\Modules\Admin\Controllers\AdminController;

Route::group(['prefix' => 'admin','middleware' => 'admin', 'namespace' => 'App\Modules\Admin\Controllers'], function () {
    Route::get('/', ['as' => 'admin.index', 'uses' => 'AdminController@index']);
    Route::get('/users', ['as' => 'admin.users', 'uses' => 'AdminController@users'])->middleware('admin');
    Route::get('/transactions', ['as' => 'admin.index', 'uses' => 'AdminController@transactions'])->middleware('admin');
    Route::get('/speakers', ['as' => 'admin.index', 'uses' => 'AdminController@speakers'])->middleware('admin');
    Route::get('/donations', ['as' => 'admin.donations', 'uses' => 'AdminController@donations'])->middleware('admin');
    Route::get('/courses', ['as' => 'admin.courses', 'uses' => 'AdminController@courses'])->middleware('admin');
    Route::get('/courses/{id}', ['as' => 'admin.coursesDetail', 'uses' => 'AdminController@detailCourse'])->middleware('admin');
    Route::post('/courses/{id}/delete', ['as' => 'admin.delete', 'uses' => 'AdminController@deleteCourse'])->middleware('admin');
    Route::post('/donations/payment/status', ['as' => 'admin.changeStatus', 'uses' => 'AdminController@paymentChangeStatus'])->middleware('admin');
    Route::post('/users/verify', ['as' => 'admin.users.verify', 'uses' => 'AdminController@verifyUser'])->middleware('admin');
    Route::post('/users/speakers/changeStatus', ['as' => 'admin.users.speaker.changeStatus', 'uses' => 'AdminController@updateSpeakerStatus'])->middleware('admin');
    Route::get('/users/speakers/', ['as' => 'admin.users.speakers', 'uses' => 'AdminController@speakers'])->middleware('admin');
    Route::get('/users/search/', ['as' => 'admin.users.search', 'uses' => 'AdminController@searchUser'])->middleware('admin');
    Route::get('/course/search/', ['as' => 'admin.courses.search', 'uses' => 'AdminController@searchCourse'])->middleware('admin');
    Route::get('/landingpage', ['as' => 'admin.landingpage', 'uses' => 'AdminController@landingPage'])->middleware('admin');
    Route::post('/landingpage', ['as' => 'admin.landingpage.save', 'uses' => 'AdminController@saveLandingPageSetting'])->middleware('admin');
    Route::get('/login', 'AdminController@auth');
    Route::post('/login', ['as' => 'admin.login', 'uses' => 'AdminController@login']);
});

?>
