<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/4/2017
 * Time: 18:54
 */

namespace App\Modules\User\Repositories;


use App\Modules\User\Models\Speaker;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Skill;
use Illuminate\Http\Request;
use DB;

class SpeakerRepository
{
    private $model;

    /**
     * UserRepository constructor.
     * @param $model
     */
    public function __construct(Speaker $model)
    {
        $this->model = $model;
    }


    public function all()
    {
        return $this->model->all();
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function create(Request $request){
        Course::create([
          'user_id'=> $request->user_id,
          'class_id'=> $request->class_id
        ]);
        return $request;
    }
    public function update($id, array $attributes)
    {
        $user = $this->model->findOrFail($id);
        $user->update();
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }

    public function search($keyword, $keahlian){
      $keahlian = $keahlian == 'all' ? '' : $keahlian;
      $keyword = $keyword == 'all' ? '' : $keyword;
      if($keahlian === ''){
        return User::where('name', 'like', '%'.$keyword.'%')
                      ->where('isspeaker',true)->paginate(9);
      }
      return User::where('name', 'like', '%'.$keyword.'%')
                    ->where('isspeaker',true)
                    ->whereIn('id',function($query) use ($keahlian){
                      $query->select('user_id')
                      ->from('skills')
                      ->where('skill',$keahlian);
                    })
                    ->paginate(9);
    }



}
