<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/4/2017
 * Time: 18:54
 */

namespace App\Modules\User\Repositories;

use App\Modules\Course\Models\Course;
use App\Modules\Transaction\Models\Donation;
use App\Modules\User\Models\ActivityLog;
use App\Modules\User\Models\Authorization;
use App\Modules\User\Models\Interest;
use App\Modules\User\Models\Skill;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Contracts\User as ProviderUser;

class UserRepositoryImpl implements UserRepository
{
    private $model;
    private $skillModel;
    private $interestModel;
    private $activityModel;
    private $courseModel;
    private $verification;
    /**
     * UserRepository constructor.
     * @param $model
     */
    public function __construct(User $model, Skill $skillModel, Interest $interestModel, ActivityLog $activityLog, Course $course)
    {
        $this->model = $model;
        $this->skillModel = $skillModel;
        $this->interestModel = $interestModel;
        $this->activityModel = $activityLog;
        $this->courseModel = $course;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }

    public function update($id, array $attributes)
    {
        return $this->model->where('id', $id)
            ->update($attributes);
    }

    public function delete($id)
    {
        return $this->model->delete($id);
    }

    public function createOrGetUser(ProviderUser $userProvider, $provider)
    {
        $account = Authorization::whereProvider($provider)
            ->whereProviderId($userProvider->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $user = User::create(['name' => $userProvider->getName(), 'email' => $userProvider->getEmail(), 'password' => null]);
            $account = new Authorization(['user_id' => $user->id, 'provider' => $provider, 'provider_id' => $userProvider->getId()]);
            $user->link_foto_profile = $userProvider->getAvatar();
            $account->user()->associate($user);
            $account->save();
            return $account->user;
        }
    }

    public function getUserDonation($userid, array $option)
    {
        // TODO: Implement getUserDonation() method.
    }

    public function getUserClassess($userid, array $option)
    {
        // TODO: Implement getUserClassess() method.
    }

    public function verifyUser($userid)
    {
        $this->model->find($userid)
            ->update(['isVerified' => true]);
    }

    public function addVerification($userid, $img_path)
    {
        $user = $this->model->find($userid);
        $user->link_foto_verfikasi = $img_path;
        $user->save();
        return redirect('/');
    }

    public function addUserInterest($interest)
    {
        $interest = new Interest(['user_id' => Auth::id(), 'interest' => $interest]);
        if (Auth::user()->interests()->save($interest))
            return true;
        return false;
    }

    public function addUserSkill($skill) {
        $skill = new Skill(['user_id' => Auth::id(), 'skill' => $skill]);
        if (Auth::user()->skills()->save($skill))
            return true;
        return false;
    }

    public function removeUserInterest($interest)
    {
       $interest = $this->interestModel->find($interest);
       if ($interest) {
           $interest->delete();
           return true;
       }
       return false;
    }

    public function removeUserSkill($skill)
    {
        $skill = $this->skillModel->find($skill);
        if ($skill) {
            $skill->delete();
            return true;
        }
        return false;
    }

    public function getCountAsSpeaker()
    {

    }

    public function getCountAsPO()
    {
        // TODO: Implement getCountAsPO() method.
    }

    public function getUserDonations() {
        $userid = Auth::id();
        $userDonations = DB::table('users')
            ->where('users.id', $userid)
            ->join('class_donations', 'users.id', '=', 'class_donations.user_id')
            ->join('courses', 'courses.id', '=', 'class_donations.user_id')
            ->join('transactions', 'transactions.transaction_code', '=', 'class_donations.transaction_code')
            ->select('courses.nama_kelas', 'transactions.amount', 'transactions.created_at', 'transactions.fraud_status', 'transactions.transaction_code')
            ->get();

        return $userDonations;
    }

    public function getDashboardInfo() {
        $userid = Auth::id();
        $userCoursesCount = DB::table('class_donations')
            ->where('user_id', $userid)->count();
        $asSpeakerCount = DB::table('courses')
            ->where('courses.user_id', $userid)
            ->where('courses.posisi_user', 'pembicara')->count();

        return ['user_courses_count' => $userCoursesCount, 'user_as_speaker_count' => $asSpeakerCount];
    }

    public function getActivities() {
        return Auth::user()->activities;
    }

    public function getCoursesAsParticipant($userid) {
        $courses = DB::table('courses')
                      ->where('courses.user_id',$userid)
                      ->paginate(5);
        return $courses;
    }

    public function getCoursesAsSpeaker($userid) {
        $courses = DB::table('courses')
            ->leftjoin('class_donations','courses.id', '=', 'class_donations.course_id')
            ->leftjoin('transactions','transactions.transaction_code','=','class_donations.transaction_code')
            ->select('courses.id','link_foto','link_kelas','nama_kelas','courses.user_id','kategori','target_dana','ispublished', DB::raw('sum(CASE WHEN fraud_status IS NOT NULL THEN amount ELSE 0 END) as total'))
            ->whereNull('fraud_status')->orWhere('fraud_status','accepted')
            ->groupBy('courses.id','link_foto','link_kelas','nama_kelas','courses.user_id','kategori','target_dana','ispublished')
            ->where('courses.user_id', $userid)
            ->where('courses.posisi_user', 'pembicara')
            ->distinct()
            ->paginate(5);
        return $courses;
    }

    public function getCoursesAsProjectOfficer($userid) {
        $courses = DB::table('courses')
            ->leftjoin('class_donations','courses.id', '=', 'class_donations.course_id')
            ->leftjoin('transactions','transactions.transaction_code','=','class_donations.transaction_code')
            ->select('courses.id','link_foto','link_kelas','nama_kelas','courses.user_id','kategori','target_dana','ispublished', DB::raw('sum(CASE WHEN fraud_status IS NOT NULL THEN amount ELSE 0 END) as total'))
            ->whereNull('fraud_status')->orWhere('fraud_status','accepted')
            ->groupBy('courses.id','link_foto','link_kelas','nama_kelas','courses.user_id','kategori','target_dana','ispublished')
            ->where('courses.user_id', $userid)
            ->where('courses.posisi_user', 'po')
            ->distinct()
            ->paginate(5);
        return $courses;
    }

    public function getCoursesAsPeserta($userid){
      return DB::table('courses')
              ->join('class_donations','courses.id', '=', 'class_donations.course_id')
              ->join('transactions','transactions.transaction_code','=','class_donations.transaction_code')
              ->select('courses.*','type','class_donations.created_at','amount','fraud_status')
              ->where('class_donations.user_id',$userid)->paginate(5);
    }
    public function createActivity($userid, $activity) {
        $activity = new ActivityLog(['user_id' => $userid, 'description' => $activity]);
        $activity->save();
    }

    public function getUserDonationDetail($userid, $donationid) {
        $userDonations = Donation::where('transaction_code', $donationid)
            ->where('user_id', $userid)->first();
        return $userDonations;
    }

    public function changeUserPassword($userid, $newpassword) {
        $user = User::find($userid);
        $user->password = bcrypt($newpassword);
        return $user->save();
    }
}
