<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/4/2017
 * Time: 18:53
 */

namespace App\Modules\User\Repositories;
use Laravel\Socialite\Contracts\User as ProviderUser;

interface UserRepository {

    /**
     * @return mixed
     * get all users
     */
    public function all();

    /**
     * @param $id
     * @return mixed
     * get user by id
     */
    public function getById($id);

    /**
     * @param array $attributes
     * @return mixed
     * create new user
     */
    public function create(array $attributes);

    /**
     * @param $id
     * @param array $attributes
     * @return mixed
     * update user with specific id
     */
    public function update($id, array $attributes);

    /**
     * @param $id
     * @return mixed
     * delete user with specific id
     */
    public function delete($id);

    /**
     * @param ProviderUser $userProvider
     * @param $provider
     * @return mixed
     * create or get user from social authentication
     */
    public function createOrGetUser(ProviderUser $userProvider, $provider);

    /**
     * @param $userid
     * @return mixed
     * get all specific user donation with id
     */
    public function getUserDonation($userid, array $option);

    /**
     * @param $userid
     * @return mixed
     * get all class which attendance with speicif user
     */
    public function getUserClassess($userid, array $option);

    /**
     * @param $userid
     * @return mixed
     * verify user status to verified
     */
    public function verifyUser($userid);

    /**
     * @param $userid
     * @param $img_path
     * @return mixed
     * create user identity verification
     */
    public function addVerification($userid, $img_path);

    /**
     * @param $interest
     * @return mixed
     * add user interest
     */
    public function addUserInterest($interest);

    public function removeUserInterest($interest);

    /**
     * @param $skill
     * @return mixed
     * add user skill
     */
    public function addUserSkill($skill);

    public function getCountAsSpeaker();

    public function getCountAsPO();
}
?>