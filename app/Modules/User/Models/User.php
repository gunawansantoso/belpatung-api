<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/4/2017
 * Time: 19:55
 */

namespace App\Modules\User\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
    use Notifiable;

    protected $fillable = ['id','name', 'email', 'password'];

    public function donations() {
        return $this->hasMany('App\Modules\Transaction\Models\Donation', 'user_id');
    }

    public function interests() {
        return $this->hasMany('App\Modules\User\Models\Interest', 'user_id');
    }

    public function skills() {
        return $this->hasMany('App\Modules\User\Models\Skill', 'user_id');
    }

    public function courses()
    {
        return $this->belongsToMany('App\Modules\Course\Models\Course', 'participants', 'user_id', 'class_id');
    }

    public function authorization() {
        return $this->hasOne('App\Modules\User\Models\Authorization', 'user_id');
    }

    public function activities() {
        return $this->hasMany('App\Modules\User\Models\ActivityLog', 'user_id');
    }

}

?>
