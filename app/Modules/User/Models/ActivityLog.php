<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLog extends Model {

    protected $fillable = ['user_id', 'description', 'course_id', 'course_name'];

    protected $table = 'activity_logs';

    public function user() {
        return $this->belongsTo(User::class);
    }
}

?>
