<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Authorization extends Model {

    protected $fillable = ['user_id', 'provider', 'provider_id'];

    protected $primaryKey = 'user_id';

    public function user() {
        return $this->belongsTo(User::class);
    }
}

?>
