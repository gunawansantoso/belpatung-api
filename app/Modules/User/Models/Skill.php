<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model {

    protected $fillable = ['user_id', 'skill'];

    protected $table = 'skills';

    public function user() {
        return $this->belongsTo(User::class);
    }
}

?>
