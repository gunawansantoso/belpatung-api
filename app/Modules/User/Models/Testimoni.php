<?php
/**
 * Created by PhpStorm.
 * course: lightmire
 * Date: 2/4/2017
 * Time: 19:55
 */

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;


class Testimoni extends Model {


    protected $fillable = ['user_id','comment'];

}

?>
