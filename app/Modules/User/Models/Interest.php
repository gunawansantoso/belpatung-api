<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model {

    protected $fillable = ['user_id', 'interest'];

    protected $table = 'interests';

    public function user() {
        return $this->belongsTo(User::class);
    }
}

?>
