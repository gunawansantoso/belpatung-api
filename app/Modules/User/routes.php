<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/3/2017
 * Time: 14:37
 */
use App\Modules\User\Controllers\UserController;
use App\Modules\User\Controllers\SpeakerController;

Route::group(['prefix' => '/', 'namespace' => 'App\Modules\User\Controllers'], function () {
    Route::get('/dashboard', ['as' => 'user.dashboard', 'uses' => 'UserController@dashboard'])->middleware('auth');
    Route::get('/donation', ['as' => 'user.donation', 'uses' => 'UserController@donation']);
    Route::get('/users/{id}', ['as' => 'profile', 'uses' => 'UserController@detail'])->where(['id' => '[0-9]+']);
    Route::post('/activate', ['as' => 'users.activate', 'uses' => 'UserController@activate']);
    Route::get('/activation', ['as' => 'activation', 'uses' => 'UserController@activation']);
    Route::get('/verification', ['as' => 'profile.verification', 'uses' =>'UserController@verification']);
    Route::post('/verification/callback', ['as' => 'users.verification.callback', 'uses' => 'UserController@verificationCallback']);
    Route::get('/verified', 'UserController@verificationCallback');
    Route::get('/speaker', 'SpeakerController@index');
    Route::post('/verified', 'UserController@verificationCallback');
    Route::resource('store', 'SpeakerController');
    Route::get('/search/{keyword}/{keahlian}', ['as' => 'users.search', 'uses' => 'SpeakerController@search']);
    Route::get('/profile/testimoni','UserController@testimoni');
    Route::post('/profile/testimoni','UserController@storeTestimoni');
    Route::get('/profile/update', ['as' => 'profile.update', 'uses' => 'UserController@updateUser'])->where(['id' => '[0-9]+']);
    Route::post('/profile/update', ['as' => 'users.profile.store', 'uses' => 'UserController@update'])->where(['id' => '[0-9]+']);
    Route::post('/profile/callback', 'UserController@updateUserCallback')->where(['id' => '[0-9]+']);
    Route::post('/profile/update/interest/add', ['as' => 'users.update.interest.add', 'uses' => 'UserController@addInterest'])->middleware('active.user');
    Route::post('/profile/update/interest/remove', ['as' => 'users.update.interest.remove', 'uses' => 'UserController@removeInterest'])->middleware('active.user');
    Route::post('/profile/update/skill/add', ['as' => 'users.update.skill.add', 'uses' => 'UserController@addSkill'])->middleware('active.user');
    Route::post('/profile/update/skill/remove', ['as' => 'users.update.skill.remove', 'uses' => 'UserController@removeSkill'])->middleware('active.user');
    Route::get('/profile/update/changephoto', ['as' => 'profile.update.photo', 'uses' => 'UserController@changePhoto']);

    Route::get('/profile/participation', ['as' => 'profile.participation', 'uses' => 'UserController@asParticipant']);
    Route::post('/profile/participation', ['as' => 'profile.participation', 'uses' => 'UserController@asParticipantPost']);

    Route::get('/profile/course/edit/{id}', ['as' => 'profile.course.edit', 'uses' => 'UserController@courseEdit']);
    Route::get('/profile/course/detail/{id}', ['as' => 'profile.course.detail', 'uses' => 'UserController@courseDetail']);


    Route::get('/profile/peserta', ['as' => 'profile.peserta', 'uses' => 'UserController@asPeserta']);
    Route::post('/profile/peserta', ['as' => 'profile.peserta', 'uses' => 'UserController@asPesertaPost']);

    Route::get('/donation/{id}', ['as' => 'profile.donation.detail', 'uses' => 'UserController@donationDetail'])->where(['id' => '[0-9]+']);
    Route::get('/donation/add', ['as' => 'profile.donation.add', 'uses' => 'UserController@donationAdd']);
    Route::get('/donation/withdraw', ['as' => 'profile.donation.withdraw', 'uses' => 'UserController@donationWithdraw']);

    Route::post('/donation/{id}', ['as' => 'profile.donation.confirm', 'uses' => 'UserController@donationConfirmation'])->where(['id' => '[0-9]+']);;
    Route::get('/profile/password', ['as' => 'profile.change.password', 'uses' => 'UserController@changePassword']);
    Route::post('/profile/password', ['as' => 'profile.change.password.store', 'uses' => 'UserController@changeUserPassword']);
});

?>
