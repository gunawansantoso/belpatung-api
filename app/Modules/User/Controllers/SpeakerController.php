<?php
/**
 * Created by PhpStorm.
 * Speaker: lightmire
 * Date: 2/4/2017
 * Time: 18:29
 */
namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\User\Models\Speaker;
use App\Modules\User\Models\User;
use App\Modules\User\Repositories\SpeakerRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SpeakerController extends Controller {

    private $speakerRepository;

    /**
     * SpeakerController constructor.
     * @param $SpeakerRepository
     */
    public function __construct(SpeakerRepository $speakerRepository)
    {
        $this->middleware('auth', ['except' => 'logout']);
        $this->speakerRepository = $speakerRepository;
    }

    public function index() {
        $speaker = $this->speakerRepository->all();
        return $speaker;
    }

    public function store(Request $request){
          $this->speakerRepository->create($request);
    }

    public function search($keyword, $keahlian){
      $results = $this->speakerRepository->search($keyword,$keahlian);
      return view('speakers',['results'=>$results,'keyword'=>$keyword]);

    }
}
?>
