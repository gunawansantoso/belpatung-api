<?php
/**
 * Created by PhpStorm.
 * User: lightmire
 * Date: 2/4/2017
 * Time: 18:29
 */
namespace App\Modules\User\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Transaction\Repositories\DonationRepository;
use App\Modules\User\Models\User;
use App\Modules\User\Models\Testimoni;
use App\Modules\User\Repositories\userRepositoryImpl;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller {

    private $userRepositoryImpl;
    private $googleClient;
    private $donationRepository;
    /**
     * UserController constructor.
     * @param $userRepositoryImpl
     */
    public function __construct(userRepositoryImpl $userRepositoryImpl, DonationRepository $donationRepository)
    {
        $this->googleClient = new \Google_Client();
        $this->googleClient->setClientId('1021903151000-cmna8uqnc4llrlngriascn361q0f2b9n.apps.googleusercontent.com');
        $this->googleClient->setClientSecret('sg-jBBlfWEA6xuelLP6EcIlg');

        $this->middleware('auth', ['except' => 'logout']);
        $this->middleware('active.user');
        $this->userRepositoryImpl = $userRepositoryImpl;
        $this->donationRepository = $donationRepository;
    }

    public function index() {
        $users = $this->userRepositoryImpl->all();
        return view("User::index", ['users' => $users]);
    }

    public function detail($id) {
        $user = $this->userRepositoryImpl->getById($id);
       if ($user != null) {
           return view('User::detail', ['user' => $user]);
       }
       return redirect('/');
    }

    public function activation() {
        $user = Auth::user();
        if ($user->isactivated) {
            return redirect()->back();
        }
        return view('User::activate');
    }

    public function activate(Request $request) {
        $user = Auth::user();
        $waNumber = $request->input('wa_number');

        if (User::where('nomor_wa', $waNumber)->first()) {
            return back()->with('errors', ['nomor wa sudah digunakan']);
        }
        $user->nomor_wa = $waNumber;
        $user->isactivated = true;
        Log::info('users : ' . json_encode($user));
        if ($user->save()) {
            Log::info('activate user');
            return redirect('/users');
        } else {
            return redirect('/');
        }
    }

    public function verification() {
        return view('User::verification');
    }

    public function verificationCallback(Request $request) {
        $identity_img = $request->input('response_img');
        $this->userRepositoryImpl->addVerification(Auth::id(), $identity_img);
    }

    public function update(Request $request) {
        $this->userRepositoryImpl->update(Auth::id(), $request->all());
        return redirect()->back();
    }

    public function updateUser() {
        $user = Auth::user();
        return view('User::update', ['user' => $user]);
    }

    public function updateUserCallback() {
        return redirect('/users/profile');
    }

    public function verifyUser($id) {
        $this->userRepositoryImpl->verifyUser($id);
    }

    public function addInterest(Request $request) {
        $interest = $request->input('interest');
        if ($this->userRepositoryImpl->addUserInterest($interest))
            return redirect()->back()->with('status', 'succcess');
        return redirect()->back()->withErrors('gagal menambahakan interest');

    }

    public function removeInterest(Request $request) {
        $interest = $request->input('interest');
        Log::info('interest' . $interest);
        if ($this->userRepositoryImpl->removeUserInterest($interest))
            return redirect()->back()->with('status', 'succcess');
        return redirect()->back()->withErrors('gagal menambahakan interest');
    }

    public function addSkill(Request $request) {
        $skill = $request->input('skill');
        if ($this->userRepositoryImpl->addUserSkill($skill))
            return redirect()->back()->with('status', 'succcess');
        return redirect()->back()->withErrors('gagal menambahakan skill');

    }

    public function removeSkill(Request $request) {
        $skill = $request->input('skill');
        if ($this->userRepositoryImpl->removeUserSkill($skill))
            return redirect()->back()->with('status', 'succcess');
        return redirect()->back()->withErrors('gagal menambahakan skill');
    }

    public function donation() {
        $userDonations = $this->userRepositoryImpl->getUserDonations();
        return view('User::donation', ['userDonations' => $userDonations]);
    }

    public function dashboard() {
        $dashboardInfo = $this->userRepositoryImpl->getDashboardInfo();
        $activities = $this->userRepositoryImpl->getActivities();
        return view('User::dashboard', ['dashboardInfo' => $dashboardInfo, 'activities' => $activities]);
    }

    public function changePhoto() {
        return view('User::change_photo');
    }

    public function asParticipant() {
        $courses = $this->userRepositoryImpl->getCoursesAsParticipant(Auth::id());
        return view('User::participant', ['courses' => $courses]);
    }
    public function asParticipantPost(Request $request){
      $courses = DB::table('courses')
                    ->where('courses.user_id',Auth::id())
                    ->where('nama_kelas','ilike', '%'.$request->keyword.'%')
                    ->paginate(5);
      return view('User::participant', ['courses' => $courses]);
    }

    public function asPeserta(){
      $courses = $this->userRepositoryImpl->getCoursesAsPeserta(Auth::id());
      return View('User::peserta', ['courses' => $courses]);
    }
    public function asPesertaPost(Request $request){
      $courses = DB::table('courses')
              ->join('class_donations','courses.id', '=', 'class_donations.course_id')
              ->join('transactions','transactions.transaction_code','=','class_donations.transaction_code')
              ->select('courses.*','type','class_donations.created_at','amount','fraud_status')
              ->where('class_donations.user_id',Auth::id())
              ->where('courses.nama_kelas','ilike','%'.$request->keyword.'%')
              ->paginate(5);
      return View('User::peserta', ['courses' => $courses]);
    }
    public function donationDetail($donationId) {
        $donation = $this->userRepositoryImpl->getUserDonationDetail(Auth::id(), $donationId);
        return view('User::donationDetail', ['donation' => $donation]);
    }

    public function donationAdd(){
        return View('User::donationAdd');
    }

    public function donationWithdraw(){
        return View('User::donationWithdraw');
    }

    public function donationConfirmation($id, Request $request) {
        $confirm_img = $request->input("confirmation_img");
        $this->donationRepository->addConfirmationImg($id, $confirm_img);
        return redirect()->back();
    }

    public function changePassword()
    {
        return view('User::changePassword');
    }

    public function testimoni(){
      return View('User::testimoni');
    }

    public function storeTestimoni(Request $request){
      Testimoni::create([
          'user_id'=>Auth::id(),
          'comment'=>$request->testimoni
      ]);
      return redirect('/dashboard');
    }

    public function changeUserPassword(Request $request) {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        Log::info('password : masuk');
        $newpassword = $request->input('old_password');
        $user = Auth::user();
        Log::info('new pasword ' . $newpassword);
        if (Hash::check($user->password, $newpassword))
            return redirect()->back()->withErrors('errors', 'password lama tidak sesuai');
        $this->userRepositoryImpl->changeUserPassword(Auth::id(), $request->input('password'));
        Log::info('berhasil ganti');
        return redirect()->back()->withErrors('errors', 'error aneh');

    }

    public function courseEdit($id){
        return View('User::courseEdit');
    }

    public function courseDetail($id){
        return View('User::courseDetail');
    }

}

?>
