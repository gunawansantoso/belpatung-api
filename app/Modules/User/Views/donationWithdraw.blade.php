@extends('layouts.dashboard')

@section('title', 'Page Title')

@section('main')
    <div class="container container-white">
      <p class="title-dashboard" > Celengan Saya <span style="color: #329666">> Cairkan Dana</span></p>
      <div class="green-line"></div>
      <br>
      <div class="row" style="width:100%;margin: auto;">
        <div class="col-sm-12 green-column" style="padding: 10px">
          <h2>Saldo Anda : <b>Rp 0</b></h2>

        </div>
        <div class="col-sm-12">
          <br>
          <p>Butuh saldo tambahan ?  <a href="">Isi disini</a> </p> 
        </div>        
      </div>
      <br><br>
      <div class="col-sm-12">
        <form>
          <div class="form-group">
            <label for="jumlah-dana">Jumlah Pencairan Dana ( Minimal Rp 50.000 )</label>
            <input type="text" class="form-control" id="jumlah-dana" name="jumlah_dana" placeholder="Masukan jumlah dana, misal 50000" required>
          </div>
          <div class="form-group">
            <label for="nama-bank">Nama Bank Tujuan:</label>
            <input type="text" class="form-control" id="nama-bank" name="nama_bank" placeholder="Masukan nama bank tujuan, misal Bank Central Asia (BCA)" required>
          </div>
          <div class="form-group">
            <label for="cabang_bank">Kantor Cabang Bank Tujuan</label>
            <input type="text" class="form-control" id="cabang_bank" name="cabang_bank" placeholder="Masukan Kantor Cabang, misal Cabang Pasar Minggu" required>
          </div>
          <div class="form-group">
            <label for="no_rek">Nomor Rekening</label>
            <input type="text" class="form-control" id="no_rek" name="no_rek" placeholder="Masukan nomor rekening bank tujuan" required>
          </div>
          <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-send"> </i> Submit</button>
        </form>
      </div>
    </div>
@endsection
@section ('scripts')
   
@endsection



<!-- /Chatra {/literal} -->

