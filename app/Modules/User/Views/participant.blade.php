@extends('layouts.dashboard')

@section('title', 'Page Title')

@section('main')
<div class="container container-white">
	<p class="title-dashboard" > Kelas Saya </p>
	<div class="green-line"></div>
	<br>
	<p class="sub-title-dashboard" ><i class="fa fa-list"></i> Lihat kelas yang saya ambil atau sedang ikuti </p>
	<br>
	<ul class="nav nav-tabs">
	  <li class="active"><a data-toggle="tab" href="#home">Sebagai Pembuat Kelas</a></li>
	  <li><a data-toggle="tab" href="#menu1">Sebagai Peserta Kelas</a></li>
	</ul>

	<div class="tab-content">
	  	<div id="home" class="tab-pane fade in active">
	  		<br><br>
		  	<form class="form-inline" >
				<div class="form-group" >
					<p> Cari kelas aktifitas saya </p>
					<input class="form-control" style="width: 300px" placeholder="Cari kelas disini...">
					<button type="button" class="btn btn-success" style="margin-left: -5px"><i class="fa fa-search"></i></button>
				</div>
			</form>
			<br><br>
			<table class="table table-bordered">
			    <thead>
			      <tr>
			        <th>Nama kelas</th>
			        <th>Tanggal dibuat</th>
			        <th>Posis Anda</th>
			        <th style="width:20%">Tindakan</th>
			      </tr>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>Kelas satu</td>
			        <td>22 Juni 2017</td>
			        <td>Pembicara</td>
			        <td>
				        <div class="row">
				        	<div class="col-sm-6">
				        		<button type="button" class="btn btn-sm btn-primary" style="width:100%">Edit</button>
				        	</div>
				        	<div class="col-sm-6">
				        		<button type="button" class="btn btn-sm btn-warning" style="width:100%">Detail</button>
				        	</div>
				        </div>
			        </td>
			      </tr>
			      <tr>
			        <td>Mary</td>
			        <td>22 Juni 2017</td>
			        <td>Peserta</td>
			        <td>
				        <div class="row">
				        	<div class="col-sm-6">
				        		<a href="edit-kelas-saya.html"><button type="button" class="btn btn-sm btn-primary" style="width:100%">Edit</button></a>
				        	</div>
				        	<div class="col-sm-6">
				        		<button type="button" class="btn btn-sm btn-warning" style="width:100%">Detail</button>
				        	</div>
				        </div>
			        </td>
			      </tr>
			    </tbody>
			 </table>
	  	</div>
	  	<div id="menu1" class="tab-pane fade">
	  		<br><br>
		  	<form class="form-inline" >
				<div class="form-group" >
					<p> Cari kelas aktifitas saya </p>
					<input class="form-control" style="width: 300px" placeholder="Cari kelas disini...">
					<button type="button" class="btn btn-success" style="margin-left: -5px"><i class="fa fa-search"></i></button>
				</div>
			</form>
			<br><br>
			<table class="table table-bordered">
			    <thead>
			      <tr>
			        <th>Nama kelas</th>
			        <th>Tanggal dibuat</th>
			        <th>Posis Anda</th>
			        <th style="width:20%">Tindakan</th>
			      </tr>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>Kelas satu</td>
			        <td>22 Janu 2017</td>
			        <td>Pembicara</td>
			        <td>
				        <div class="row">
				        	<div class="col-sm-6">
				        		<button type="button" class="btn btn-sm btn-warning" style="width:100%">Detail</button>
				        	</div>
				        </div>
			        </td>
			      </tr>
			      <tr>
			        <td>Mary</td>
			        <td>Moe</td>
			        <td>mary@example.com</td>
			        <td>
				        <div class="row">
				        	<div class="col-sm-6">
				        		<button type="button" class="btn btn-sm btn-warning" style="width:100%">Detail</button>
				        	</div>
				        </div>
			        </td>
			      </tr>
			    </tbody>
			 </table>
	  	</div>
	 </div>
</div>
@endsection



<!-- /Chatra {/literal} -->
