@extends('layouts.app')
@section('style')
    .list {
    margin: 0;
    padding: 0;
    list-style: none;
    }
    .list li {
    display: inline-block;
    margin: 0 .3em .3em 0;
    padding: 0;
    display: inline-block;
    max-width: 100px;
    height: 28px;
    line-height: 28px;
    padding: 0 1em;
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 3px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    color: #333;
    font-size: 13px;
    text-decoration: none;
    -webkit-transition: .2s;
    transition: .2s;
    }
@endsection
@section('content')
    <div class="row" style="width: 95%;margin:auto;">
        <div class="col-sm-9">
            <div class="container-detail-profile">
                <div class="profile profile-title">
                    {{ $user->link_foto_profile }}
                    <img src="{{ $user->link_foto_profile }}" alt="" class="img-circle" style="width: 100px;">
                    <h3>{{ $user->name }}</h3>
                    <h5>
                        @if($user->isverified)
                            <a class="label label-success">
                                <i class="fa fa-check"></i>
                                    Verified
                            </a>
                        @else
                            <a class="label label-danger" href="{{ route('profile.verification') }}">
                                <i class="fa fa-close"></i>
                                Not Verified
                            </a>
                        @endif
                    </h5>
                </div>
                <div class="profile profile-content">
                    <hr>
                    <center>
                        <p style="font-size: 1.5em;font-family:montserrat ">Ketertarikan </p>
                        <ul class="list">
                            @foreach($user->interests as $interest)
                                <li> {{ $interest->interest }} </li>
                            @endforeach
                        </ul>
                    </center>
                    <hr>
                    <center>
                        <p style="font-size: 1.5em;font-family:montserrat ">Keahlian </p>
                        <ul class="list">
                            @foreach($user->skills as $skill)
                                <li> {{ $skill->skill }} </li>
                            @endforeach
                        </ul>

                    </center>
                    <br>
                    <center>
                        <hr>
                        <p style="font-size: 1.5em;font-family:montserrat ">Biodata Pribadi </p>
                    </center>

                    <br>
                    <div class="row" style="width: 100%;margin:auto;">
                        <div class="col-sm-4">
                            <p>Nama Lengkap :</p>
                        </div>
                        <div class="col-sm-8">
                            <p>{{ $user->name }}</p>
                        </div>
                        <div class="col-sm-4">
                            <p>Tempat lahir :</p>
                        </div>
                        <div class="col-sm-8">
                            <p>Jakarta</p>
                        </div>
                        <div class="col-sm-4">
                            <p>Jumlah kelas diikuti :</p>
                        </div>
                        <div class="col-sm-8">
                            <p>{{ count($user->courses) }}</p>
                        </div>
                        <div class="col-sm-4">
                            <p>Jumlah sebagai pembicara :</p>
                        </div>
                        <div class="col-sm-8">
                            <p>20</p>
                        </div>
                        <div class="col-sm-4">
                            <p>Jumlah sebagai Poject Office :</p>
                        </div>
                        <div class="col-sm-8">
                            <p>20</p>
                        </div>
                    </div>

                    <center>
                        <hr>
                        <p style="font-size: 1.5em;font-family:montserrat ">Kelas Telah Diikuti </p>
                    </center>
                    <div class="row" style="width: 100%;margin:auto">
                        @foreach($user->courses as $course)
                            <div class="col-sm-4 image-thumb  ">
                                <div class="green-column-inverse">
                                    <img src="images/kelas1.jpg">
                                    <div class="sinopsis-kelas">
                                        <h4><b> {{ $course->nama_kelas }} </b></h4>
                                        <p class="lighttext"> {{ $course->kategori }} </p>
                                        <hr>
                                        <p>{{ $course->deskripsi }}
                                        </p>
                                        <a href="">{{ $course->link_kelas }}</a>
                                    </div>
                                </div>
                        </div>
                        @endforeach
                    </div>
                    <br>

                </div>

            </div>
        </div>
        <div class="col-sm-3">
            <div class="container-rekomendasi-profile">
                <center>
                    <p style="font-size: 1.5em;font-family:montserrat ">Rekomendasi </p>
                    <br>
                    <div class="profile profile-title">
                        <img src="images/img_avatar1.png" alt="" class="img-circle" style="width: 100px;">
                        <h6>Mr Joko (Prof1/2/3>)</h6>
                        <h6><i class="fa fa-search"></i> Detail</h6>
                    </div>
                    <div class="profile profile-title">
                        <img src="images/img_avatar1.png" alt="" class="img-circle" style="width: 100px;">
                        <h6>Mr Joko (Prof1/2/3>)</h6>
                        <h6><i class="fa fa-search"></i> Detail</h6>
                    </div>
                    <div class="profile profile-title">
                        <img src="images/img_avatar1.png" alt="" class="img-circle" style="width: 100px;">
                        <h6>Mr Joko (Prof1/2/3>)</h6>
                        <h6><i class="fa fa-search"></i> Detail</h6>
                    </div>
                </center>
            </div>
        </div>
    </div>
@endsection
