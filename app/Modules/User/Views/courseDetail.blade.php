@extends('layouts.dashboard')
@section('title','Page Title')
@section('main')
<div class="container container-white">
	<p class="title-dashboard" >Detail Kelas Saya </p>
	<div class="green-line"></div>
	<br>
	<p class="sub-title-dashboard" ><i class="fa fa-list"></i> Detail kelas yang saya ambil atau sedang ikuti </p>
	<hr>
	<div>
		<button type="button" class="btn btn-default btn-info" data-toggle="modal" data-target="#review_modal"><i class="fa fa-pencil"></i> Tulis Review Kamu</button>
		<!-- Modal -->
		<div id="review_modal" class="modal fade" role="dialog">
		  <div class="modal-dialog">

		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Silahkan tuliskan review</h4>
		      </div>
		      <div class="modal-body">
		        <p style="font-size:1.2em">Disini Judul Kelas</p>
		        <hr>
		        <p>Review</p>
		        <form actio="" method="">
		        	<textarea style="height:120px" class="form-control" required></textarea>
			        <br>
			        <button type="submit" class="btn btn-info">Submit review</button>
		        </form>
		        
		        <hr>
		        <p>Berikan Rating kelas ini (skala 1 -5 )</p>
		        <form action="" method="">
		        	<div class="radio">
					  <label><input type="radio" name="optradio">1 (butuh banyak perbaikan kedepannya) </label>
					</div>
					<div class="radio">
					  <label><input type="radio" name="optradio">2 ( Butuh sedikit perbaikan dan pengembangan Pembicara dan organisasi )</label>
					</div>
					<div class="radio">
					  <label><input type="radio" name="optradio">3 ( konten materi cukup baik, cukup mudah dipahami dan pembicara bagus ) </label>
					</div>
					<div class="radio">
					  <label><input type="radio" name="optradio">4 ( Kelas menarik, materi baik, mudah dipahami dan pembicara ahli )</label>
					</div>
					<div class="radio">
					  <label><input type="radio" name="optradio">5 (  sangat menarik dan sangat baik segala aspeknya )</label>
					</div>
					<br>
					<button type="submit" class="btn btn-warning" style="color:#ffffff !important">Kirim Rating </button>
		        </form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>

		  </div>
		</div>
	</div>
	
	<br><br>
	<img src="{{asset('belpatung-api/public/images/kelas1.jpg')}}" alt="" style="max-width:500px">
	<br><br>
	<div >
		<div style="display: inline-block;;width:200px">
			<p>Nama kelas  :</p>
		</div>
		<div style="display: inline-block;">
			<p>Kelas Matematika 1</p>
		</div>
	</div>

	<div >
		<div style="display: inline-block;;width:200px">
			<p>Target Dana  :</p>
		</div>
		<div style="display: inline-block;">
			<p>20.000.000</p>
		</div>
	</div>

	<div >
		<div style="display: inline-block;;width:200px">
			<p>Posisi Saya  :</p>
		</div>
		<div style="display: inline-block;">
			<p>Pembicara</p>
		</div>
	</div>

	<div >
		<div style="display: inline-block;;width:200px">
			<p>Estimasi Peserta  :</p>
		</div>
		<div style="display: inline-block;">
			<p>50</p>
		</div>
	</div>

	<div >
		<div style="display: inline-block;;width:200px">
			<p>Link Kelas  :</p>
		</div>
		<div style="display: inline-block;">
			<p>https://www.belpatugan.com</p>
		</div>
	</div>

	<div >
		<div style="display: inline-block;;width:200px">
			<p>Nama Pembicara  :</p>
		</div>
		<div style="display: inline-block;">
			<p>Mr robert, Mr josh</p>
		</div>
	</div>

	<div >
		<div style="display: inline-block;;width:200px">
			<p>Deadline kelas  :</p>
		</div>
		<div style="display: inline-block;">
			<p>11 Janu 2017</p>
		</div>
	</div>
	<hr>
	<div >
		<div style="display: inline-block;;width:200px">
			<p style="font-size: 1.5em">Deskripsi :</p>

		</div>
		<div style="display: inline-block;">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
		</div>
	</div>
	<hr>
	<div >
		<div style="display: inline-block;;width:200px">
			<p style="font-size: 1.5em">Apa kata Mereka ?</p>
			<p style="font-size:0.9em"><i>Rating 4 dari 5</i></p>
			<br>
		</div>
		<div style="display: inline-block;">
			<div class="media">
			  <div class="media-left">
			    <img src="{{asset('belpatung-api/public/images/img_avatar1.png')}}" class="media-object img-circle" style="width:60px">
			  </div>
			  <div class="media-body">
			    <h4 class="media-heading">John Doe</h4><span class="label label-success">Memberikan rating 4</span><br>
			    <p>Mantap gan</p>
			  </div>
			</div>
			<hr>
			<div class="media">
			  <div class="media-left">
			    <img src="{{asset('belpatung-api/public/images/img_avatar1.png')}}" class="media-object img-circle" style="width:60px">
			  </div>
			  <div class="media-body">
			    <h4 class="media-heading">John Doe</h4>
			    <p>Mantap gan</p>
			  </div>
			</div>
			<hr>
			<div class="media">
			  <div class="media-left">
			    <img src="{{asset('belpatung-api/public/images/img_avatar1.png')}}" class="media-object img-circle" style="width:60px">
			  </div>
			  <div class="media-body">
			    <h4 class="media-heading">John Doe</h4><span class="label label-success">Memberikan rating 4</span><br>
			    <p>Mantap gan</p>
			  </div>
			</div>
		</div>
	</div>
</div>
@endsection