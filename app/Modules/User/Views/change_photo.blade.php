@extends('layouts.dashboard')
@section('style')
    .list {
    margin: 0;
    padding: 0;
    list-style: none;
    }
    .list li {
    display: inline-block;
    margin: 0 .3em .3em 0;
    display: inline-block;
    height: 28px;
    line-height: 28px;
    padding: 0 1em;
    padding-right:0;
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 3px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    color: #333;
    font-size: 13px;
    text-decoration: none;
    -webkit-transition: .2s;
    transition: .2s;
    }
@endsection
@section('content')
    <div class="container container-white">
        <p class="title-dashboard" > Profil Saya <span style="color: #329666"> > Kelolah informasi</span></p>
        <div class="green-line"></div>
        <br>
        <form id="update_form">
            <img id="preview" />
            <div class="form-group">
                <label for="nama">Foto</label>
                <input type="file" class="form-control" id="payload_img" name="image"/>
            </div>
            <button type="submit" class="btn btn-primary btn-lg">Submit</button>
        </form>
    </div>
    <script>

        $("#payload_img").change(function(){
            files = $("input[type=file]")[0];
            console.log(files);
            readURL(this);
        });

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#update_form").submit(function(e){
            $("#spinner").show();
            var data = $(this).serialize();
            var url = "{{ route('users.profile.store') }}";
            var dataValue = new FormData(this);

            e.preventDefault();

            $.ajax({
                type: 'POST',
                //remove the .php from results.php.php
                url: "https://api.imgur.com/3/upload",
                //Add the request header
                headers: {
                    Authorization: 'Client-ID 64b7e5009ee260e'
                },
                contentType: 'application/x-www-form-urlencoded',
                //Add form data
                data: dataValue,
                contentType: false,
                cache: false,
                processData:false,
                success: function (response) {
                    var link = response.data.link;
                    data = {
                        'link_foto_profile' : link,
                        'name' : data.name,
                        'nama_organisasi' : data.nama_organisasi
                    };

                    console.log("data", data.toString());
                    $.ajax({
                        type : "POST",
                        url : url,
                        data : data,
                        success : function(data) {
                            console.log("data");
                            location.reload();
                        },
                        failure : function (err) {
                            console.log(err);
                            location.reload();
                        }
                    });
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                }
            });
        });
    </script>
@endsection
