@extends('layouts.dashboard')
@section('main')
<div class="container container-white">
  <p class="title-dashboard" > Testimoni </p>
  <div class="green-line"></div>
  <br>
  <form method="POST" action="{{url('/profile/testimoni')}}">
      	{{ csrf_field() }}
        <div class="form-group">

  				<textarea id="testimoni" placeholder="Masukkan testimoni kamu.." class="form-control" name="testimoni" required ></textarea>
  			</div>
        	<button type="submit" class="btn btn-primary btn-lg">Submit</button>
  </form>
</div>
@endsection
