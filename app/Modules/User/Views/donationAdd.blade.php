@extends('layouts.dashboard')

@section('title', 'Page Title')

@section('main')
    <div class="container container-white">
        <p class="title-dashboard" > Celengan Saya <span style="color: #329666"> > Tambah Celengan</span></p>
        <div class="green-line"></div>  
        <br>
        <form>
          <div class="form-group">
            <label for="jumlah_uang">Jumlah Nominal : ( minimum Rp ??? )</label>
            <input required type="text" class="form-control" id="jumlah_uang" name="jumlah_uang">
          </div>
          <div class="form-group">
            <label for="metode">Metode Bayar :</label>
            <select name="metode" class="form-control" id="metode">
                <option value="Bank 1"> bank 1</option>
                <option value="Bank 2"> bank 2</option>
                <option value="Bank 3"> bank 3</option>
            </select>
          </div>
          <div class="form-group">
            <label for="bukti_bayar">Upload Bukti Bayar :</label>
            <input type="file" name="bukti_bayar" class="btn ">
          </div>
          <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-send"></i> Submit</button>
        </form>
    </div>
@endsection
@section ('scripts')
   
@endsection



<!-- /Chatra {/literal} -->

