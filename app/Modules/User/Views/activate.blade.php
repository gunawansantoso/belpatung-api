@extends('layouts.dashboard')
@section('title', 'Page Title')

@section('main')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h1> Aktivasi Akun Anda</h1>
                    </div>
                    <div class="panel-body">
                        <form action="{{ route('users.activate') }}" method="POST" >
                            <div class="input-group">
                                <label for="wa_number"> Nomor WA  @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif</label>
                                <input type="text" name="wa_number" class="form-control" id="wa_number"/>
                            </div>
                            <div class="input-group">
                                <label for="interest"> Interest </label>
                                <select id="interest" class="form-control">
                                    <option value="sport"> Sport </option>
                                </select>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary"> Activate </button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection



<!-- /Chatra {/literal} -->
