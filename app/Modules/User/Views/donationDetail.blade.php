@extends('layouts.dashboard')

@section('title', 'Page Title')

@section('main')
    <div class="container container-white">
        <p class="title-dashboard" > Detail Donasi </p>
        <div class="green-line"></div>
        <br>
        <p class="sub-title-dashboard" ><i class="fa fa-list"></i> Donasi untuk <b> Kelas Matematika pak Yusuf </b> </p>
        <br>
        <div class="well" style="background-color: #ffffff">
            <div>
                <div style="display: inline-block;width:200px">
                    <p>Jumlah Donasi </p>
                </div>
                <div style="display: inline-block;width:400px">
                    <p>Rp 10.000.000 </p>
                </div>
            </div>                      
        </div>
    </div>
@endsection
@section ('scripts')
    <script>
        $("#payload_img").change(function(){
            files = $("input[type=file]")[0];
            console.log(files);
            readURL(this);
        });

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#update_form").submit(function(e){

            var data = $(this).serialize();
            var url = "{{ Request::url() }}";
            var dataValue = new FormData(this);

            e.preventDefault();

            $.ajax({
                type: 'POST',
                //remove the .php from results.php.php
                url: "https://api.imgur.com/3/upload",
                //Add the request header
                headers: {
                    Authorization: 'Client-ID 64b7e5009ee260e'
                },
                contentType: 'application/x-www-form-urlencoded',
                //Add form data
                data: dataValue,
                contentType: false,
                cache: false,
                processData:false,
                success: function (response) {
                    var link = response.data.link;
                    data = {
                        'confirmation_img' : link,
                    };
                    $.ajax({
                        type : "POST",
                        url : url,
                        data : data,
                        success : function(data) {
                            console.log("data");
                            window.reload();
                        },
                        error : function (err) {
                            console.log(err.responseText);
                        }
                    });
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                }
            });
        });
    </script>
@endsection



<!-- /Chatra {/literal} -->

