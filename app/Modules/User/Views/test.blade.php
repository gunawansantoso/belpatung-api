@extends('layouts.app')
@section('title', 'Page Title')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h1> Verifikasi Akun Anda </h1>
                    </div>
                    <div class="panel-body">
                        <form id="upload_form" method="POST">
                            <div class="input-group">
                                <input id="payload_img" type="file" name="image" class="form-control">
                            </div>
                            <img id="preview" src="#" alt="your image" width="300" height="200" />
                            <br>
                            <button id="submit" type="submit" class="btn btn-primary"> Submit </button>
                        </form>

                        <form id="callback" action="verification/callback" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            <input type="hidden" id="response_img" name="response_img"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var files;

        $("#payload_img").change(function(){
            files = $("input[type=file]")[0];
            console.log(files);
            readURL(this);
        });

        $("#upload_form").submit(function (e) {

            e.preventDefault();

            var dataValue = new FormData(this);

            console.log("onsubmit", dataValue);

            $.ajax({
                type: 'POST',
                //remove the .php from results.php.php
                url: "https://api.imgur.com/3/upload",
                //Add the request header
                headers: {
                    Authorization: 'Client-ID 64b7e5009ee260e'
                },
                contentType: 'application/x-www-form-urlencoded',
                //Add form data
                data: dataValue,
                contentType: false,
                cache: false,
                processData:false,
                success: function (response) {

                    var link = response.data.link;
                    console.log(response.data);
                    $("#response_img").val(link);
                    $("#callback").submit();
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                }
            });
        });

        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
                files = new FormData(input.files[0]);
            }
        }


    </script>
@endsection

<!-- /Chatra {/literal} -->
