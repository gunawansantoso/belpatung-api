@extends('layouts.dashboard')
@section('title','Page Title')
@section('main')
<div class="container container-white">
	<p class="title-dashboard" > Edit Kelas Saya </p>
	<div class="green-line"></div>
	<br>
	<p class="sub-title-dashboard" ><i class="fa fa-list"></i> Edit kelas yang saya ambil atau sedang ikuti </p>
	<br>
	<form>
	  <div class="form-group">
	    <label for="nama_kelas">Nama kelas</label>
	    <input type="text" class="form-control" id="nama_kelas" required maxlength="40" value="kelas ini belum diedit">
	  </div>
	  <div class="form-group">
	    <label for="dana">Target Dana</label>
	    <input type="number" class="form-control" id="dana" required value="50000">
	  </div>
	  <div class="form-group">
	    <label for="posisi">Posisi Saya Sebagai</label>
	    <select name="posisi" required class="form-control">
	    	<option  value="sebagai peserta"> Sebagai Peserta</option>
	    	<option  value="sebagai peserta" selected> Sebagai Project Officer</option>
	    	<option  value="sebagai peserta"> Sebagai Pembicara</option>
	    </select>
	  </div>
	   <div class="form-group">
	    <label for="peserta">Estimasi Peserta kelas</label>
	    <input type="number" class="form-control" id="peserta" required value="50">
	  </div>
	  <div class="form-group">
	    <label for="link">Link kelas</label>
	    <input type="text" class="form-control" id="link" placeholder="belpatugan.com/kelas1" required value="http://belpatungan.com/kelas1">
	  </div>
	  <div class="form-group">
	    <label for="pembicara">Nama Pembicara</label>
	    <input list="pembicara" class="form-control" placeholder="Prof Amir ,  Prof Joko" required name="pembicara" type="text">
		<datalist id="pembicara">
		  <option value="Internet Explorer">
		  <option value="Firefox">
		  <option value="Google Chrome">
		  <option value="Opera">
		  <option value="Safari">
		</datalist>
	    
	  </div>

	  <div class="checkbox">
	    <label><input type="checkbox">Tambahkan saya sebagai pembicara</label>
	  </div>
	  <div class="form-group">
	    <label for="deadline">Deadline kelas</label>
	    <input type="date" class="form-control" id="deadline"  required value="5 Januari 2017">
	  </div>
	  <div class="form-group">
	    <label for="foto">Edit foto</label>
	    <input type="file" class="form-control" id="foto"  required>
	    <p>Foto saat ini</p>
	    <img src="images/kelas1.jpg" alt="" style="max-width:300px;">
	  </div>
	  <div class="form-group">
	    <label for="detail">Deskripsi kelas</label>
	    <textarea type="text" class="form-control textarea" id="detail"  required minlength="100">Deskripsi ini belum diganti</textarea>
	  </div>
	 
	  <button type="submit" class="btn btn-primary btn-lg">Edit kelas saya</button>
	</form>
</div>
@endsection