@extends('layouts.app')

@section('title', 'Page Title')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heWWading">
                        <h1> Belpatung Users</h1>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Activation</th>
                            <th>Verification</th>
                            <th>Verification Img</th>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>
                                        @if($user->isactivated)
                                            <a class="btn btn-success"> Activated </a>
                                        @else
                                            <a class="btn btn-danger"> Not Activated </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($user->isverified)
                                            <a class="btn btn-success"> Verified </a>
                                        @else
                                            <a class="btn btn-danger"> Not Verified </a>
                                        @endif
                                    </td>
                                    <td>
                                        <img src="{{ $user->link_foto_verfikasi }}" width="100" height="75"/>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



<!-- /Chatra {/literal} -->
