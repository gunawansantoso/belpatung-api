@extends('layouts.dashboard')
@section('title', 'Page Title')

@section('main')
    <div class="container container-white">
      <p class="title-dashboard" > Pengaturan Akun <span style="color: #329666"> > Validasi Akun</span></p>
      <div class="green-line"></div>
      <br>
      <form>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" id="email" required>
          <p>Kami akan mengirimkan kode verifikasi ke email anda</p>
        </div>
        <div class="form-group">
          <label for="telepon">telepon</label>
          <input type="number" class="form-control" id="telepon" required>
        </div>
        <div class="form-group">
          <label for="ktp">Upload Foto KTP</label>
          <input type="file" class="form-control" id="ktp" required>
          <p> KTP hanya akan digunakan untuk validasi, dan tidak akan kami salahgunakan </p>
        </div>

        <button type="submit" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModalverifikasi">Validasi Akun Saya</button>
      </form>
    </div>
@endsection
@section('scripts')
<script>
    @if(Auth::user()->isverified)
           $("input").attr('disabled', 'disabled'); //Disable
    @endif
    var files;
    $("#upload_form").submit(function (e) {

        e.preventDefault();

        var dataValue = new FormData(this);

        console.log("onsubmit", dataValue);

        $.ajax({
            type: 'POST',
            //remove the .php from results.php.php
            url: "https://api.imgur.com/3/upload",
            //Add the request header
            headers: {
                Authorization: 'Client-ID 64b7e5009ee260e'
            },
            contentType: 'application/x-www-form-urlencoded',
            //Add form data
            data: dataValue,
            contentType: false,
            cache: false,
            processData:false,
            success: function (response) {

                var link = response.data.link;
                console.log(response.data);
                $("#response_img").val(link);
                $("#callback").submit();
            },
            error: function (xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                console.log(err);
            }
        });
    });

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
            files = new FormData(input.files[0]);
        }
    }


</script>
@endsection

<!-- /Chatra {/literal} -->
