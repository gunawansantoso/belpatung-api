@extends('layouts.dashboard')

@section('main')
<div class="container container-white">
  <p class="title-dashboard" > Dashboard </p>
  <div class="green-line"></div>
  <br>
  <div class="row" style="width: 100%;margin: auto;">
    <div class="col-sm-3 nopadding">
      <div class="green-column " style="width: 95%;padding: 10px">
        <center>
          <div class="row">
            <div class="col-sm-4">
              <h2><i class="fa fa-line-chart fa-2x"></i></h2>
            </div>
            <div class="col-sm-8">
              <h3><b> 10.000</b></h3>
              <h6> Statistik </h6>
            </div>
          </div>
        </center>
      </div>
    </div>
    <div class="col-sm-3 nopadding">
      <div class="green-column" style="width: 95%;padding: 10px">
        <center>
          <div class="row">
            <div class="col-sm-4">
              <h2><i class="fa fa-graduation-cap fa-2x"></i></h2>
            </div>
            <div class="col-sm-8">
              <h3><b> 24</b></h3>
              <h6> kelas diikuti  </h6>
            </div>
          </div>
        </center>
      </div>
    </div>

    <div class="col-sm-3 nopadding">
      <div class="green-column" style="width: 95%;padding: 10px">
        <center>
          <div class="row">
            <div class="col-sm-4">
              <h2><i class="fa fa-graduation-cap fa-2x"></i></h2>
            </div>
            <div class="col-sm-8">
              <h3><b> 24</b></h3>
              <h6> Kelas diajar  </h6>
            </div>
          </div>
        </center>
      </div>
    </div>
  </div>
  <br>
  <div class="green-line"></div>
  <br>
  <p class="sub-title-dashboard" ><i class="fa fa-list"></i> Update Aktifitas Saya </p>
  <br>
  <form class="form-inline" >
  <div class="form-group" >
    <p> Cari aktifitas saya </p>
    <input class="form-control" style="width: 300px" placeholder="Cari aktifitas disini...">
    <button type="button" class="btn btn-success" style="margin-left: -5px"><i class="fa fa-search"></i></button>
  </div>
  </form>
  <br><br>
  <div class="aktifitas-list">
    <div class="border left-border-green">
      <p>Saya telah berdonasi <b> Rp 10.000</b> untuk kelas <b> blapteak</b></p>

      <a href="" style="text-decoration: none;color: #329666"><i class="fa fa-search"></i> Lihat detail </a>
    </div>
    <br>
    <div class="border left-border-green">
      <p>Saya telah berkontribusi sebagai <b> peserta</b> untuk kelas <b> blapteak</b></p>

      <a href="" style="text-decoration: none;color: #329666"><i class="fa fa-search"></i> Lihat detail </a>
    </div>
    <br>
    <div class="border left-border-green">
      <p>Saya telah berkontribusi sebagai <b> peserta</b> untuk kelas <b> blapteak</b></p>

      <a href="" style="text-decoration: none;color: #329666"><i class="fa fa-search"></i> Lihat detail </a>
    </div>
    <br>
    <center>
    <ul class="pagination">
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">4</a></li>
      <li><a href="#">5</a></li>
    </ul>
    </center>
  </div>
</form>
</div>
@endsection
