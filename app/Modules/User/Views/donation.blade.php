@extends('layouts.dashboard')

@section('title', 'Page Title')

@section('main')
    <div class="container container-white">
                    <p class="title-dashboard" > Celengan Saya <span style="color: #329666">( Rp 23.000.000 )</span></p>
                    <div class="green-line"></div>
                    <br>
                    <button type="" class="btn btn-lg btn-success"><i class="fa fa-plus"></i> Tambah Celengan</button>
                    <br><br>
                    <form class="form-inline" >
                    <div class="form-group" >
                        <p> Daftar celengan saya </p>
                        <input class="form-control" style="width: 300px" placeholder="Cari daftar celengan...">
                        <button type="button" class="btn btn-success" style="margin-left: -5px"><i class="fa fa-search"></i></button>
                    </div>
                    </form>     
                    <br>
                    <table class="table table-bordered table-responsive">
                        <thead class="table table-black-header">
                          <tr>
                            <th>Nama kelas</th>
                            <th>Nominal</th>
                            <th>Tanggal</th>
                            <th>Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Kelas 1 <a href="{{route('profile.donation.detail',1)}}"><i class="fa fa-search"></i> </a></td>
                            <td>Rp 10.000</td>
                            <td>22 Januari 2017</td>
                            <td><span class="label label-success">Paid</span></td>                        
                          </tr>
                          <tr>
                            <td>Kelas 1 <a href="detail_donasi.html"><i class="fa fa-search"></i> </a></td>
                            <td>Rp 10.000</td>
                            <td>22 Januari 2017</td>
                            <td><span class="label label-success">Paid</span></td>
                          </tr>
                          <tr>
                            <td>Kelas 1 <a href="detail_donasi.html"><i class="fa fa-search"></i> </a></td>
                            <td>Rp 10.000</td>
                            <td>22 Januari 2017</td>
                            <td><span class="label label-danger">Pending</span></td>
                          </tr>
                           <tr>
                            <td>Kelas 1 <a href="detail_donasi.html"><i class="fa fa-search"></i> </a></td>
                            <td>Rp 10.000</td>
                            <td>22 Januari 2017</td>
                            <td><span class="label label-success">Paid</span></td>                        
                          </tr>
                          <tr>
                            <td>Kelas 1 <a href="detail_donasi.html"><i class="fa fa-search"></i> </a></td>
                            <td>Rp 10.000</td>
                            <td>22 Januari 2017</td>
                            <td><span class="label label-success">Paid</span></td>
                          </tr>
                          <tr>
                            <td>Kelas 1 <a href="detail_donasi.html"><i class="fa fa-search"></i> </a></td>
                            <td>Rp 10.000</td>
                            <td>22 Januari 2017</td>
                            <td><span class="label label-danger">Pending</span></td>
                          </tr>
                        </tbody>
                    </table>
                    <br>
                        <center>
                        <ul class="pagination">
                          <li><a href="#">1</a></li>
                          <li><a href="#">2</a></li>
                          <li><a href="#">3</a></li>
                          <li><a href="#">4</a></li>
                          <li><a href="#">5</a></li>
                        </ul>
                        </center>
                </div>
@endsection



<!-- /Chatra {/literal} -->

