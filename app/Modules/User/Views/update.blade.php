@extends('layouts.dashboard')
@section('style')
    .list {
    margin: 0;
    padding: 0;
    list-style: none;
    }
    .list li {
    display: inline-block;
    margin: 0 .3em .3em 0;
    display: inline-block;
    height: 28px;
    line-height: 28px;
    padding: 0 1em;
    padding-right:0;
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 3px;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
    color: #333;
    font-size: 13px;
    text-decoration: none;
    -webkit-transition: .2s;
    transition: .2s;
    }
@endsection
@section('main')
<div class="container container-white">
					<p class="title-dashboard" > Profil Saya <span style="color: #329666"> > Kelola informasi</span></p>
					<div class="green-line"></div>
					<br>
					<form>
            <div class="form-group">
					    <label for="foto">Upload Foto Profile</label>
					    <small><i>*Tidak perlu diisi jika anda tidak ingin mengganti foto profile anda</i></small>
					    <input type="file" name="foto" class="form-control btn-default" id="foto">
					  </div>
					  <div class="form-group">
					    <label for="nama">Nama Lengkap</label>
					    <input type="text" class="form-control" id="nama" required value="John Doe">
					  </div>
					  <div class="form-group">
					    <label for="email">Email anda</label>
					    <input type="email" class="form-control" id="email" disabled="disabled" value="johndoe@gmail.com">
					  </div>
					  <div class="form-group">
					    <label for="email">Pilih Ketertarikan anda</label>
					    <br>
					     <label class="checkbox-inline"><input type="checkbox" value="">Option 1</label>
						<label class="checkbox-inline"><input type="checkbox" value="">Option 2</label>
						<label class="checkbox-inline"><input type="checkbox" value="">Option 3</label>
					  </div>

					  <div class="form-group">
					    <label for="nohp">No Hp/Whatsapp anda</label>
					    <input type="text" class="form-control" id="nohp" required value="08327532">
					  </div>
					  <div class="form-group">
					    <label for="lokasi">Propinsi anda</label>
					    <select name="lokasi_prop" class="form-control" id="lokasi">
					    	<option value="Kab. Anyer"> Kab. Anyer</option>
					    	<option value="Kab. Lain"> Kab. Lain</option>
					    </select>
					  </div>
					  <div class="form-group">
					    <label for="lokasi">Kota anda</label>
					    <select name="lokasi_kota" class="form-control" id="lokasi">
					    	<option value="Kab. Anyer"> Kab. Anyer</option>
					    	<option value="Kab. Lain"> Kab. Lain</option>
					    </select>
					  </div>
					   <div class="form-group">
					    <label for="lokasi_kab">Kecamatan anda</label>
					    <select name="lokasi" class="form-control" id="lokasi">
					    	<option value="Kab. Anyer"> Kab. Anyer</option>
					    	<option value="Kab. Lain"> Kab. Lain</option>
					    </select>
					  </div>
					  <div class="form-group">
					    <label for="nama">Nama Jalan</label>
					    <input type="text" class="form-control" id="jalan" required value="John Doe">
					  </div>
					  <div class="form-group">
					    <label for="biografi">Ceritakan diri anda</label>
					    <textarea name="biografi" class="form-control textarea" id="biografi">

					    </textarea>
					  </div>
					  <button type="submit" class="btn btn-primary btn-lg">Submit</button>
					</form>
				</div>
        <script>
                $("#payload_img").change(function(){
                    files = $("input[type=file]")[0];
                    console.log(files);
                    readURL(this);
                });

                function readURL(input) {

                    if (input.files && input.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $('#preview').attr('src', e.target.result);
                        };

                        reader.readAsDataURL(input.files[0]);
                    }
                }

                $("#update_form").submit(function(e){

                    var data = $(this).serialize();
                    var url = "{{ route('users.profile.store') }}";
                    var dataValue = new FormData(this);

                    e.preventDefault();

                    $.ajax({
                        type: 'POST',
                        //remove the .php from results.php.php
                        url: "https://api.imgur.com/3/upload",
                        //Add the request header
                        headers: {
                            Authorization: 'Client-ID 64b7e5009ee260e'
                        },
                        contentType: 'application/x-www-form-urlencoded',
                        //Add form data
                        data: dataValue,
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function (response) {
                            var link = response.data.link;
                            data = {
                                'link_foto_profile' : link,
                                'name' : data.name,
                                'nama_organisasi' : data.nama_organisasi
                            };

                            console.log("data", data.toString());
                            $.ajax({
                                type : "POST",
                                url : url,
                                data : data,
                                success : function(data) {
                                    console.log("data");
                                    $("#callback").submit();
                                },
                                failure : function (err) {
                                    console.log(err);
                                }
                            });
                        },
                        error: function (xhr, status, error) {
                            var err = eval("(" + xhr.responseText + ")");
                            console.log(err);
                        }
                    });
                });

                function removeInterest(interest) {
                    console.log('interest', interest);
                    $("#interest_value").val(interest.interest);
                    $("#input_remove_interest").val(interest.id);
                }

                function removeSkill(skill) {
                    $("#input_remove_skill").val(skill.id);
                }
        </script>
    </div>
@endsection
@section('scripts')
<script>
	    tinymce.init({
	    selector: '.textarea',
	    theme: 'modern',
	    height: 300,
	    plugins: [
	      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
	      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
	      'save table contextmenu directionality emoticons template paste textcolor'
	    ],
	    content_css: 'css/content.css',
	    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  print preview media fullpage | forecolor backcolor emoticons'
	  });
</script>
@endsection
