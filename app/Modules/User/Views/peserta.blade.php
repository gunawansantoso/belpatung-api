@extends('layouts.dashboard')

@section('title', 'Page Title')

@section('content')



      <div class="container container-white">
        <p class="title-dashboard" > Kelas Saya </p>
        <div class="green-line"></div>
        <br><br>
        <table class="table table-bordered">
            <thead>
              <tr>
                <th>Nama kelas</th>
                <th>Tanggal dibuat</th>
                <th>Jenis Patungan</th>
                <th style="width:20%">Tindakan</th>
              </tr>
              </tr>
            </thead>
            <tbody>
                @foreach($courses as $course)
                  <tr>
                    <td>{{$course->nama_kelas}}</td>
                    <td>{{$course->created_at}}</td>
                    <td>{{$course->type}}</td>
                    <td>
                      <div class="row">

                        <div class="col-sm-6">
                          <a href="/courses/{{$course->link_kelas}}" class="btn btn-sm btn-warning" style="width:100%">Detail</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
            </tbody>
         </table>
           <center>  {{ $courses->links() }}</center>
      </div>

@endsection



<!-- /Chatra {/literal} -->
