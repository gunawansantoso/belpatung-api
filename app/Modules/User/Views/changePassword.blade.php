@extends("layouts.dashboard")

@section('main')

    <div class="container container-white">
        <p class="title-dashboard" > Pengaturan Akun <span style="color: #329666"> > Ganti Password</span></p>
        <div class="green-line"></div>
        <br>
        <p> Setelah mengganti password, maka anda akan terlogout secara otomatis, dan kembali login dengan password baru anda</p>
        <br>
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{ route('profile.change.password.store') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="password">Password saat ini</label>
                <input type="password" class="form-control" id="password" name="old_password" required>
            </div>
            <div class="form-group">
                <label for="new-password"> Password Baru </label>
                <input type="password" class="form-control" id="new-password" name="password" required>
            </div>
            <div class="form-group">
                <label for="re-new-password" re>Ulangi Password Baru </label>
                <input type="password" class="form-control" id="re-new-password" name="password_confirmation" required>
            </div>
            <button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-key"></i> Submit</button>
        </form>
    </div>
@endsection
