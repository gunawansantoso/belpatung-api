<?php

namespace App\Modules;
use App\Modules\Admin\Repositories\AdminRepository;

class ModuleServiceProvider extends \Illuminate\Support\ServiceProvider {

    public function boot() {
        $modules = config("module.modules");

        while(list(,$module) = each($modules)) {

            // load the route for each of the module
            if (file_exists(__DIR__.'/'.$module.'/routes.php')) {
                include __DIR__.'/'.$module.'/routes.php';
            }

            // load the views
            if (is_dir(__DIR__.'/'.$module.'/Views')) {
                $this->loadViewsFrom(__DIR__.'/'.$module.'/Views', $module);
            }
        }
    }

    // NOTE : register module repository here
    public function register() {
        $this->app->singleton(UserRepository::class,
            UserRepository::class,
            CourseRepository::class,
            TransactionRepository::class,
            SpeakerRepository::class,
            AdminRepository::class);
    }
}
