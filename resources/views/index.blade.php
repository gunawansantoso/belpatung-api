@extends('layouts.app')
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection
@section('content')

<!-- section navigasi-->
<div style="background-image:url({{asset('/public/images/belajar22.png')}});background-size: 100%;background-attachment: fixed;padding: 20px;text-align: center;padding-top:120px;padding-bottom: 120px;margin-top: -30px">
  <h1 style="color:#ffffff;font-family: Montserrat;font-size: 4.5em"><b>Kelas apa yang Kamu Cari ?</b></h1>

  <br>

    <div style="display: inline-block;;width: 350px;padding:0 !important;margin:0 !important">
      <input type="text" name="keyword" id="keyword" class="form-control" style="width: 100%" placeholder="cari nama kelas">
    </div>
    <div style="display: inline-block;;width: 225px;padding:0 !important;margin:0 !important">
      <select name="kategori" class="form-control" style="width: 100%">
        <option value="value1">Kategori 1</option>
        <option value="value1">Kategori 1</option>
        <option value="value1">Kategori 1</option>
      </select>
    </div>
    <div style="display: inline-block;;width: 225px;padding:0 !important;margin:0 !important">
      <select name="jenis" class="form-control" style="width: 100%">
        <option value="value1">Kelas Gratis</option>
        <option value="value1">Kelas Berbayar</option>
      </select>
    </div>
    <div style="display: inline-block;;width: 225px;padding:0 !important;margin:0 !important">
      <select name="lokasi" class="form-control" style="width: 100%" id="location">
        <option value="all">semua</option>
        <option value="jakarta">Jakarta</option>
        <option value="bandung">Bandung</option>
      </select>
    </div>
    <div style="display: inline-block;;width: 80px;padding:0 !important;margin:0 !important">
      <button type="button" id="btn-search" class="btn btn-warning" style="color:#ffffff !important"><i class="fa fa-search"></i>Cari Kelas</button>
    </div>

  <br><br>
</div>
<!--end section navigasi-->
<!--SECTION 2-->
<div id="section-2" class="section">
  <div class="content-section">
    <div class="title">
      <h1><b> Mengapa Daftar di Belpatungan ? </b></h1>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <div class="container-video">

        </div>
      </div>
      <div class="col-sm-6">
        <div class="row" style="text-align: center">
          <div class="col-sm-6 values" style="min-height: 200px">
            <i class="fa fa-book fa-3x" style="color:#329666"></i>
            <h3>BISA BELAJAR GRATIS</h3>

            <p>Belpatungan mewadahi expert/guru/pelatih untuk memberikan kelas gratis berbagai jenis atau kategori pelatihan</p>
          </div>
          <div class="col-sm-6 values" style="min-height: 200px">
            <i class="fa fa-percent fa-3x" style="color:#329666"></i>
            <h3>BELAJAR LEBIH HEMAT</h3>

            <p>Pelatihan yang berbayar di Belpatungan dapat dibayar secara patungan atau mengajak orang lain berdonasi di kelas yang kita buat/ikuti</p>
          </div>
          <div class="col-sm-6 values" style="min-height: 200px">
            <i class="fa fa-money fa-3x" style="color:#329666"></i>
            <h3>ALTERNATIF PENGHASILAN</h3>

            <p>Pengguna Belpatungan yang memiliki keahlian khusus di suatu bidang dapat membuat agenda dan memberlakukan tarif kelas yang dibuat</p>
          </div>
          <div class="col-sm-6 values" style="min-height: 200px">
            <i class="fa fa-yelp fa-3x" style="color:#329666"></i>
            <h3>TEMPAT BERKUMPULNYA EO</h3>
            <p>Di Belpatungan, pengguna dapat menciptakan agenda pelatihan yang mendatangkan keuntungan (materil/non) lewat kolaborasi bersama</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="section-4" class="section">
  <div class="content-section">
    <div class="title" id="title-section-4">
      <h1><b> Kelas Organisasi -Perorangan </b></h1>
      <p style="font-size:1.5em">106 Kelas</p>
    </div>
    <div class="row" style="width: 90%;margin:auto">
      @foreach($courses as $course)
      <div class="col-sm-4 image-thumb  ">
        <div class="green-column-inverse">
          <img src="{{asset('/public/uploads')}}/{{ $course->link_foto }}">
          <div class="sinopsis-kelas">
            <h4><b> {{$course->nama_kelas}} </b></h4>
            <p class="lighttext"> Jenis kelas </p>
            <span class="label label-success">{{ $course->kategori }}</span>
            <hr>
            <p>{{ str_limit($course->deskripsi, $limit = 100, $end = '...') }}
            </p>
            <hr>
            <div class="media">
              <div class="media-left">
                <?php $user=$users->where('id',$course->user_id)->first(); ?>
                <img src="{{$user->link_foto_profile}}" class="media-object img-circle" style="width:60px">
              </div>
              <div class="media-body">
                <h4 class="media-heading">{{$user->name}}</h4>
                <p>{{$user->title}}</p>
              </div>
            </div>
            <div class="line-primary"></div>
            <div class="row">
              <div class="col-sm-4">
                <h3><b> {{round($course->total_dana/$course->target_dana,2)*100}} % </b></h3>
                <h5>Progress</h5>
              </div>
              <div class="col-sm-8">
                <div style="padding-top:2px;padding-bottom: 2px">
                  <i class="fa fa-user" style="color: #337ab7"> </i> {{ count($course->donations) }} peserta terdaftar
                </div>
                <div style="padding-top:2px;padding-bottom: 2px">
                  <i class="fa fa-search" style="color: #337ab7"> </i> <a href="{{url('courses')}}/{{$course->link_kelas}}"> Lihat Detail </a>
                </div>
                <div style="padding-top:2px;padding-bottom: 2px">
                @if(Auth::user())
                <?php 

		$registered = false;
		if ($participants && Auth::user())
                $registered=$participants->where('course_id',$course->id)->where('user_id',Auth::user()->id)->first(); 

                // echo count($registered);
                ?>
                @if(!$registered)
                <i class="fa fa-pencil" style="color: #337ab7"> </i> <a href="{{url('transactions/donation')}}/{{$course->id}}"> Join kelas </a>
                @else
                <i class="fa fa-check" style="color: #337ab7"> </i> <span> Terdaftar </span>
                @endif

                @else
                <i class="fa fa-pencil" style="color: #337ab7"> </i> <a href="{{url('transactions/donation')}}/{{$course->id}}"> Join kelas </a>
                @endif
                </div>

              </div>
            </div>
          </div>
        </div>
        <br>
      </div>
      @endforeach
    </div>
    <center>
      <a type="button" href="{{ url('search/courses/peserta') }}" class="btn btn-green btn-lg">Lihat Selengkapnya</a>
    </center>
  </div>
</div>
<!-- -->

<!--Section 5 -->
<div id="section-5" class="section">
  <div class="content-section">
    <div class="title" id="title-section-5">
      <h1><b> Kelas Pembicara</b></h1>
      <p style="font-size:1.5em">106 Kelas</p>
    </div>
    <div class="row" style="width: 90%;margin:auto">
    @foreach($speakerCourses as $course)
      <div class="col-sm-4 image-thumb  ">
        <div class="green-column-inverse">
          <img src="{{asset('/public/uploads')}}/{{ $course->link_foto }}">
          <div class="sinopsis-kelas">
            <h4><b> {{$course->nama_kelas}} </b></h4>
            <p class="lighttext"> Jenis kelas </p>
            <span class="label label-success">{{ $course->kategori }}</span>
            <hr>
            <p>{{ str_limit($course->deskripsi, $limit = 100, $end = '...') }}
            </p>
            <hr>
            <div class="media">
              <?php 
		if ($users)
		$user=$users->where('id',$course->user_id)->first(); ?>
              <div class="media-left">
                <img src="{{$user->link_foto_profile}}" class="media-object img-circle" style="width:60px">
              </div>
              <div class="media-body">
                <h4 class="media-heading">{{$user->name}}</h4>
                <p>{{$user->title}}</p>
              </div>
            </div>
            <div class="line-primary"></div>
            <div class="row">
              <div class="col-sm-4">
                <h3><b> {{round($course->total_dana/$course->target_dana,2)*100}} % </b></h3>
                <h5>Progress</h5>
              </div>
              <div class="col-sm-8">
                <div style="padding-top:2px;padding-bottom: 2px">
                  <i class="fa fa-user" style="color: #337ab7"> </i> {{ count($course->donations) }} peserta terdaftar
                </div>
                <div style="padding-top:2px;padding-bottom: 2px">
                  <i class="fa fa-search" style="color: #337ab7"> </i> <a href="{{url('courses')}}/{{$course->link_kelas}}"> Lihat Detail </a>
                </div>
                <div style="padding-top:2px;padding-bottom: 2px">
                @if(Auth::user())
                <?php 
		$registered = false;
		if ($participants && Auth::user())
		 $registered=$participants->where('course_id',$course->id)->where('user_id',Auth::user()->id)->first(); 
                // echo count($registered);
                ?>
                  @if(!$registered)
                  <i class="fa fa-pencil" style="color: #337ab7"> </i> <a href="{{url('transactions/donation')}}/{{$course->id}}"> Join kelas </a>
                  @else
                  <i class="fa fa-check" style="color: #337ab7"> </i> <span> Terdaftar </span>
                  @endif
                @else
                <i class="fa fa-pencil" style="color: #337ab7"> </i> <a href="{{url('transactions/donation')}}/{{$course->id}}"> Join kelas </a>
                @endif
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
      </div>
      @endforeach
    </div>
    <center>
      <a href="{{ url('search/courses/pembicara') }}" type="button" class="btn btn-green btn-lg" style="">Lihat Selengkapnya</a>
    </center>
  </div>
</div>
<!-- -->

<!--section pembicara-->
<div id="section-5" class="section">
  <div class="content-section">
    <div class="title" id="title-section-5">
      <h1><b> Pembicara</b></h1>
      <p style="font-size:1.5em">106 Pembicara</p>
    </div>
    <div class="row" style="width: 90%;margin:auto">
    <?php $speakers = $users->where('isspeaker',1); ?>
      @foreach($speakers->take(6) as $speaker)
      <div class="col-sm-2 image-thumb  ">
        <center>
        <img src="{{$speaker->link_foto_profile}}" alt="" style="width:100%;" class="img-circle img-responsive">
        <p>{{$speaker->name}}<br/><b>( {{$speaker->title}} ) </b></p>
        </center>
      </div>
      @endforeach

    </div>
    <center>
      <a href="{{ url("search/speakers") }}" type="button" class="btn btn-green btn-lg" style="">Lihat Selengkapnya</a>
    </center>
  </div>
</div>
<!--end section pembiara-->

<!--SECtion 6-->
<div id="section-6" class="section">
  <h1 style="font-family: Montserrat">Ayo jadi bagian dari perubahan!</h1>
  <br>
  <p>Saya tidak mau pengalaman dan pengetahuan yang saya miliki terkubur bersama tubuh saya ketika mati kelak -Bob sadino</p>
  <br>
  <div class="row" style="width:80%;margin: auto;">
    <div class="col-sm-6">
      <a href="{{route('courses.create')}}"><button type="button" class="btn-big btn-green btn-lg" style="width: 100%;opacity: 0.7">Buat Kelas</button></a>
    </div>
    <div class="col-sm-6">
      <button type="button" class="btn-big btn-green-inverse btn-lg" style="width: 100%;opacity: 0.7">Menjadi Pembicara</button>
    </div>
  </div>
  <br>
</div>
<!-- END SECTION 6-->

<!--SECTION 7-->
<div id="section-7" class="section">
<div class="content-section">
  <div class="title" id="title-section-5">
    <h1><b> Apa Kata Mereka ? </b></h1>
  </div>
  <div class="row">
    @foreach($testimonis as $testimoni)
    <div class="col-sm-4">
      <div class="testimoni">
	
        <?php 
	if ($users)
	$user=$users->where('id',$testimoni->user_id)->first(); ?>
        <img src="{{$user->link_foto_profile}}" class="img-circle img-responsive">
        <hr>
        <h3>{{$user->name}} </h3>
        <i>{{$testimoni->comment}}</i>
      </div>
    </div>
    @endforeach
  </div>
  <br>

  <br>
</div>
</div>
<!--END SECTION 7 -->

<!--SECTION OUR PARTNER-->
<div id="section-testimoni" class="section">
<div class="content-section">
  <div class="title" id="title-section-5">
    <h1><b> Partner Kami </b></h1>
  </div>
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <div class="row">
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
        </div>
      </div>

      <div class="item">
        <div class="row">
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
        </div>
      </div>

      <div class="item">
        <div class="row">
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
        </div>
      </div>

      <div class="item">
        <div class="row">
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
          <div class="col-sm-3">
             <img src="{{asset('/public/images/partner.png')}}" alt="Chania" style="width:100%">
          </div>
        </div>
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
</div>
<!---->

<script type="text/javascript">
$(document).ready(function(){

  $('#btn-search').click(function(event){

      var keyword = $('#keyword').val()== '' ? 'all' : $('#keyword').val() ;
      // var tanggal_mulai = $('#tanggal_mulai').val() == '' ? 'all' : $('#tanggal_mulai').val() ;
      // var tanggal_selesai =  $('#tanggal_akhir').val() == '' ? 'all' : $('#tanggal_akhir').val() ;
      var tanggal_mulai = 'all';
      var tanggal_selesai =  'all';
      if((tanggal_mulai == 'all' && tanggal_selesai != 'all') ||(tanggal_mulai != 'all' && tanggal_selesai == 'all')){
        alert('Lengkapi rentang tanggal yang diinginkan');
      }
      else if(tanggal_mulai > tanggal_selesai){
        alert('Tanggal selesai harus lebih besar dari tanggal mulai');
      }
      else{
        var url = "{{url('/courses/search')}}/"+keyword+"/"+$('#location').val()+"/"+ tanggal_mulai + "/" + tanggal_selesai;
        $(location).attr('href',url);
      }

  });

  $('#keyword').keypress(function(e){
      if(e.which == 13){
        $('#btn-search').click();
      }
  });
});


</script>
@endsection
