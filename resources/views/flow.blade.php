@extends('layouts.app')
@section('title', 'Page Title')
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection
@section('style')
.navbar{
  margin-bottom:0;
}
@endsection

@section('content')
<div style="background-image:url({{asset('belpatung-api/public/images/belajar22.png')}});background-size: 100%;background-attachment: fixed;padding: 20px;text-align: center;padding-top:120px;padding-bottom: 120px;margin-top: 0px">
  <h1 style="color:#ffffff;font-family: Montserrat;">Cara Kerja</h1>

  <br>
  <center>
    <div style="height: 2px;width:300px;background-color: #FFCC00">
  </center>
  </div>

</div>
<!-- SECTION CARA KERJA -->

<div id="section-cara-kerja" class="section">
  <div class="content-section">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#peserta"><h3>Sebagai Peserta</h3></a></li>
      <li><a data-toggle="tab" href="#pembicara"><h3>Sebagai Pembicara</h3></a></li>
    </ul>

    <div class="tab-content">
      <div id="peserta" class="tab-pane fade in active">
        <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/murid1.png')}}" alt="" style="width:100%;margin:auto">
        </div>
        <div class="col-sm-6" >
          <div class="content-bn" style="text-align: justify">
            <h4>Pendaftaran Pelatihan</h4>
            Pilih pelatihan yang kamu inginkan, daftar, bayar biaya pelatihan (jika pelatihan yang kamu inginkan berbayar, jika tidak cukup daftar sebagai peserta) dan tunggu informasi selanjutnya dari admin atau PO/pembuat kelas/pembicara.

          </div>
        </div>
      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6" >
          <div class="content-bn" >
            <h4>Pantau Postingan kelas yang kamu ikuti (pastikan semuanya berjalan positif) dan sudah siap terlaksana.</h4>
            Promosikan ke berbagai sosmed/chanel promosi milikmu/lembaga dan jejaringmu untuk membantu terselenggaranya kelas.

          </div>
        </div>
        <div class="col-sm-6">
          <img src="{{asset('belpatung-api/public/images/fwdikon/murid-2.png')}}" alt="" style="width:100%;margin:auto">
        </div>

      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/murid-3.png')}}" alt="" style="width:100%;margin:auto">
        </div>
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Masa Pelatihan</h4>
            Pastikan Kamu ikut serta saat kegiatan agar kamu memiliki data keikutsertaan yang berguna sebagai statistik kamu dimasa datang untuk mendapatkan berbagai promo/keuntungan dari belpat dan juga sertifikat jika pelatihan/kelas memenuhi syarat dibuatkan sertifikat peserta.

          </div>
        </div>

      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Pasca Pelatihan</h4>
            Setelah masa pelatihan berakhir, kamu disarankan memberikan rating dan komentar terhadap pelatihan yang telah kamu ikuti melalui website belpatungan agar kamu bisa mendapatkan poin dan dapat mencairkan sertifikatmu.

          </div>
        </div>
        <div class="col-sm-6">
          <img src="{{asset('belpatung-api/public/images/fwdikon/murid-4.png')}}" alt="" style="width:100%;margin:auto">
        </div>

      </div>
      </div>
      <div id="pembicara" class="tab-pane fade">
        <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/pengajar-1.png')}}" alt="" style="width:100%;margin:auto">
        </div>
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Validasi AKun</h4>
            Pembuat kelas baik itu dengan hak sebagai pembicara ataupun  Project Officer harus memvalidasi akun sesuai ketentuan belpat terlebih dahulu sebelum membuat kelas. Validasi meliputi upload Identitas Diri (KTP) ke halaman profil. Hal ini harus dilakukan agar kelas yang dibuat dapat tampil di halaman web dan disetujui admin belpat.
          </div>
        </div>
      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Inisiasi Pelatihan</h4>
            Buat pelatihan yang kamu inginkan. Buat rancangan berupa rincian keperluan pelatihan sesuai tema, budget, dan detail lainnya. Jika dibutuhkan, kamu dapat membuat tim dengan cara mengajak user lain untuk membantu terlaksananya pelatihan dan menyuruh mereka mendaftar (jika belum)

          </div>
        </div>
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/pengajar-2.png')}}" alt="" style="width:100%;margin:auto">
        </div>

      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/pengajar-3.png')}}" alt="" style="width:100%;margin:auto">
        </div>
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Fiksasi keperluan pelatihan</h4>
            Kamu (dan tim) harus memfiksasi segala keperluan pelatihan secara mandiri. Belpatungan akan memberikan informasi berupa kontak pelatih, alamat tempat pelatihan, dan informasi lainnya untuk kebutuhan jika pelatihan dalam keadaan membutuhkan dan belpat memiliki sumber dayanya.
          </div>
        </div>
      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Crosscheck kelasmu</h4>
            Pantau Postingan event kelasmu dan update detail kelas ciptaanmu. Pastikan semuanya berjalan positif, anggaran kelas terpenuhi (untuk kelas berbayar) dan kuota terpenuhi pula.

          </div>
        </div>
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/pengajar-4.png')}}" alt="" style="width:100%;margin:auto">
        </div>

      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/pengajar-5.png')}}" alt="" style="width:100%;margin:auto">
        </div>
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Publikasikan</h4>
            Promosikan ke berbagai sosmed/chanel promosi milikmu/lembaga dan jejaringmu agar kuota peserta dan sumberdaya lainnya terpenuhi dan bertambah.
          </div>
        </div>
      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Masa Pendaftaran Peserta</h4>
            Setelah segala keperluan pelatihan difiksasi, saatnya memasuki masa pendaftaran peserta. Saat ini kamu tinggal menunggu peserta sampai waktu pendaftaran berakhir dan teruslah berkomunikasi dengan Tim dari belpat agar mereka dapat mengetahui progresmu dan nantinya bisa membantumu.

          </div>
        </div>
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/pengajar-6.png')}}" alt="" style="width:100%;margin:auto">
        </div>

      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/pengajar-7.png')}}" alt="" style="width:100%;margin:auto">
        </div>
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Masa Persiapan Pra Kelas</h4>
            Setelah waktu pendaftaran berakhir, kamu harus segera mempersiapkan segala keperluan pelatihan sebelum hari-H. dan pastikan semuanya sesuai SOP sesuai yang disampaikan tim belpat.
          </div>
        </div>
      </div>
      <hr>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Masa Pelatihan /Kelas/Event</h4>
            (pastikan setiap sumberdaya telah siap dan jangan lupa untuk membuat setiap orang mendapatkan peran di hari acara seperti MC/moderator, Dokumentasi dan lainnya.

          </div>
        </div>
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/pengajar-8.png')}}" alt="" style="width:100%;margin:auto">
        </div>

      </div>
      <div class="row" style="width: 80%;margin:auto">
        <div class="col-sm-6 ">
          <img src="{{asset('belpatung-api/public/images/fwdikon/pengajar-9.png')}}" alt="" style="width:100%;margin:auto">
        </div>
        <div class="col-sm-6" >
          <div class="content-bn">
            <h4>Pasca Pelatihan</h4>
            Terima Reward (Sertifikat (pembicara/PO). Serta Fee jika pelatihan adalah pelatihan berbayar). Ini dapat kamu lihat di celengan belpatunganmu atau di transfer langsung oleh tim belpat (koordinasikan dengan tim Belpat)
          </div>
        </div>
      </div>
      </div>
    </div>

</div>
</div>
<!-- END SECTION CARA KERJA-->
@endsection
