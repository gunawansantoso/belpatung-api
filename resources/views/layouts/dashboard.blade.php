<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pembayaran</title>
    <title>Beranda</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href={{ asset("css/style.css")}}>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Damion">
    <script src="{{ asset('bootstrap/js/bootstrap.min.js')}}" type="text/javascript" charset="utf-8" async defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src=" {{ asset('js/myjs.js') }}" type="text/javascript" charset="utf-8" async defer></script>
    <style type="text/css">
        @yield('style')
    </style>
</head>
<!--  UNTUK MENU DAFTAR DI HEADER. LIHAT BAGIAN BOOTSTRAP MODAL SECTION -> DAFTAR_MODAL-->
<body>
<div id="">
    <div id="nav">
        <div id="nav-logo"><a href="#"> LOGO HERE </a></div>

        <div class="nav-menu" id="navmenu1"> CARA KERJA </div>
        <div class="nav-menu" id="navmenu2"> TENTANG KAMI </div>
        <div class="nav-menu" id="navmenu3"> BLOG </div>
        <div class="nav-menu" id="navmenu4"> KALKEL </div>
        <div id="nav-flag"> <img src="{{ asset('images/flag_indo.png') }}"></div>
        @yield('navbar')
        @if(Auth::check())
            <div id="nav-button"><a href="{{ url('/logout') }}" class="btn btn-green"
                                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Logout
                </a></div>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @endif
    </div>
</div>
@endsection
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection
@section('style')
.navbar{
  margin-bottom:0;
}
@endsection
@section('content')

<div class="body-wrapper-dashboard">
	<div class="content-dashboard">
		<div class="row">
			<div class="col-sm-3 nopadding" style="background-color: #ffffff">
				<div class="picture-wrap">
					<img src="{{@Auth::user()->link_foto_profile ? @Auth::user()->link_foto_profile :  asset('belpatung-api/public/images/img_avatar1.png')}}" class="img-circle" >
					<br><br>
					<p>{{@Auth::user()->name}}</p>
				</div>
				<br>
				<div class="row-3-button">
					<center>
						<a href="{{ route('profile.update') }}"><button  type="button" class="btn btn-row-3-button"  data-toggle="tooltip" data-placement="bottom" title="Edit profil"><i class="fa fa-edit fa-2x"></i></button></a>
						<a href="{{ route('courses.create') }}"><button type="button" class="btn btn-row-3-button" data-toggle="tooltip" data-placement="bottom" title="Buat kelas"><i class="fa fa-graduation-cap fa-2x"></i>

						</button></a>
						<a href="{{url('/profile/testimoni')}}"><button href="" type="button" class="btn btn-row-3-button" data-toggle="tooltip" data-placement="bottom" title="Buat Testimoni"><i class="fa fa-comments-o fa-2x"></i></button></a>
					</center>
				</div>
				<br>
				<a href="{{route('user.dashboard')}}" style="text-decoration: none"><div class="sidebar_menu_dashboard {{ \Route::current()->getName()=='user.dashboard' ?'smd_active':''}}"> <b><i class="fa fa-dashboard"></i> &nbsp Beranda saya </b></div></a>
				<div class="sidebar_menu_dashboard {{(\Route::current()->getName()=='profile.update' || \Route::current()->getName()=='profile.participation')?'smd_active':''}}" id="flip"> <b><i class="fa fa-user"></i>&nbsp &nbsp Profile saya &nbsp </b> <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
					<div id="panel" class="panel panel-class">
						<a href="{{ route('profile.update') }}" style="text-decoration: none;color: #4d4d4d"><div style="width: 100%"> Kelola informasi </div></a>
						<br>
						<a href="{{ route('profile.participation') }}" style="text-decoration: none;color: #4d4d4d"><div style="width: 100%"> Daftar Kelas Saya </div></a>
						<br>
					</div>
				</div>

				<div class="sidebar_menu_dashboard {{(\Route::current()->getName()=='profile.verification' || \Route::current()->getName()=='profile.change.password')?'smd_active':''}}" id="flip2"> <b><i class="fa fa-cog"></i>&nbsp &nbsp Pengaturan akun &nbsp </b> <span class="pull-right"><i class="fa fa-chevron-down"></i></span>
					<div id="panel2" class="panel panel-class">
						<a href="{{ route('profile.verification') }}" style="text-decoration: none;color: #4d4d4d"><div style="width: 100%"> Validasi akun </div></a>
						<br>
						<a href="{{ route('profile.change.password') }}" style="text-decoration: none;color: #4d4d4d"><div style="width: 100%"> Ganti password </div></a>
						<br>
					</div>
				</div>

				<div class="sidebar_menu_dashboard {{(\Route::current()->getName()=='user.donation' || \Route::current()->getName()=='profile.donation.add' || \Route::current()->getName()=='profile.donation.withdraw')?'smd_active':''}}" id="flip3"> <b><i class="fa fa-money"></i> &nbsp Celengan saya </b><span class="pull-right"><i class="fa fa-chevron-down"></i></span>
					<div id="panel3" class="panel panel-class">
						<a href="{{ route('user.donation') }}" style="text-decoration: none;color: #4d4d4d"><div style="width: 100%"> Lihat Celengan </div></a>
						<br>
						<a href="{{route('profile.donation.add')}}" style="text-decoration: none;color: #4d4d4d"><div style="width: 100%"> Tambah Celengan </div></a>
						<br>
						<a href="{{route('profile.donation.withdraw')}}" style="text-decoration: none;color: #4d4d4d"><div style="width: 100%"> Cairkan Dana </div></a>
						<!-- <br> -->
						<!-- <a href="#" style="text-decoration: none;color: #4d4d4d"><div style="width: 100%"> Another Menu</div></a> -->
						<br>
					</div>
				</div>
			</div>
			<div class="col-sm-9">
				@yield('main')
			</div>
		</div>
	</div>
	<br><br>
</div>

<div id="review_modal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Silahkan tuliskan review</h4>
	      </div>
	      <div class="modal-body">
	        <p style="font-size:1.2em">Disini Judul Kelas</p>
	        <hr>
	        <p>Review</p>
	        <form actio="" method="">
	        	<textarea style="height:120px" class="form-control" required></textarea>
		        <br>
		        <button type="submit" class="btn btn-info">Submit review</button>
	        </form>

	        <hr>
	        <p>Berikan Rating kelas ini (skala 1 -5 )</p>
	        <form action="" method="">
	        	<div class="radio">
				  <label><input type="radio" name="optradio">1 (butuh banyak perbaikan kedepannya) </label>
				</div>
				<div class="radio">
				  <label><input type="radio" name="optradio">2 ( Butuh sedikit perbaikan dan pengembangan Pembicara dan organisasi )</label>
				</div>
				<div class="radio">
				  <label><input type="radio" name="optradio">3 ( konten materi cukup baik, cukup mudah dipahami dan pembicara bagus ) </label>
				</div>
				<div class="radio">
				  <label><input type="radio" name="optradio">4 ( Kelas menarik, materi baik, mudah dipahami dan pembicara ahli )</label>
				</div>
				<div class="radio">
				  <label><input type="radio" name="optradio">5 (  sangat menarik dan sangat baik segala aspeknya )</label>
				</div>
				<br>
				<button type="submit" class="btn btn-warning" style="color:#ffffff !important">Kirim Rating </button>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

@endsection
@section('scripts')
<script>
	    tinymce.init({
	    selector: '.textarea',
	    theme: 'modern',
	    height: 300,
	    plugins: [
	      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
	      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
	      'save table contextmenu directionality emoticons template paste textcolor'
	    ],
	    content_css: 'css/content.css',
	    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent |  print preview media fullpage | forecolor backcolor emoticons'
	  });
</script>
@endsection
