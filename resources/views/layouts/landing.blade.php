<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--
    <meta property="og:url"           content="https://belpatung-api.herokuapp.com/courses" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Belpatungan" />
    <meta property="og:description"   content="Website terkece sepanjang masa" />
    <meta property="og:image"         content="http://blog.caranddriver.com/wp-content/uploads/2015/11/BMW-2-series.jpg" /> -->

    <title>Beranda</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href={{ asset("css/style.css")}}>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Damion">
    <script src="{{ asset('bootstrap/js/bootstrap.min.js')}}" type="text/javascript" charset="utf-8" async defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src=" {{ asset('js/myjs.js') }}" type="text/javascript" charset="utf-8" async defer></script>
    <style type="text/css">
        @yield('style')
    </style>
</head>
<body>
    <div id="header">
      <nav class="navbar navbar-default" style="opacity: 0.9">
          <div class="container-fluid">
              <div class="navbar-header">
               <a class="navbar-brand" href="#">LOGO HERE </a>
             </div>
                <ul class="nav navbar-nav">
                  <li class="active" ><a href="#" style="color:#329666">CARA KERJA </a></li>
        		      <li><a href="#" style="color:#329666">TENTANG KAMI</a></li>
        		      <li><a href="#" style="color:#329666">BLOG</a></li>
        		      <li><a href="#" style="color:#329666">KALKEL</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  Pilih bahasa : <img src="{{ asset('images/flag_indo.png') }}">
                  @yield('navbar')
                  @if(Auth::check())
                      <a href="{{ url('/logout') }}" class="btn btn-green"
                         onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                          Logout
                          </a>
                      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                      <!-- <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button> -->

                  @endif
                </ul>
          </div>
        </nav>
    </div>
    @yield('content')
    <!-- Scripts -->
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/589bd4a2a1e7630ada59c5bc/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');f
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
