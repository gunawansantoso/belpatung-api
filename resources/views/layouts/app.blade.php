<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>
    @yield('title')
  </title>
	<link rel="stylesheet" href="{{asset('/public/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('/public/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto">
 	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway">
 	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat">
  	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Damion">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="{{asset('/public/bootstrap/js/bootstrap.min.js')}}" type="text/javascript" charset="utf-8" async defer></script>
 	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src='https://cdn.tinymce.com/4/tinymce.min.js'></script>
 	<script src="{{asset('/public/js/myjs.js')}}" type="text/javascript" charset="utf-8" async defer></script>
  <style>
  		body{
  			font-family: 'Montserrat';
  		}
		.navbar{
				margin-bottom: 0;
				border-radius: 0 !important;
			}
      @yield('style')
  </style>
</head>
<!--  UNTUK MENU DAFTAR DI HEADER. LIHAT BAGIAN BOOTSTRAP MODAL SECTION -> DAFTAR_MODAL-->
<body>
  <div id="header" style="position: relative;z-index:2">
    <nav class="navbar navbar-default" style="opacity: 0.9">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="{{url('/')}}"><img class="logo" id="nav-logo" src="{{asset('/public/images/logo.png')}}"/></a>
        </div>
        <ul class="nav navbar-nav">
					<li class="{{\Request::is('dashboard') ? 'active' : ''}}"><a href="{{ route('user.dashboard') }}" style="color:#329666">DASHBOARD</a></li>
          <li class="{{\Request::is('flow') ? 'active' : ''}}" ><a href="{{url('flow')}}" style="color:#329666">CARA KERJA </a></li>
          <li class="{{\Request::is('about') ? 'active' : ''}}"><a href="{{url('about')}}" style="color:#329666">TENTANG KAMI</a></li>
          <li class="{{\Request::is('blog') ? 'active' : ''}}"><a href="{{url('blog')}}" style="color:#329666">BLOG</a></li>
          <li class="{{\Request::is('calendar') ? 'active' : ''}}"><a href="{{url('calendar')}}" style="color:#329666">KALKEL</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          Pilih bahasa : <img src="{{asset('/public/images/flag_indo.png')}}">
          <!-- <button type="button" class="btn btn-green navbar-btn" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
          <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button> -->
					@yield('navbar')
          @if(Auth::check())
              <div id="nav-button"><a href="{{ url('/logout') }}" class="btn btn-green"
                                      onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
                      Logout
                  </a></div>
              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
          @endif </ul>

        </ul>
      </div>
    </nav>
    <div id="myCarousel2" class="carousel slide" data-ride="carousel2" style="margin-top:-20px;display:none">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel2" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel2" data-slide-to="1"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox" style="min-height:200px;max-height: 600px">
        <div class="item active">
          <img src="images/belajar1.jpg" alt="Chania" style="width:100%">
        </div>

        <div class="item">
          <img src="images/belajar2.jpg" alt="Chania" style="width:100%">
        </div>

      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
  @yield('content')
	<!--SECTION FOOTER-->
	<div class="section" id="section-footer" >
		<div class="content-section" style="width:80%;margin: auto">
			<hr>
			<div class="row">
				<div class="col-sm-3">
					<h3 style="color:#329666;font-family: Montserrat">Tentang Belpatungan</h2>
					<p>Merupakan crowfunding platform dan marketplace pelatihan pertama yang menjadi katalisator perbaikan sistem pendidikan informal dunia agar belajar lebih mudah, murah, efektif dan efisien.
						</p>
				</div>
				<div class="col-sm-3">
					<h3 style="color:#329666;font-family: Montserrat">Pelajari Lebih Lanjut</h2>
					<p>
						<a href="" style="color:#777777 !important">FAQ</a>
						<br><br>
						<a href="" style="color:#777777 !important">Syarat dan Ketentuan</a>
						<br><br>
						<a href="" style="color:#777777 !important">Kebijakan Privasi</a>
						<br><br>
						<a href="" style="color:#777777 !important">Cara Kerja</a>
					</p>
				</div>
				<div class="col-sm-3">
					<h3 style="color:#329666;font-family: Montserrat">Kontak Kami</h2>
					<p>
						<i class="fa fa-phone"> </i> CP1 : +627895784366 (Rizki)
						<br><br>
						<i class="fa fa-phone"> </i> CP2 : +6281282697335 (Vio)
						<br><br>
						<i class="fa fa-envelope"> </i> belajarpatungan@gmail.com
						<hr>
						<p><i>Jalan Sunan Giri no.1 , Rawamangun, Jakarta Timur</i></p>
					</p>
				</div>
				<div class="col-sm-3">
					<h3 style="color:#329666;font-family: Montserrat">Sitemap</h2>
					<p>
						<a href="" style="color:#777777 !important">Blog</a>
						<br><br>
						<a href="" style="color:#777777 !important">Cari Kelas</a>
						<br><br>
						<a href="" style="color:#777777 !important">Cari Pembicara</a>
					</p>

				</div>
			</div>
			<hr>
			<center>
				<a href="https://m.facebook.com/profile.php?id=167191960416896"><i class="fa fa-facebook"></i> </a>
				&nbsp &nbsp <a href="http://www.twitter.com/BelPatungan"><span> <i class="fa fa-twitter"></i> </span></a>
				&nbsp &nbsp <a href="http://www.instagram.com/BelPatungan_yuk"><span> <i class="fa fa-instagram"></i> </span></a>

			</center>
		</div>
	</div>
	<!--END SCTION-->


	<!-- DAFTAR Modal -->
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header header-daftar">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title"><center>Register</center></h4>
	      </div>
	      <div class="modal-body">
		      	<form action="{{ url('/register') }}" method="POST" accept-charset="utf-8">
		      		<div class="row">
			        	<div class="col-sm-6">
			        		<div class="form-group">
							    <input required type="text" class="form-control" id="name" name="name" placeholder="Masukan nama lengkap ...">
							</div>
							<div class="form-group">
							    <input required type="password" class="form-control" id="password" name="password" placeholder="Masukan password anda ...">
							</div>
							<div class="form-group">
							    <input required type="number" min="0" class="form-control" id="phone_number" name="phone_number" placeholder="Masukan telepon anda ...">
							</div>
							<div class="form-group">
							    <select name="skill" class="form-control">
							    	<option value="skill1"> Skill 1</option>
							    	<option value="skill1"> Skill 1</option>
							    	<option value="skill1"> Skill 1</option>
							    	<option value="skill1"> Skill 1</option>
							    </select>
							</div>
							<br>
							 <center><button type="submit" class="btn btn-green" style="width: 100%" value="daftarkan saya"> <i class="fa fa-user-plus"> </i> &nbsp Daftarkan saya </button></center>
			       			 <br>
			        	</div>
			        	<div class="col-sm-6">
			        		<div class="form-group">
							    <input required type="email" class="form-control" id="email" name="email" placeholder="Masukan email anda ...">
							</div>
							<div class="form-group">
							    <input required type="password" class="form-control" id="password-confirm" name="password_confirmation" placeholder="Masukan password kembali ...">
							</div>
							<div class="form-group">
							    <input required type="text" class="form-control" id="organization" name="organization" placeholder="Masukan nama organisasi ...">
							</div>
<!-- 							<div class="form-group">
							    <textarea required type="text" class="form-control" id="organisasi" name="organisasi" placeholder="Tuliskan sesuatu ??? ..." style="height: 100px"></textarea>
							</div> -->
			        	</div>
			        </div>
			        <br>
			        <hr>
			        <p style="margin-top: -30px"> <center><b> atau daftar dengan</b> </center></p>
			        <br>
		      	</form>

		        <div class="row">
		        	<div class="col-sm-6">
		        		<center><a href="{{ url('auth/facebook') }}" class="btn btn-primary" style="width: 100%"> <i class="fa fa-facebook"> </i> &nbsp Facebook </a></center><br>
		        	</div>
		        	<div class="col-sm-6">
		        		<center><a href="{{ url('auth/google')}}" class="btn btn-danger" style="width: 100%"><i class="fa fa-google-plus"> </i> &nbsp Google + </a></center>
		        	</div>
		        </div>
	      </div>
	      <div class="modal-footer footer-daftar">
	       <p><center>Belum punya akun ? <a href="" style="text-decoration: none;color: #ffffff"><b>Daftar disini </b></a></center></p>
	      </div>
	    </div>

	  </div>
	</div>
	<!-- END DAFTAR MODAL -->


	<!--LOGIN MODAL -->
	<!-- Modal -->
	<div id="myModal2" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-sm">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header header-login">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title ">Login akun</h4>
	      </div>
	      <div class="modal-body">
		       <form role="form" method="POST" action="{{ url('/login') }}" accept-charset="utf-8">
		       		{{ csrf_field() }}
		       		<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
					    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Masukkan Email">
					     @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
					</div>
					<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
					    <input type="password" class="form-control" id="password" name="password" placeholder="Masukan password anda ..." required>
					    @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
					</div>
					<center><button type="submit" class="btn btn-green" style="width: 100%"><i class="fa fa-sign-in"></i> &nbsp Masuk akun </button></center>
					<hr>
			        <p style="margin-top: 20px"> <center><b> atau login dengan</b> </center></p>
					<hr>
			        
			        <center><a href="{{ url('/auth/facebook') }}" class="btn btn-primary" style="width: 100%"> <i class="fa fa-facebook"> </i> &nbsp Facebook </a></center>
			        <br>
			        <center><a href="{{ url('/auth/google') }}" class="btn btn-danger" style="width: 100%"><i class="fa fa-google-plus"> </i> &nbsp Google + </a></center>
		       </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

    <!-- Scripts -->
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/589bd4a2a1e7630ada59c5bc/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');f
s0.parentNode.insertBefore(s1,s0);
})();
</script>

@yield('scripts')
<!--End of Tawk.to Script-->
</body>
</html>
