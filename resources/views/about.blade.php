@extends('layouts.app')
@section('title', 'Page Title')
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection

@section('content')
<div id="section-tentang-kami" class="section">
  <div class="content-section">
  <div class="title" id="title-section-5">
    <h1><b> Tentang Kami </b></h1>
  </div>
  <div class="row">
    <div class="col-sm-3">
      <a href="#konten-ttg-1" style="text-decoration: none"><div id="tentang-kami-1" >
      Tentang kami
      </div></a>
      <a href="#konten-ttg-2" style="text-decoration: none"><div id="tentang-kami-2">
      Our Family
      </div></a>
      <a href="#konten-ttg-3" style="text-decoration: none"><div id="tentang-kami-3">
      Kontak Kami
      </div></a>
    </div>
    <div class="col-sm-9" id="konten-tentang-kami">
      <div id="konten-ttg-1">
      Saat ini, Indonesia memiliki peluang yang sangat besar untuk menjadi Negara yang lebih makmur dengan banyaknya jumlah masyarakat usia produktif hingga 2030 nanti. Anehnya, jumlah usia produktif yang banyak tersebut tidak diimbangi dengan kualitas SDM-nya. Kompas tanggal 25 November 2015 melansir berita bahwa Institute of Management Development (IMD) yang merupakan lembaga pendidikan bisnis terkemuka di Swiss melaporkan hasil penelitiannya berjudul IMD World Talent Report 2015. Penelitian ini berbasis survei yang menghasilkan peringkat tenaga berbakat dan terampil di dunia pada tahun 2015. Dalam laporan tersebut dinyatakan bahwa peringkat Indonesia turun 16 peringkat dari peringkat ke-25 pada tahun 2014, menjadi peringkat ke-41 pada tahun 2015. Posisi Indonesia berada jauh di bawah posisi negara tetangga seperti Singapura, Malaysia, bahkan Thailand.
      <br><br>

      Kita mengetahui, ada berbagai balai latihan kerja dan training yang terjadi hampir setiap bulan, di berbagai tempat. Sayangnnya, berbagai pelatihan ini tidak efektif dan efisien disebabkan oleh terbatasnya akses informasi bagi setiap orang, tidak sebandingnya jumlah peserta pelatihan dengan wadah pelatihan yang tersedia, dan sumberdaya yang masyarakat miliki tidak mencukupi untuk mengikuti pelatihan (misal: berbayar mahal) serta berbagai permasalahan lainnya yang membuat output pelatihan tersebut belum mampu secara signifikan mengatasi masalah kualitas SDM Indonesia yang berkaitan erat dengan kualitas tenaga kerja.
      <br><br>
      Untuk membantu mengatasi permasalahan tersebut, kami mengusulkan Belajar Patungan (Belpatungan.com). Belpatungan.com ini adalah sosial bisnis berupa sebuah wadah (website) dengan sitem crowdfunding dan marketplace pertama di Dunia yang mempertemukan orang-orang yang membutuhkan pelatihan/workshop/coaching/kursus dalam peningkatan kemampuan hardskill dan softskill mereka dengan orang-orang yang memiliki keahlian (sesuai permintaan user) dan mempunyai keinginan untuk melatih user, baik secara profesional (dibayar) atau sukarela (gratis). Belpatungan.com ini juga memiliki fungsi sebagai mini search engine yang terfokus pada pembuatan dan pencarian kelas-belajar, baik itu kelas-belajar berbentuk pelatihan, workshop, seminar, kursus, pengajian, ataupun mentoring. Selain itu, belpatungan.com juga akan menghimpun dan menyediakan database pembicara berbayar dan gratis untuk berbagai jenis bidang keahlian.
      </div>

      <div id="konten-ttg-2">
      <b><h3 style="font-family: Montserrat;color:#000000">Yuk Kenalan Dengan Anggota Kami</h3></b>
        <hr>
        <div class="row" style="width:90%;margin:auto">
          <div class="col-sm-4" style="min-height:400px;text-align: center">
            <img src="{{asset('belpatung-api/public/images/fotorizki.png')}}" alt="" style="width: 90%;margin:auto;max-height:300px">
            <br>
            <b>Rizki Ananda</b>
            <p>CEO & Cofounder</p>

            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/twitterlogo.png')}}" alt="" style="width: 100%">
            </div>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/instagramlogo.png')}}" alt="" style="width: 100%">
            </div>
            <br><br>
          </div>
          <div class="col-sm-4" style="min-height:400px;text-align: center">
            <img src="{{asset('belpatung-api/public/images/fotowawan.png')}}" alt="" style="width: 90%;margin:auto;max-height:300px">
            <br>
            <b>Hiemawan Arie</b>
            <p>CMO & Cofounder</p>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/twitterlogo.png')}}" alt="" style="width: 100%">
            </div>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/instagramlogo.png')}}" alt="" style="width: 100%">
            </div>
            <br><br>
          </div>
          <div class="col-sm-4" style="min-height:400px;text-align: center">
            <img src="{{asset('belpatung-api/public/images/fotovio.png')}}" alt="" style="width: 90%;margin:auto;max-height:300px">
            <br>
            <b>Violita Sabilla</b>
            <p>COO & Cofounder</p>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/twitterlogo.png')}}" alt="" style="width: 100%">
            </div>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/instagramlogo.png')}}" alt="" style="width: 100%">
            </div>
            <br><br>
          </div>
          <div class="col-sm-4" style="min-height:400px;text-align: center">
            <img src="{{asset('belpatung-api/public/images/fotobeta.png')}}" alt="" style="width: 90%;margin:auto;max-height:300px">
            <br>
            <b>Farah Beta Maulida</b>
            <p>CFO & Cofounder</p>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/twitterlogo.png')}}" alt="" style="width: 100%">
            </div>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/instagramlogo.png')}}" alt="" style="width: 100%">
            </div>
            <br><br>
          </div>
          <div class="col-sm-4" style="min-height:400px;text-align: center">
            <img src="{{asset('belpatung-api/public/images/fotoami.png')}}" alt="" style="width: 90%;margin:auto;max-height:300px">
            <br>
            <b>Khairun Najmi</b>
            <p>PR dan Cofounder</p>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/twitterlogo.png')}}" alt="" style="width: 100%">
            </div>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/instagramlogo.png')}}" alt="" style="width: 100%">
            </div>
            <br><br>
          </div>
          <div class="col-sm-4" style="min-height:400px;text-align: center">
            <img src="{{asset('belpatung-api/public/images/fotohafits.png')}}" alt="" style="width: 90%;margin:auto;max-height:300px">
            <br>
            <b>Hafits Mulya</b>
            <p>Media dan Cofounder</p>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/twitterlogo.png')}}" alt="" style="width: 100%">
            </div>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/instagramlogo.png')}}" alt="" style="width: 100%">
            </div>
            <br><br>
          </div>
          <div class="col-sm-4" style="min-height:400px;text-align: center">
            <img src="{{asset('belpatung-api/public/images/fotolutfi.png')}}" alt="" style="width: 90%;margin:auto;max-height:300px">
            <br>
            <b>Lutfianti Zaskia</b>
            <p>Design & Cofounder</p>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/twitterlogo.png')}}" alt="" style="width: 100%">
            </div>
            <div style="display:inline-block;width:50px">
              <img src="{{asset('belpatung-api/public/images/instagramlogo.png')}}" alt="" style="width: 100%">
            </div>
            <br><br>
          </div>
        </div>
      </div>

      <div id="konten-ttg-3">
        <b><h3 style="font-family: Montserrat;color:#000000">Tetap Terhubung dengan Kami</h3></b>
        <hr>
        <form action="" method="" >

          <div class="row">
            <div class="col-sm-6 col-md-6">
              <p>Nama anda </p>
              <input type="text" placeholder="nama anda" name="nama" class="form-control">
              <br>
              <p>Email anda </p>
              <input type="email" placeholder="email anda" name="email" class="form-control">
              <br>
              <p>Subjek</p>
              <input type="text" placeholder="subjek anda" name="subjek" class="form-control">
            </div>
            <div class="col-sm-6 col-md-6">
              <p>Isi pesan anda :</p>
              <textarea class="form-control" style="height:200px"></textarea>
              <br>
              <button type="submit" class="btn btn-large btn-warning" style="color:#ffffff !important">Kirim Pesan Anda</button>
            </div>
          </div>
          <hr>
          Temukan kami di :
          <br><br>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.513402780971!2d106.88168531418765!3d-6.1957869955146085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f494a4b9bce7%3A0xbb4430cc979bfcec!2sBelpatungan.com!5e0!3m2!1sen!2sid!4v1488866567188" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

        </form>

      </div>
    </div>
  </div>
</div>
</div>
@endsection
