<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>404</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Damion">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<!--  UNTUK MENU DAFTAR DI HEADER. LIHAT BAGIAN BOOTSTRAP MODAL SECTION -> DAFTAR_MODAL-->
<body >
<div class="container_404">
    <div class="content_404">
        <br>
        <div style="height:200px"></div>
        <h1 style="color: #329666;font-size: 5em">Ooops </h1>
        <h3 style="color: #329666;">Sepertinya halaman yang anda cari tidak ada </h3>
        <div class="row" style="width: 60%;margin: auto">
            <div class="col-sm-6">
                <a href="" type="button" class="btn btn-green btn-lg" style="width: 100%"> Kembali ke beranda</a>
            </div>
            <div class="col-sm-6">
                <a href="" type="button" class="btn btn-warning btn-lg" style="width: 100%"> Hubungi kami</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
