@extends('layouts.app')
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection
@section('content')
<div id="section-kalkel" class="section">
  <div class="content-section">
  <div class="title" id="title-section-kalkel">
    <h1><b>  <i class="fa fa-calendar"></i> Kalender Kelas </b></h1>
  </div>
  <div class="row">
    <div class="col-sm-4">
      <h3><i class="fa fa-search"></i> Lihat jadwal kelas</h3>
      <hr>
      <div id="calendar">
        <div id="calendar-template">

        </div>
      </div>
       <!-- Calendar -->
        <!-- /.box -->
      <form style="width: 100%">
        <div class="form-group" style="width: 100%">
        <input type="date" value="" class="form-control" style="width: 100%">
        </div>

         <div class="form-group" style="width: 100%">
        <input type="text" value="" name="nama-kelas" class="form-control" style="width: 100%" placeholder="anda juga cari nama kelas disini...">
        </div>

        <center><button type="button" class="btn btn-green"> <i class="fa fa-search"></i> Cari Kelas</button></center>
      </form>

      <br>
      <br>
      <div>
        <h3>Kelas Hari ini</h3>
        <hr>
        <div class="row">
          <div class="col-sm-6" style="min-height: 200px">
            <img src="{{asset('belpatung-api/public/images/belajar22.png')}}" alt="" style="width: 100%">
            <br>
            <a href=""><p>Kelas matematika pak budi</p></a>
          </div>
          <div class="col-sm-6" style="min-height: 200px">
            <img src="{{asset('belpatung-api/public/images/belajar22.png')}}" alt="" style="width: 100%">
            <br>
            <a href=""><p>Kelas matematika pak budi</p></a>
          </div>
          <div class="col-sm-6" style="min-height: 200px">
            <img src="{{asset('belpatung-api/public/images/belajar22.png')}}" alt="" style="width: 100%">
            <br>
            <a href=""><p>Kelas matematika pak budi</p></a>
          </div>
          <div class="col-sm-6" style="min-height: 200px">
            <img src="{{asset('belpatung-api/public/images/belajar22.png')}}" alt="" style="width: 100%">
            <br>
            <a href=""><p>Kelas matematika pak budi</p></a>
          </div>
        </div>

        <center><button type="button" class="btn btn-warning" style="color:#ffffff !important">Selengkapnya</button></center>
      </div>
    </div>
    <div class="col-sm-8">
       <div class="panel panel-success">
          <div class="panel-heading">12 Februari 2017 <b>( 5 kelas )</b></div>
          <div class="panel-body">
            <div>
              <h3><b>Kelas Matematika <a href=""><i class="fa fa-search" style="color: #329666"></i></a></b></h3>
              <p>Lorem Ipsum Dolor Sit Amet Vue </p>
              <hr>
            </div>
            <div>
              <h3><b>Kelas Matematika <a href=""><i class="fa fa-search" style="color: #329666"></i></a></b></h3>
              <p>Lorem Ipsum Dolor Sit Amet Vue </p>
              <hr>
            </div>
            <div>
              <h3><b>Kelas Matematika <a href=""><i class="fa fa-search" style="color: #329666"></i></a></b></h3>
              <p>Lorem Ipsum Dolor Sit Amet Vue </p>
              <hr>
            </div>
            <div>
              <h3><b>Kelas Matematika <a href=""><i class="fa fa-search" style="color: #329666"></i></a></b></h3>
              <p>Lorem Ipsum Dolor Sit Amet Vue </p>
              <hr>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
</div>
@endsection
