@extends('layouts.app')
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection
@section('content')
<div id="section-kalkel" class="section">
  <div class="content-section">
  <div class="title" id="title-section-kalkel">
    <h1><b> Cari Pembicara </b></h1>
  </div>
  <div class="row">
    <div class="col-sm-3">
      <p><i class="fa fa-search"></i> Cari jadwal pembicara</p>
      <hr>

        <div class="form-group">
          <input type="text" class="form-control" id="keyword" required placeholder="cari nama pembicara ..." value="{{$keyword ? $keyword : ''}}">
        </div>
        <br>
        <div class="form-group">
          <p><i class="fa fa-map-marker"></i> Nama Lokasi </p>
          <input type="text" class="form-control" id="lokasi" placeholder="pilih lokasi">
        </div>
        <br>

        <div class="form-group">
          <p><i class="fa fa-list"></i> Kategori pembicara </p>
          <select id="keahlian" name="kategori_pembicara" required class="form-control">
            <option value="all"> All</option>
            <option value="value 1"> kategori 1</option>
            <option value="value 1"> kategori 1</option>
          </select>
        </div>


        <br>

        <button type="submit" class="btn btn-green btn-lg" id="btn-search"> <i class="fa fa-search"></i> Cari Pembicara</button>

    </div>
    <div class="col-sm-9">
      <?php $chunks=$results->chunk(3); ?>
      @foreach($chunks as $chunk)
      <div class="row" style="width:90%;margin:auto">
        @foreach($chunk as $speaker)
        <div class="col-sm-4 image-thumb  ">
          <center>
          <img src="{{$speaker->link_foto_profile}}" alt="" style="width:100%;" class="img-circle img-responsive">
          <h4><b>{{$speaker->name}}</b></h4>
          <p class="lighttext"> <i>{{$speaker->title}}</i> </p>
          <hr>
          </center>
        </div>
        @endforeach
      </div>
      @endforeach
    </div>
  </div>
</div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

  $('#btn-search').click(function(event){

      var keyword = $('#keyword').val() == '' ? 'all' : $('#keyword').val();
      var url = "{{url('/search')}}/"+keyword+"/"+$('#keahlian').val();
      $(location).attr('href',url);

  });

  $('#keyword').keypress(function(event){
      if(event.which==13){
        $('#btn-search').click();
      }
  });
});
</script>
