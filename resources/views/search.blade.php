@extends('layouts.app')
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection
@section('content')
<div id="section-kalkel" class="section">
  <div class="content-section">
  <div class="title" id="title-section-kalkel">
    <h1><b> Cari Kelas </b></h1>
  </div>
  <div class="row">
    <div class="col-sm-3">
      <p><i class="fa fa-search"></i> Cari jadwal kelas</p>
      <hr>

        <div class="form-group">
          <input id="keyword" name="keyword" type="text" class="form-control"  required placeholder="cari nama kelas ..." value="{{$keyword ? $keyword : ''}}">
        </div>
        <br>
        <div class="form-group">
          <p><i class="fa fa-map-marker"></i> Nama Lokasi </p>
          <select id="location" class="form-control" required>
							<option value="all" selected>Semua Lokasi</option>
							@foreach($locations as $location)
								<option values="{{$location->nama}}">{{$location->nama}}</option>
							@endforeach
					</select>
        </div>
        <br>

        <div class="form-group">
          <p><i class="fa fa-calendar"></i> Dari tanggal </p>
          <input type="date" class="form-control" id="tanggal_mulai" placeholder="pilih tanggal mulai" >
        </div>
        <br>
        <div class="form-group">
          <p><i class="fa fa-calendar"></i> Sampai tanggal </p>
          <input type="date" class="form-control" id="tanggal_akhir" placeholder="pilih tanggal mulai" >
        </div>
        <br>
        <!-- <div class="form-group">
          <p><i class="fa fa-list"></i> Kategori kelas </p>
          <select name="kategori_kelas" required class="form-control">
            <option value="value 1"> kategori 1</option>
            <option value="value 1"> kategori 1</option>
            <option value="value 1"> kategori 1</option>
          </select>
        </div>
        <br>
        <div class="form-group">
          <p><i class="fa fa-users"></i> Jumlah peserta </p>


          <div class="checkbox">
          <label><input type="checkbox" value="">< 20 peserta </label>
        </div>
        <div class="checkbox">
          <label><input type="checkbox" value="">20 - 50 peserta</label>
        </div>
        <div class="checkbox ">
          <label><input type="checkbox" value="" >50 - 80 peserta</label>
        </div>
        <div class="checkbox ">
          <label><input type="checkbox" value="" >> 80 peserta</label>
        </div>
        </div> -->

        <br>

        <button id="btn-search" class="btn btn-green btn-lg"> <i class="fa fa-search"></i> Cari Kelas</button>

    </div>
    <div class="col-sm-9">
      <div class="row" style="width: 90%;margin:auto">
      @if($results)
      @foreach($results as $course)
      <div class="col-sm-4 image-thumb  ">
        <div class="green-column-inverse">
          <img src="{{asset('public/uploads')}}/{{ $course->link_foto }}">
          <div class="sinopsis-kelas">
            <h4><b> {{$course->nama_kelas}} </b></h4>
            <p class="lighttext"> Jenis kelas </p>
            <span class="label {{$course->id==1 ? 'label-primary': 'label-success'}}">{{ $course->kategori }}</span>
            <hr>
            <p>{{ str_limit($course->deskripsi, $limit = 100, $end = '...') }}
            </p>
            <hr>
            <div class="media">
              <div class="media-left">
                <?php $user=$users->where('id',$course->user_id)->first(); ?>
                <img src="{{$user->link_foto_profile}}" class="media-object img-circle" style="width:60px">
              </div>
              <div class="media-body">
                <h4 class="media-heading">{{$user->name}}</h4>
                <p>{{$user->title}}</p>
              </div>
            </div>
            <div class="line-primary"></div>
            <div class="row">
              <div class="col-sm-4">
                <h3><b> {{round($course->total_dana/$course->target_dana,2)*100}} % </b></h3>
                <h5>Progress</h5>
              </div>
              <div class="col-sm-8">
                <div style="padding-top:2px;padding-bottom: 2px">
                  <i class="fa fa-user" style="color: #337ab7"> </i> {{ count($course->donations) }} peserta terdaftar
                </div>
                <div style="padding-top:2px;padding-bottom: 2px">
                  <i class="fa fa-search" style="color: #337ab7"> </i> <a href="{{url('courses')}}/{{$course->link_kelas}}"> Lihat Detail </a>
                </div>
                <div style="padding-top:2px;padding-bottom: 2px">
                <?php 
                $registered=false;
                if($participants && Auth::user()){
                  $registered=$participants->where('course_id',$course->id)->where('user_id',Auth::user()->id)->first(); 
                }
                
                // echo count($registered);
                ?>
                @if(!$registered)
                <i class="fa fa-pencil" style="color: #337ab7"> </i> <a href="{{url('transactions/donation')}}/{{$course->id}}"> Join kelas </a>
                @else
                <i class="fa fa-check" style="color: #337ab7"> </i> <span> Terdaftar </span>
                @endif
                </div>
              </div>
            </div>
          </div>
        </div>
        <br>
      </div>
      @endforeach
      @endif

      </div>
    </div>
  </div>
</div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

  $('#btn-search').click(function(event){

      var keyword = $('#keyword').val()== '' ? 'all' : $('#keyword').val() ;
      var tanggal_mulai = $('#tanggal_mulai').val() == '' ? 'all' : $('#tanggal_mulai').val() ;
      var tanggal_selesai =  $('#tanggal_akhir').val() == '' ? 'all' : $('#tanggal_akhir').val() ;
			if((tanggal_mulai == 'all' && tanggal_selesai != 'all') ||(tanggal_mulai != 'all' && tanggal_selesai == 'all')){
				alert('Lengkapi rentang tanggal yang diinginkan');
			}
			else if(tanggal_mulai > tanggal_selesai){
				alert('Tanggal selesai harus lebih besar dari tanggal mulai');
			}
			else{
	      var url = "{{url('/courses/search')}}/"+keyword+"/"+$('#location').val()+"/"+ tanggal_mulai + "/" + tanggal_selesai;
        // console.log(url);
	      $(location).attr('href',url);
			}

  });

  $('#keyword').keypress(function(e){
      if(e.which == 13){
        $('#btn-search').click();
      }
  });
});


</script>
