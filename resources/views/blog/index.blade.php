@extends('layouts.app')
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection
@section('style')
.navbar{
  margin-bottom:0;
}
@endsection
@section('content')
<div style="width: 100%;height: 400px;background-image: url({{asset('belpatung-api/public/images/belajar2.jpg')}});background-size: 100%">

	</div>
	<!--SECTION TENTANG BLOG-->
	<div id="section-tentang-kami" class="section">
	<div class="content-section">
		<div class="title" id="title-section-blog">
			<h1><b> Blog </b></h1>
		</div>
		<div class="row">
			<div class="col-sm-8">
				<div class="row">
					<div class="col-sm-6">
						<a href="{{route('blog.detail',1)}}"><img src="{{asset('belpatung-api/public/images/bg2.png')}}" style="max-width: 100%"></a>

						<h3 style="font-style: montserrat;font-weight: bold;color:#329666"><center>Title blog 1</center></h3>
						<i class="fa fa-calendar"></i> &nbsp 12 Januari 2017  &nbsp | &nbsp <i class="fa fa-comment-o"></i> 2 komentar
						<hr>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<br><br>

					</div>
					<div class="col-sm-6">
						<a href=""><img src="{{asset('belpatung-api/public/images/bg2.png')}}" style="max-width: 100%"></a>

						<h3 style="font-style: montserrat;font-weight: bold;color:#329666"><center>Title blog 1</center></h3>
						<i class="fa fa-calendar"></i> &nbsp 12 Januari 2017  &nbsp | &nbsp <i class="fa fa-comment-o"></i> 2 komentar
						<hr>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<br><br>

					</div>
					<div class="col-sm-6">
						<a href=""><img src="{{asset('belpatung-api/public/images/bg2.png')}}" style="max-width: 100%"></a>

						<h3 style="font-style: montserrat;font-weight: bold;color:#329666"><center>Title blog 1</center></h3>
						<i class="fa fa-calendar"></i> &nbsp 12 Januari 2017  &nbsp | &nbsp <i class="fa fa-comment-o"></i> 2 komentar
						<hr>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<br><br>

					</div>
					<div class="col-sm-6">
						<a href=""><img src="{{asset('belpatung-api/public/images/bg2.png')}}" style="max-width: 100%"></a>

						<h3 style="font-style: montserrat;font-weight: bold;color:#329666"><center>Title blog 1</center></h3>
						<i class="fa fa-calendar"></i> &nbsp 12 Januari 2017  &nbsp | &nbsp <i class="fa fa-comment-o"></i> 2 komentar
						<hr>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<br><br>

					</div>
					<div class="col-sm-6">
						<a href=""><img src="{{asset('belpatung-api/public/images/bg2.png')}}" style="max-width: 100%"></a>

						<h3 style="font-style: montserrat;font-weight: bold;color:#329666"><center>Title blog 1</center></h3>
						<i class="fa fa-calendar"></i> &nbsp 12 Januari 2017  &nbsp | &nbsp <i class="fa fa-comment-o"></i> 2 komentar
						<hr>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<br><br>

					</div>
					<div class="col-sm-6">
						<a href=""><img src="{{asset('belpatung-api/public/images/bg2.png')}}" style="max-width: 100%"></a>

						<h3 style="font-style: montserrat;font-weight: bold;color:#329666"><center>Title blog 1</center></h3>
						<i class="fa fa-calendar"></i> &nbsp 12 Januari 2017  &nbsp | &nbsp <i class="fa fa-comment-o"></i> 2 komentar
						<hr>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</p>
						<br><br>

					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<form class="form-inline" >
					<div class="form-group" >
						<input class="form-control" style="width: 300px" placeholder="Cari artikel disini...">
						<button type="button" class="btn btn-success" style="margin-left: -5px"><i class="fa fa-search"></i></button>
					</div>
				</form>
				<br><br>
				<p><b>Baca juga :</b></p>
				<hr>
				<p>
					<div class="list-group">
					  <a href="#" class="list-group-item">First item</a>
					  <a href="#" class="list-group-item">Second item</a>
					  <a href="#" class="list-group-item">Third item</a>
					</div>
				</p>
			</div>
		</div>

		<center>
			<ul class="pagination">
			  <li><a href="#">1</a></li>
			  <li class="active"><a href="#">2</a></li>
			  <li><a href="#">3</a></li>
			  <li><a href="#">4</a></li>
			  <li><a href="#">5</a></li>
			</ul>
		</center>
	</div>
	</div>
	<!--SECTION END BLOG -->
@endsection
