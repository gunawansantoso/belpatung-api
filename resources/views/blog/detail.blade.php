@extends('layouts.app')
@section('navbar')
    @if (Route::has('login'))
            @if (!Auth::check())
              <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal"> DAFTAR </button>
              <button type="button" class="btn btn-green-inverse" data-toggle="modal" data-target="#myModal2"> MASUK </button>
            @endif
    @endif
@endsection
@section('style')
.navbar{
  margin-bottom:0;
}
@endsection
@section('content')
	<div id="section-tentang-kami" class="section">
		<div class="content-section">
			<div class="row">
				<div class="col-sm-8">
					<div class="title title-detail-artikel" id="title-section-blog">
						<h1><b> Judul Artikel 1</b></h1>
					</div>
					<a href=""><img src="{{asset('belpatung-api/public/images/bg2.png')}}" style="max-width: 100%"></a>
					<hr>					
					<i class="fa fa-calendar"></i> &nbsp 12 Januari 2017  &nbsp | &nbsp <i class="fa fa-comment-o"></i> 2 komentar
					<hr>
					<p style="text-align: justify;">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					Why do we use it?
					It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
					<br><br>
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					Why do we use it?
					It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					Why do we use it?
					It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
					</p>
					<hr>
					<h2> Komentar </h2>
					<br>
					<div class="komentar=panel">
						<!-- Left-aligned -->
						<div class="media">
						  <div class="media-left">
						    <img src="{{asset('belpatung-api/public/images/img_avatar1.png')}}" class="media-object img-circle" style="width:60px">
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">John Doe</h4>
						    <p class="text-green"><i class="fa fa-calendar"> 15 December 2016</i></p>
						    <p>Lorem ipsum...</p>
						  </div>
						  <br>
						</div>

						<div class="media">
						  <div class="media-left">
						    <img src="{{asset('belpatung-api/public/images/img_avatar1.png')}}" class="media-object img-circle" style="width:60px">
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">Anonym121251</h4>
						    <p class="text-green"><i class="fa fa-calendar"> 15 September 2016</i></p>
						    <p>many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like</p>
						  </div>
						  <br>
						</div>
						<br>
						<a href="">5 komentar lain...</a>
					</div>
					<hr>
					<div class="add-komentar">
						<h2> Tuliskan komentar anda </h2>
						<form action="single-blog_submit" method="get" accept-charset="utf-8">
							<div class="form-group">
							    <textarea type="text" name="komentar" class="form-control" id="komentar" placeholder="masukan komentar anda disini ..." style="height: 250px"> </textarea>
						    </div>
						    <button type="button" class="btn btn-primary btn-lg"> <i class="fa fa-send"> </i> Kirim komentar </button>
						</form>
					</div>
				</div>

				<div class="col-sm-4">
					<form class="form-inline" >
						<div class="form-group" >
							<input class="form-control" style="width: 300px" placeholder="Cari artikel disini...">
							<button type="button" class="btn btn-success" style="margin-left: -5px"><i class="fa fa-search"></i></button>
						</div>
					</form>
					<br><br>
					<p><b>Baca juga :</b></p>
					<hr>
					<p>
						<div class="list-group">
						  <a href="#" class="list-group-item">First item</a>
						  <a href="#" class="list-group-item">Second item</a>
						  <a href="#" class="list-group-item">Third item</a>
						</div>
					</p>
				</div>
			</div>
			

		</div>
	</div>

@endsection
