@extends('layouts.admin')
@section("content")
    <section class="content">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissable fade in fixed">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $error }}
            </div>
        @endforeach
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="pull-left">
                        <a href="{{ route('admin.blog.edit', $post->id) }}" class="btn btn-primary">
                            Edit
                        </a>
                        <a href="#" data-toggle="modal" class="btn btn-danger btn-sm" data-target="#delete-modal"><i class='fa fa-trash-o'> Hapus </i></a>
                    </div>
                    <div class="pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3> {{ $post->title }}</h3>
                            <img src="{{ $post->photo }}" class="img-responsive"/>
                        </div>
                        <div class="panel-body">
                            {{ $post->content }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
            <div class="modal modal-danger" id="delete-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h4 class="modal-title">Delete Post</h4>
                        </div>
                        <form action="{{ route('admin.blog.delete', $post->id) }}" method="post">
                            {{ csrf_field() }}
                            <div class="modal-body">
                                <p>Anda yakin akan menghapus <b> "{{ $post->title}}"</b></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-outline">Delete</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
    </section>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#photo").change(function(){
            readURL(this);
        });
    </script>
@endsection
