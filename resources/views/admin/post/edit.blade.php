@extends('layouts.admin')
@section("content")
    <section class="content">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissable fade in fixed">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $error }}
            </div>
        @endforeach
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="pull-left">
                        <a href="#" class="btn btn-primary">
                        </a>
                    </div>
                    <div class="pull-right">
                    </div>
                </div>
                <div class="box-body">
                    <form action="{{ route('admin.blog.update', $post->id) }}" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="title">
                                Judul Post
                            </label>
                            <input type="text" name="title" id="title" class="form-control" value="{{$post->title}}"/>
                        </div>
                        <div class="upload-area" style="position: relative;text-align: center">
                            <div class="form-group" style="position: absolute; height: 100; width: 100;top:0;opacity: 0;">
                                <label for="icon">Icon</label>
                                <input type="file" class="form-control input-lg" id="photo" name="photo">
                            </div>
                            <div>
                                <img id="preview" width="100px" height="100px" src="{{ $post->photo }}" id="thumb">
                            </div>
                        </div>
                        <div class="form-group">
                            <script src='{{ asset('js/vendor/tinymce/js/tinymce/tinymce.min.js') }}'></script>
                            <script>
                                tinymce.init({
                                    selector: '#content',
                                    plugins : ["advlist autolink lists link image charmap print preview anchor"],
                                    force_br_newlines : false,
                                    force_p_newlines : false,
                                    force_span_newlines:false,
                                    entity_encoding : 'raw',
                                    forced_root_block : '',
                                    mode : "textareas",
                                });
                            </script>
                            <textarea name="content" id="content">{{ $post->content }}</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"> Simpan </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#photo").change(function(){
            readURL(this);
        });
    </script>
@endsection
