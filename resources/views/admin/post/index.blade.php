@extends('layouts.admin')
@section("content")
    <section class="content">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissable fade in fixed">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $error }}
            </div>
        @endforeach
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="{{ route('admin.blog.create') }}" class="btn btn-primary">
                                Buat
                            </a>
                        </div>
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        @foreach($posts as $post)
                            <div class="col-lg-4">
                                <div class="panel">
                                    <div class="panel-heading">
                                        <h3>
                                            {{ $post->title }}
                                        </h3>
                                    </div>
                                    <div class="panel-body">
                                        {{ $post->content }}
                                    </div>
                                    <div class="panel-footer">
                                        <a href="{{ route('admin.blog.show', $post->id) }} }}" class="btn btn-primary btn-sm">Lihat</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
