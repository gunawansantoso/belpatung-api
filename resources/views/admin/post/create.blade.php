@extends('layouts.admin')
@section("content")
    <section class="content">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger alert-dismissable fade in fixed">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $error }}
            </div>
        @endforeach
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-left">
                            <a href="#" class="btn btn-primary">
                            </a>
                        </div>
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="{{ route('admin.blog.store') }}" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title">
                                    Judul Post
                                </label>
                                <input type="text" name="title" id="title" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="photo">
                                    Photo
                                </label>
                                <input type="file" name="photo" id="photo" class="form-control"/>
                            </div>
                            <div class="form-group">
                                <script src='{{ asset('js/vendor/tinymce/js/tinymce/tinymce.min.js') }}'></script>
                                <script>
                                    tinymce.init({
                                        selector: '#content',
                                        plugins : ["advlist autolink lists link image charmap print preview anchor"],
                                        force_br_newlines : false,
                                        force_p_newlines : false,
                                        force_span_newlines:false,
                                        entity_encoding : 'raw',
                                        forced_root_block : '',
                                        mode : "textareas",
                                    });
                                </script>
                                <textarea name="content" id="content"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"> Buat </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
