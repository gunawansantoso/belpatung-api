<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDescriptionTypeInCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('courses', function (Blueprint $table) {
        //     //
        //     $table->
        // });
        DB::statement('ALTER TABLE courses MODIFY COLUMN deskripsi TEXT');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('courses', function (Blueprint $table) {
        //     //
        // });
        DB::statement('ALTER TABLE courses MODIFY COLUMN deskripsi VARCHAR(190)');
    }
}
