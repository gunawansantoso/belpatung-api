<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandingPageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_page', function (Blueprint $table) {
            $table->integer("id")->default(1);
            $table->string("description")->nullable();
            $table->integer('class_id_1')->nullable();
            $table->integer('class_id_2')->nullable();
            $table->integer('class_id_3')->nullable();
            $table->integer('class_id_4')->nullable();
            $table->integer('class_id_5')->nullable();
            $table->integer('class_id_6')->nullable();
            $table->string('partner_img')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
