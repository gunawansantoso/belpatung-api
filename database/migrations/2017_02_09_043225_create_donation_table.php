<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_donations', function (Blueprint $table) {
            $table->increments("donation_id");
            $table->integer("user_id");
            $table->integer("course_id");
            $table->integer("transaction_code");
            $table->boolean("isVerified")->default(false);
            $table->string('danation_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
