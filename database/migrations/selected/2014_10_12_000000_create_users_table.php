<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email', 250);
            $table->string('password')->nullable();
            $table->string('nomor_wa')->nullable();
            $table->string('nama_organisasi')->nullable();
            $table->string('alamat_organisasi')->nullable();
            $table->string('link_foto_profile')->nullable();
            $table->string('link_foto_verfikasi')->nullable();
            $table->boolean('isactivated')->default(false);
            $table->boolean('isverified')->default(false);
            $table->string('level')->nullable();
            $table->boolean('isspeaker')->nullable()->default(false);
            $table->boolean('isadmin')->nullable()->default(false);
            $table->string('title')->nullable()->default('mentor');
            $table->string('address')->nullable();
            $table->string('description')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
