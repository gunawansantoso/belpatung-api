<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('nama_kelas');
            $table->string('kategori');
            $table->integer('target_dana');
            $table->integer('total_dana');
            $table->string('posisi_user');
            $table->integer('estimasi_peserta');
            $table->string('link_kelas')->unique();
            $table->string('link_foto');
            $table->date('tanggal_kelas');
            $table->date('deadline_pembayaran');
            $table->string('lokasi');
            $table->string('deskripsi');
            $table->string('status')->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
