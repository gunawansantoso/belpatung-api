<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Modules\User\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Modules\Course\Models\Participant::class, function (Faker\Generator $faker){
    return [
        'class_id' => $faker->numberBetween(1, 10),
        'user_id' => $faker->numberBetween(1,4),
        'donasi' => $faker->numberBetween(1,4),
        'isConfirmed' => false,
        'status' => "completed"
    ];
});

$factory->define(App\Modules\Transaction\Models\Donation::class, function (Faker\Generator $faker){
    return [
        'donation_id' => $faker->unique()->numberBetween(1, 100),
        'user_id' => $faker->numberBetween(1,4),
        'course_id' => $faker->numberBetween(1,10),
        'transaction_code' => $faker->unique()->numberBetween(1,100),
    ];
});

$factory->define(App\Modules\Transaction\Models\Transaction::class, function (Faker\Generator $faker){
    return [
        'transaction_code' => $faker->unique()->numberBetween(1, 100),
        'payment_method' => $faker->randomAscii(),
        'bank' => $faker->randomLetter(),
        'amount' => $faker->numberBetween(1000,100000),
        'fraud_status' => $faker->randomLetter(),
        'type' => $faker->randomLetter()
    ];
});

$factory->define(App\Modules\User\Models\Skill::class, function (Faker\Generator $faker){
    return [
        'user_id' => $faker->numberBetween(1, 100),
        'skill' => $faker->randomLetter(),
    ];
});
